-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- H�te : 127.0.0.1
-- G�n�r� le :  jeu. 03 mai 2018 � 14:24
-- Version du serveur :  10.1.26-MariaDB
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de donn�es :  `3a22`
--
CREATE DATABASE IF NOT EXISTS `3a22` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `3a22`;

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

CREATE TABLE `personne` (
  `id` int(11) NOT NULL,
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- D�chargement des donn�es de la table `personne`
--

INSERT INTO `personne` (`id`, `nom`, `prenom`) VALUES
(1, 'aa', 'gfhkvjk'),
(2, 'aa', 'gfhkvjk'),
(3, 'aa', 'gfhkvjk'),
(4, 'aa', 'gfhkvjk');

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `personne`
--
ALTER TABLE `personne`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables d�charg�es
--

--
-- AUTO_INCREMENT pour la table `personne`
--
ALTER TABLE `personne`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Base de donn�es :  `aa`
--
CREATE DATABASE IF NOT EXISTS `aa` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `aa`;

-- --------------------------------------------------------

--
-- Structure de la table `annonces`
--

CREATE TABLE `annonces` (
  `id_annonce` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `ville_depart` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `ville_arriver` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `cout` int(11) DEFAULT NULL,
  `lieux_depart` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lieux_arriver` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nbr_places_dispo` int(11) DEFAULT NULL,
  `date_depart` date DEFAULT NULL,
  `date_arriver` date DEFAULT NULL,
  `bagage` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `colis` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `handicap` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `animaux` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sexe_passagers` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cat�gorie`
--

CREATE TABLE `cat�gorie` (
  `id_cat�gorie` int(11) NOT NULL,
  `libelle_cat�gorie` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `colis`
--

CREATE TABLE `colis` (
  `id_colis` int(11) NOT NULL,
  `poids` double NOT NULL,
  `dimension` int(11) NOT NULL,
  `description` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE `commentaire` (
  `id_user` int(11) NOT NULL,
  `Id_commentaire` int(11) NOT NULL,
  `Description` text COLLATE utf8_unicode_ci NOT NULL,
  `Note_commentaire` int(11) DEFAULT NULL,
  `id_sujet` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `commentaire`
--

INSERT INTO `commentaire` (`id_user`, `Id_commentaire`, `Description`, `Note_commentaire`, `id_sujet`) VALUES
(2, 2, 'ddcsdv', NULL, 2),
(2, 3, 'good ', NULL, 2);

-- --------------------------------------------------------

--
-- Structure de la table `evaluation`
--

CREATE TABLE `evaluation` (
  `Id_evaluation` int(11) NOT NULL,
  `Id_user` int(11) NOT NULL,
  `Id_evaluateur` int(11) NOT NULL,
  `Note` int(11) DEFAULT NULL,
  `Description` text COLLATE utf8_unicode_ci NOT NULL,
  `Etoile` text COLLATE utf8_unicode_ci,
  `Id_annonce` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ev�nement`
--

CREATE TABLE `ev�nement` (
  `id_ev�nement` int(11) NOT NULL,
  `description` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_de_debut` date NOT NULL,
  `date_de_fin` date NOT NULL,
  `nom_�v�nement` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `site_web` varchar(60) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `reclamation`
--

CREATE TABLE `reclamation` (
  `idReclamation` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `typeReclamation` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `dateReclamation` date NOT NULL,
  `sujetReclamation` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `reservations`
--

CREATE TABLE `reservations` (
  `id_reservation` int(11) NOT NULL,
  `id_annonce` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nbr_places_reservees` int(11) DEFAULT NULL,
  `itineraire_souhaite` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `baggage` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `id_colis` int(11) NOT NULL,
  `handicape` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sujet_forum`
--

CREATE TABLE `sujet_forum` (
  `id_sujet` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nom_sujet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `timeline`
--

CREATE TABLE `timeline` (
  `id_timeline` int(11) NOT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `timeline`
--

INSERT INTO `timeline` (`id_timeline`, `date`, `action`, `user`) VALUES
(1, '2018-02-23 � 03:45', 'a a ajouter un sujet ', 'a'),
(2, '2018-02-23 � 03:45', 'a a supprimer son sujet ', 'a'),
(3, '2018-02-23 � 03:45', 'a a ajouter un sujet ', 'a'),
(4, '2018-02-23 � 03:45', 'a a commenter le sujet 2:aaaaa', 'a'),
(5, '2018-02-23 � 03:46', 'a a supprimer son commentaire ', 'a'),
(6, '2018-02-23 � 03:46', 'a a commenter le sujet 2:ddcsdv', 'a'),
(7, '2018-02-23 � 03:47', 'a a commenter le sujet 2:good ', 'a'),
(8, '2018-02-23 � 04:59', 'a a modifier son sujet 2', 'a'),
(9, '2018-02-23 � 04:59', 'a a supprimer son sujet ', 'a');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `id_user` int(11) NOT NULL,
  `nom` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `cin` int(11) NOT NULL,
  `date_naissance` date DEFAULT NULL,
  `sexe` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `num_tel` int(11) NOT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `adresse` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `mot_de_passe` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `login` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `moyenne` double DEFAULT NULL,
  `role` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id_user`, `nom`, `prenom`, `cin`, `date_naissance`, `sexe`, `num_tel`, `email`, `adresse`, `age`, `mot_de_passe`, `login`, `photo`, `moyenne`, `role`) VALUES
(1, 'ahmed', 'benabdllah', 9846221, '2018-02-22', 'Homme', 5555, 'benabdallah@gmail.com', 'aaa', 22, 'aa', 'aaa', NULL, NULL, 2),
(2, 'a', 'a', 0, '2018-02-22', 'Homme', 0, 'ben.abdallah.ahmed00@gmail.com', 'a', 2, 'aa', 'aa', NULL, NULL, 2);

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `annonces`
--
ALTER TABLE `annonces`
  ADD PRIMARY KEY (`id_annonce`);

--
-- Index pour la table `cat�gorie`
--
ALTER TABLE `cat�gorie`
  ADD PRIMARY KEY (`id_cat�gorie`);

--
-- Index pour la table `colis`
--
ALTER TABLE `colis`
  ADD PRIMARY KEY (`id_colis`);

--
-- Index pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD PRIMARY KEY (`Id_commentaire`);

--
-- Index pour la table `evaluation`
--
ALTER TABLE `evaluation`
  ADD PRIMARY KEY (`Id_evaluation`);

--
-- Index pour la table `ev�nement`
--
ALTER TABLE `ev�nement`
  ADD PRIMARY KEY (`id_ev�nement`);

--
-- Index pour la table `reclamation`
--
ALTER TABLE `reclamation`
  ADD PRIMARY KEY (`idReclamation`);

--
-- Index pour la table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id_reservation`);

--
-- Index pour la table `sujet_forum`
--
ALTER TABLE `sujet_forum`
  ADD PRIMARY KEY (`id_sujet`);

--
-- Index pour la table `timeline`
--
ALTER TABLE `timeline`
  ADD PRIMARY KEY (`id_timeline`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT pour les tables d�charg�es
--

--
-- AUTO_INCREMENT pour la table `annonces`
--
ALTER TABLE `annonces`
  MODIFY `id_annonce` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `cat�gorie`
--
ALTER TABLE `cat�gorie`
  MODIFY `id_cat�gorie` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `colis`
--
ALTER TABLE `colis`
  MODIFY `id_colis` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `commentaire`
--
ALTER TABLE `commentaire`
  MODIFY `Id_commentaire` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `evaluation`
--
ALTER TABLE `evaluation`
  MODIFY `Id_evaluation` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `ev�nement`
--
ALTER TABLE `ev�nement`
  MODIFY `id_ev�nement` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `reclamation`
--
ALTER TABLE `reclamation`
  MODIFY `idReclamation` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id_reservation` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `sujet_forum`
--
ALTER TABLE `sujet_forum`
  MODIFY `id_sujet` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `timeline`
--
ALTER TABLE `timeline`
  MODIFY `id_timeline` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Base de donn�es :  `amendes`
--
CREATE DATABASE IF NOT EXISTS `amendes` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `amendes`;

-- --------------------------------------------------------

--
-- Structure de la table `amende`
--

CREATE TABLE `amende` (
  `id` int(11) NOT NULL,
  `personne` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `montant` double DEFAULT NULL,
  `date` date DEFAULT NULL,
  `penaltie` double DEFAULT NULL,
  `payee` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `amende`
--

INSERT INTO `amende` (`id`, `personne`, `montant`, `date`, `penaltie`, `payee`) VALUES
(4, '000011111', 786786, '2013-01-01', 0, 'oui');

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

CREATE TABLE `personne` (
  `cin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `personne`
--

INSERT INTO `personne` (`cin`, `nom`, `prenom`, `adresse`) VALUES
('000011111', 'ben ', 'ben ', 'ben'),
('09846221', 'ahmed', 'ahmed', 'ahmed');

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `amende`
--
ALTER TABLE `amende`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_613014CFFCEC9EF` (`personne`);

--
-- Index pour la table `personne`
--
ALTER TABLE `personne`
  ADD PRIMARY KEY (`cin`);

--
-- AUTO_INCREMENT pour les tables d�charg�es
--

--
-- AUTO_INCREMENT pour la table `amende`
--
ALTER TABLE `amende`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Contraintes pour les tables d�charg�es
--

--
-- Contraintes pour la table `amende`
--
ALTER TABLE `amende`
  ADD CONSTRAINT `FK_613014CFFCEC9EF` FOREIGN KEY (`personne`) REFERENCES `personne` (`cin`);
--
-- Base de donn�es :  `atelier_5_php`
--
CREATE DATABASE IF NOT EXISTS `atelier_5_php` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `atelier_5_php`;

-- --------------------------------------------------------

--
-- Structure de la table `document`
--

CREATE TABLE `document` (
  `ref` int(11) NOT NULL,
  `nom` varchar(0) NOT NULL,
  `date` date NOT NULL,
  `prix` float NOT NULL,
  `auteur` varchar(11) NOT NULL,
  `nbPages` int(11) DEFAULT NULL,
  `capacite` int(11) DEFAULT NULL,
  `duree` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- D�chargement des donn�es de la table `document`
--

INSERT INTO `document` (`ref`, `nom`, `date`, `prix`, `auteur`, `nbPages`, `capacite`, `duree`, `type`) VALUES
(6526, '', '2017-02-01', 2500, '1', 1, NULL, NULL, 0);

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`ref`);
--
-- Base de donn�es :  `atelier_5_said`
--
CREATE DATABASE IF NOT EXISTS `atelier_5_said` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `atelier_5_said`;

-- --------------------------------------------------------

--
-- Structure de la table `document`
--

CREATE TABLE `document` (
  `Reference` int(11) NOT NULL,
  `Nom` text NOT NULL,
  `Date_creation` text NOT NULL,
  `Prix` float NOT NULL,
  `Auteur` text NOT NULL,
  `nbPages` int(11) DEFAULT NULL,
  `Duree` int(11) DEFAULT NULL,
  `Capacite` int(11) DEFAULT NULL,
  `Type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- D�chargement des donn�es de la table `document`
--

INSERT INTO `document` (`Reference`, `Nom`, `Date_creation`, `Prix`, `Auteur`, `nbPages`, `Duree`, `Capacite`, `Type`) VALUES
(10, 'akoum', '2017-01-26', 100, 'mohamed', 150, NULL, NULL, 'Livre'),
(50, 'akoum', '2017-01-26', 100, 'mohamed', 150, NULL, NULL, 'Livre'),
(99, 'akoum', '2017-01-19', 100, 'mohamed', 150, NULL, NULL, 'Livre');

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`Reference`);
--
-- Base de donn�es :  `bd`
--
CREATE DATABASE IF NOT EXISTS `bd` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bd`;
--
-- Base de donn�es :  `bibliotheque`
--
CREATE DATABASE IF NOT EXISTS `bibliotheque` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bibliotheque`;

-- --------------------------------------------------------

--
-- Structure de la table `document`
--

CREATE TABLE `document` (
  `reference` int(11) NOT NULL,
  `nom` varchar(20) NOT NULL,
  `dateCreation` date NOT NULL,
  `prix` int(11) NOT NULL,
  `auteur` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `nbPages` int(11) DEFAULT NULL,
  `duree` int(11) DEFAULT NULL,
  `capacite` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- D�chargement des donn�es de la table `document`
--

INSERT INTO `document` (`reference`, `nom`, `dateCreation`, `prix`, `auteur`, `type`, `nbPages`, `duree`, `capacite`) VALUES
(21321, '1', '2017-10-12', 2, '3ebd lehmid ', 'livre', 3665, 0, 0),
(1111111, '5165', '0000-00-00', 3, '111', 'livre', 3, 0, 0);

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`reference`);
--
-- Base de donn�es :  `candidat`
--
CREATE DATABASE IF NOT EXISTS `candidat` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `candidat`;

-- --------------------------------------------------------

--
-- Structure de la table `candidat`
--

CREATE TABLE `candidat` (
  `id` int(11) NOT NULL,
  `cin` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `situationfamiliale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nbjours` int(11) NOT NULL,
  `etat` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `candidat`
--

INSERT INTO `candidat` (`id`, `cin`, `nom`, `situationfamiliale`, `nbjours`, `etat`) VALUES
(1, 1414, 'fxhr', 'eqrg', 12, 'accepterhahahaah'),
(2, 752, 'eqrt', 'qerg', 25, 'accepter'),
(3, 14, '25', '25', 25, 'refuser'),
(4, 22, '8', '78', 785, 'refuser'),
(5, 14444, '25', '25', 225, 'accepter');

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `candidat`
--
ALTER TABLE `candidat`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_6AB5B471ABE530DA` (`cin`);

--
-- AUTO_INCREMENT pour les tables d�charg�es
--

--
-- AUTO_INCREMENT pour la table `candidat`
--
ALTER TABLE `candidat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Base de donn�es :  `cdtvl`
--
CREATE DATABASE IF NOT EXISTS `cdtvl` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `cdtvl`;

-- --------------------------------------------------------

--
-- Structure de la table `athlete`
--

CREATE TABLE `athlete` (
  `cin` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `datenaissance` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `etat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tauxD` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `test_dopage`
--

CREATE TABLE `test_dopage` (
  `code_test` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `athlete`
--
ALTER TABLE `athlete`
  ADD PRIMARY KEY (`cin`);

--
-- Index pour la table `test_dopage`
--
ALTER TABLE `test_dopage`
  ADD PRIMARY KEY (`code_test`);

--
-- AUTO_INCREMENT pour les tables d�charg�es
--

--
-- AUTO_INCREMENT pour la table `athlete`
--
ALTER TABLE `athlete`
  MODIFY `cin` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `test_dopage`
--
ALTER TABLE `test_dopage`
  MODIFY `code_test` int(11) NOT NULL AUTO_INCREMENT;
--
-- Base de donn�es :  `etudiant`
--
CREATE DATABASE IF NOT EXISTS `etudiant` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `etudiant`;

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

CREATE TABLE `etudiant` (
  `matricule` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `niveau` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `numclasse` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `etudiant`
--

INSERT INTO `etudiant` (`matricule`, `nom`, `prenom`, `niveau`, `numclasse`) VALUES
(-4, '25', '558', '4qfdsd', -4),
(2, '11', '11', '', 11),
(5, '25', 'ahmed', '', 3),
(17, '25', 'ahmed', 'csqcqsc', -3),
(18, 'tgrr', 'fzef', '3', 2),
(425, '45245', '42', '58', 3);

-- --------------------------------------------------------

--
-- Structure de la table `projet`
--

CREATE TABLE `projet` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `niveau` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `projet`
--

INSERT INTO `projet` (`id`, `titre`, `description`, `niveau`) VALUES
(1, 'vverv', 'rvgerg', '3a'),
(3, 'fefz', 'dzd', '4TWIN');

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD PRIMARY KEY (`matricule`);

--
-- Index pour la table `projet`
--
ALTER TABLE `projet`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables d�charg�es
--

--
-- AUTO_INCREMENT pour la table `projet`
--
ALTER TABLE `projet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Base de donn�es :  `examengrp`
--
CREATE DATABASE IF NOT EXISTS `examengrp` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `examengrp`;

-- --------------------------------------------------------

--
-- Structure de la table `athlete`
--

CREATE TABLE `athlete` (
  `Cin` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateN` date NOT NULL,
  `etat` varbinary(255) NOT NULL,
  `tauxD` int(11) DEFAULT NULL,
  `testDopage` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `athlete`
--

INSERT INTO `athlete` (`Cin`, `nom`, `prenom`, `dateN`, `etat`, `tauxD`, `testDopage`) VALUES
(58778, '25', '558', '2013-01-01', 0x30, 0, NULL),
(5877888, '7868', '78578', '2013-01-01', 0x30, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `test_dopage`
--

CREATE TABLE `test_dopage` (
  `code` int(11) NOT NULL,
  `libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `test_dopage`
--

INSERT INTO `test_dopage` (`code`, `libelle`) VALUES
(1, 'stimulant'),
(2, 'narcotique');

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `athlete`
--
ALTER TABLE `athlete`
  ADD PRIMARY KEY (`Cin`),
  ADD KEY `IDX_C03B83218AC12AEE` (`testDopage`);

--
-- Index pour la table `test_dopage`
--
ALTER TABLE `test_dopage`
  ADD PRIMARY KEY (`code`);

--
-- Contraintes pour les tables d�charg�es
--

--
-- Contraintes pour la table `athlete`
--
ALTER TABLE `athlete`
  ADD CONSTRAINT `FK_C03B83218AC12AEE` FOREIGN KEY (`testDopage`) REFERENCES `test_dopage` (`code`);
--
-- Base de donn�es :  `fos`
--
CREATE DATABASE IF NOT EXISTS `fos` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `fos`;

-- --------------------------------------------------------

--
-- Structure de la table `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `nom`, `prenom`) VALUES
(1, 'ahmed', 'ahmed', 'ahmed.benabdallah@esprit.tn', 'ahmed.benabdallah@esprit.tn', 1, NULL, '$2y$13$oK8ZLnMSwm7G7qudDfLTreeQY5Z.B2TNm3fbiLTdBI.hADTxt52re', '2018-02-04 00:10:27', NULL, NULL, 'a:0:{}', '', '');

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`);

--
-- AUTO_INCREMENT pour les tables d�charg�es
--

--
-- AUTO_INCREMENT pour la table `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Base de donn�es :  `hopital`
--
CREATE DATABASE IF NOT EXISTS `hopital` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `hopital`;

-- --------------------------------------------------------

--
-- Structure de la table `medecin`
--

CREATE TABLE `medecin` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `medecin`
--

INSERT INTO `medecin` (`id`, `libelle`, `code`) VALUES
(1, 'ahemed', '114'),
(2, 'ahmedd', '25'),
(3, 'aaaaaaaaa', 'aaaaaaaaa');

-- --------------------------------------------------------

--
-- Structure de la table `patient`
--

CREATE TABLE `patient` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `medecin` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `patient`
--

INSERT INTO `patient` (`id`, `nom`, `prenom`, `date`, `medecin`) VALUES
(3, 'zregtze', 'ze', '2012-01-01', 'aaaaaaaaa'),
(6, 'zregtze', 'dgzergzegzeg', '2012-01-01', 'aaaaaaaaa');

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `medecin`
--
ALTER TABLE `medecin`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables d�charg�es
--

--
-- AUTO_INCREMENT pour la table `medecin`
--
ALTER TABLE `medecin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `patient`
--
ALTER TABLE `patient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Base de donn�es :  `login`
--
CREATE DATABASE IF NOT EXISTS `login` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `login`;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `login` varchar(20) NOT NULL,
  `psw` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- D�chargement des donn�es de la table `user`
--

INSERT INTO `user` (`nom`, `prenom`, `login`, `psw`) VALUES
('ahmed', 'ben abdallah', 'd', ''),
('erg', 'zergz', 'd', 'sdv'),
('ahmed', 'aaaazda', 'zadd', 'azdza'),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', ''),
('', '', '', '');
--
-- Base de donn�es :  `majouli`
--
CREATE DATABASE IF NOT EXISTS `majouli` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `majouli`;

-- --------------------------------------------------------

--
-- Structure de la table `avis`
--

CREATE TABLE `avis` (
  `id` int(11) NOT NULL,
  `reponse` int(11) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `avis` double NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `baby`
--

CREATE TABLE `baby` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adrese` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `disponibilite` date NOT NULL,
  `idb` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prix` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `id_cat` int(11) DEFAULT NULL,
  `nom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nbr_publications` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `id` int(11) NOT NULL,
  `idp` int(11) NOT NULL,
  `idc` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prix` int(11) NOT NULL,
  `idf` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  `image` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `genre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `demande`
--

CREATE TABLE `demande` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adrese` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `disponibilite` date NOT NULL,
  `idb` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prix` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `evenement`
--

CREATE TABLE `evenement` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prix` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idu` int(11) NOT NULL,
  `particiate` int(11) NOT NULL,
  `datedeb` date NOT NULL,
  `datefin` date NOT NULL,
  `heuredeb` time NOT NULL,
  `heurefin` time NOT NULL,
  `nomorg` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adrese` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `nom`, `prenom`, `image`, `adrese`, `facebook_id`, `description`) VALUES
(1, 'am', 'am', 'am@gmail.com', 'am@gmail.com', 1, NULL, '$2y$13$xteqKugovx406lv3pgAyNOag5MkfVcLu19nFKDL2SNoJm9ajOHnwi', '2018-02-23 09:53:24', NULL, NULL, 'a:1:{i:0;s:17:\"ROLE_PROPRIETAIRE\";}', 'amal', 'amal', NULL, NULL, NULL, NULL),
(2, 'parent', 'parent', 'parent@gmail.com', 'parent@gmail.com', 1, NULL, '$2y$13$vzjtfDgei66O5MlkMXwqTONVtyzMyZbxFmFmulRF.DzYgkau5OgQK', '2018-02-23 09:52:13', NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_PARENT\";}', 'parent', 'parent', NULL, NULL, NULL, NULL),
(3, 'oo', 'oo', 'oumaimaest42@gmail.com', 'oumaimaest42@gmail.com', 1, NULL, '$2y$13$n1mxS4OwuzN0rLauDtd8E.G8JKb1HMrDOl59zn0pm/SixHxyNU9CK', '2018-02-24 14:07:10', NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_PARENT\";}', 'ooo', 'ooo', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `jarenfant`
--

CREATE TABLE `jarenfant` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `proprietaire` int(11) NOT NULL,
  `nbrnote` int(11) DEFAULT NULL,
  `note` int(11) DEFAULT NULL,
  `numtel` int(11) DEFAULT NULL,
  `adressemail` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `jarenfant`
--

INSERT INTO `jarenfant` (`id`, `nom`, `description`, `adresse`, `logo`, `proprietaire`, `nbrnote`, `note`, `numtel`, `adressemail`) VALUES
(2, 'tigerGarden', 'Has autem provincias, quas Orontes ambiens amnis imosque pedes Cassii montis illius celsi praetermeans funditur in Parthenium mare, Gnaeus Pompeius superato Tigrane regnis Armeniorum abstractas dicioni Romanae coniunxit.', 'ghazela 1002 tounes', 'images/551103-1TOqFD1502285018.jpg', 1, NULL, NULL, 21458800, 'tiger@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `id_recepteur` int(11) NOT NULL,
  `idEmetteur` int(11) NOT NULL,
  `objet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contenu_message` varchar(3000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `messages`
--

INSERT INTO `messages` (`id`, `id_recepteur`, `idEmetteur`, `objet`, `contenu_message`) VALUES
(1, 1, 2, 'contactez moi', 'rsgfqdg fdgfdgopfdqdmh qdfgmjfqdmlhqd hqfdhomjdfqmlhjfqd fqdhgofdmkh'),
(2, 1, 2, 'uskgdhdslkgjsdgf', 'fdgoifdhgfdqkjgdfq fqighfqdslng fgqfqdg'),
(3, 1, 2, 'contactez moi svp ', 'Has autem provincias, quas Orontes ambiens amnis imosque pedes Cassii montis illius celsi praetermeans funditur in Parthenium mare, Gnaeus Pompeius superato Tigrane regnis Armeniorum abstractas dicioni Romanae coniunxit.\r\n\r\n'),
(4, 1, 2, 'num tel invalide', 'Has autem provincias, quas Orontes ambiens amnis imosque pedes Cassii montis illius celsi praetermeans funditur in Parthenium mare, Gnaeus Pompeius superato Tigrane regnis Armeniorum abstractas dicioni Romanae coniunxit.\r\n\r\n'),
(5, 1, 1, 'test1', 'voila le test de cete envoi'),
(6, 1, 1, 'ahmed', 'haw ahmed je ba7dheya tawa tawa');

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE `produit` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prix` int(11) NOT NULL,
  `idf` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `image` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `genre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quantite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `produit_donation`
--

CREATE TABLE `produit_donation` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idu` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `datenow` date NOT NULL,
  `categorie` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nomuser` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `genre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `publication`
--

CREATE TABLE `publication` (
  `id` int(11) NOT NULL,
  `id_categorie` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `titre_qestion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nbr_vue` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `nbr_reponse` int(11) NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contenu` longtext COLLATE utf8_unicode_ci NOT NULL,
  `piece_jointe` varchar(5000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `reponse`
--

CREATE TABLE `reponse` (
  `id` int(11) NOT NULL,
  `id_publication` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `contenu` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `vues`
--

CREATE TABLE `vues` (
  `id` int(11) NOT NULL,
  `iduser` int(11) DEFAULT NULL,
  `idcategorie` int(11) DEFAULT NULL,
  `vue` int(11) NOT NULL,
  `idPublication` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `avis`
--
ALTER TABLE `avis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8F91ABF05FB6DEC7` (`reponse`),
  ADD KEY `IDX_8F91ABF08D93D649` (`user`);

--
-- Index pour la table `baby`
--
ALTER TABLE `baby`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_CB8C5497FAABF2` (`id_cat`);

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `demande`
--
ALTER TABLE `demande`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `evenement`
--
ALTER TABLE `evenement`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`);

--
-- Index pour la table `jarenfant`
--
ALTER TABLE `jarenfant`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `produit_donation`
--
ALTER TABLE `produit_donation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_AF3C6779C9486A13` (`id_categorie`),
  ADD KEY `IDX_AF3C67796B3CA4B` (`id_user`);

--
-- Index pour la table `reponse`
--
ALTER TABLE `reponse`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5FB6DEC7B72EAA8E` (`id_publication`),
  ADD KEY `IDX_5FB6DEC76B3CA4B` (`id_user`);

--
-- Index pour la table `vues`
--
ALTER TABLE `vues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_ECAC35835E5C27E9` (`iduser`),
  ADD KEY `IDX_ECAC3583EF619801` (`idPublication`),
  ADD KEY `IDX_ECAC358337667FC1` (`idcategorie`);

--
-- AUTO_INCREMENT pour les tables d�charg�es
--

--
-- AUTO_INCREMENT pour la table `avis`
--
ALTER TABLE `avis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `baby`
--
ALTER TABLE `baby`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `demande`
--
ALTER TABLE `demande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `evenement`
--
ALTER TABLE `evenement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `jarenfant`
--
ALTER TABLE `jarenfant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `produit`
--
ALTER TABLE `produit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `produit_donation`
--
ALTER TABLE `produit_donation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `publication`
--
ALTER TABLE `publication`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `reponse`
--
ALTER TABLE `reponse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `vues`
--
ALTER TABLE `vues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables d�charg�es
--

--
-- Contraintes pour la table `avis`
--
ALTER TABLE `avis`
  ADD CONSTRAINT `FK_8F91ABF05FB6DEC7` FOREIGN KEY (`reponse`) REFERENCES `reponse` (`id`),
  ADD CONSTRAINT `FK_8F91ABF08D93D649` FOREIGN KEY (`user`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD CONSTRAINT `FK_CB8C5497FAABF2` FOREIGN KEY (`id_cat`) REFERENCES `categorie` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `publication`
--
ALTER TABLE `publication`
  ADD CONSTRAINT `FK_AF3C67796B3CA4B` FOREIGN KEY (`id_user`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_AF3C6779C9486A13` FOREIGN KEY (`id_categorie`) REFERENCES `categorie` (`id`);

--
-- Contraintes pour la table `reponse`
--
ALTER TABLE `reponse`
  ADD CONSTRAINT `FK_5FB6DEC76B3CA4B` FOREIGN KEY (`id_user`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_5FB6DEC7B72EAA8E` FOREIGN KEY (`id_publication`) REFERENCES `publication` (`id`);

--
-- Contraintes pour la table `vues`
--
ALTER TABLE `vues`
  ADD CONSTRAINT `FK_ECAC358337667FC1` FOREIGN KEY (`idcategorie`) REFERENCES `categorie` (`id`),
  ADD CONSTRAINT `FK_ECAC35835E5C27E9` FOREIGN KEY (`iduser`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_ECAC3583EF619801` FOREIGN KEY (`idPublication`) REFERENCES `publication` (`id`);
--
-- Base de donn�es :  `parc`
--
CREATE DATABASE IF NOT EXISTS `parc` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `parc`;

-- --------------------------------------------------------

--
-- Structure de la table `modele`
--

CREATE TABLE `modele` (
  `id` int(11) NOT NULL,
  `lib` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pays` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `modele`
--

INSERT INTO `modele` (`id`, `lib`, `pays`) VALUES
(42, 'testadaadadad', 'tunisieefeffefgjtryj'),
(46, 'zzzz', 'Allemagne'),
(47, 'r\"�\"r�\"r�\"r�', 'Allemagne'),
(48, 'aaa', 'aaaa');

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE `produit` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prix` int(11) NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datenew` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `produit`
--

INSERT INTO `produit` (`id`, `nom`, `description`, `image`, `prix`, `prenom`, `datenew`) VALUES
(11, 'zzz', 'qqzzzq', 'uploads/images/vlcsnap-error888.png', 0, '', '0000-00-00 00:00:00'),
(12, 'aa', 'a', 'uploads/images/Capture.PNG', 47, '', '0000-00-00 00:00:00'),
(13, 'stock_yes', 'eeeee', 'uploads/images/2017-11-01 at 20-43-26.png', 0, '', '0000-00-00 00:00:00'),
(14, 'stock_yes', 'zeffzefeb', 'uploads/images/Capture.PNG', 0, '', '0000-00-00 00:00:00'),
(15, 'stock_backordered', 'zefdzefzef', 'uploads/images/Capture.PNG', 0, '', '0000-00-00 00:00:00'),
(16, 'stock_yes', 'azfda', 'uploads/images/Capture.PNG', 0, '', '0000-00-00 00:00:00'),
(17, 'stock_yes', 'dfgerg', 'uploads/images/2017-11-01 at 20-43-30.png', 0, NULL, '2018-02-11 05:53:19'),
(18, 'stock_yes', 'avvvv', 'uploads/images/vlcsnap-error888.png', 0, NULL, '2018-02-11 05:53:55');

-- --------------------------------------------------------

--
-- Structure de la table `voiture`
--

CREATE TABLE `voiture` (
  `id` int(11) NOT NULL,
  `serie` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DateMiseCirculation` date NOT NULL,
  `marque` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modele` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `voiture`
--

INSERT INTO `voiture` (`id`, `serie`, `DateMiseCirculation`, `marque`, `modele`) VALUES
(11, '1111', '2012-01-01', 'azd', 'testadaadadad');

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `modele`
--
ALTER TABLE `modele`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `voiture`
--
ALTER TABLE `voiture`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables d�charg�es
--

--
-- AUTO_INCREMENT pour la table `modele`
--
ALTER TABLE `modele`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT pour la table `produit`
--
ALTER TABLE `produit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `voiture`
--
ALTER TABLE `voiture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Base de donn�es :  `pers`
--
CREATE DATABASE IF NOT EXISTS `pers` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `pers`;

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

CREATE TABLE `personne` (
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `age` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Base de donn�es :  `phpmyadmin`
--
CREATE DATABASE IF NOT EXISTS `phpmyadmin` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `phpmyadmin`;

-- --------------------------------------------------------

--
-- Structure de la table `pma__bookmark`
--

CREATE TABLE `pma__bookmark` (
  `id` int(11) NOT NULL,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';

-- --------------------------------------------------------

--
-- Structure de la table `pma__central_columns`
--

CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_length` text COLLATE utf8_bin,
  `col_collation` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8_bin DEFAULT '',
  `col_default` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Central list of columns';

-- --------------------------------------------------------

--
-- Structure de la table `pma__column_info`
--

CREATE TABLE `pma__column_info` (
  `id` int(5) UNSIGNED NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';

--
-- D�chargement des donn�es de la table `pma__column_info`
--

INSERT INTO `pma__column_info` (`id`, `db_name`, `table_name`, `column_name`, `comment`, `mimetype`, `transformation`, `transformation_options`, `input_transformation`, `input_transformation_options`) VALUES
(1, 'hopital', '', '(db_comment)', 'DROP Hopital', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `pma__designer_settings`
--

CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `settings_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Settings related to Designer';

--
-- D�chargement des donn�es de la table `pma__designer_settings`
--

INSERT INTO `pma__designer_settings` (`username`, `settings_data`) VALUES
('root', '{\"angular_direct\":\"direct\",\"relation_lines\":\"true\",\"snap_to_grid\":\"off\"}');

-- --------------------------------------------------------

--
-- Structure de la table `pma__export_templates`
--

CREATE TABLE `pma__export_templates` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `template_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved export templates';

--
-- D�chargement des donn�es de la table `pma__export_templates`
--

INSERT INTO `pma__export_templates` (`id`, `username`, `export_type`, `template_name`, `template_data`) VALUES
(1, 'root', 'database', 'pidev', '{\"quick_or_custom\":\"quick\",\"what\":\"sql\",\"structure_or_data_forced\":\"0\",\"table_select[]\":[\"avis\",\"baby\",\"categorie\",\"commande\",\"comment\",\"demande\",\"evenement\",\"fos_user\",\"jarenfant\",\"mail\",\"message\",\"messages\",\"message_metadata\",\"notif\",\"participer\",\"produit\",\"produit_donation\",\"publication\",\"publicationresolu\",\"rating\",\"reponse\",\"thread\",\"thread_metadata\",\"vues\"],\"table_structure[]\":[\"avis\",\"baby\",\"categorie\",\"commande\",\"comment\",\"demande\",\"evenement\",\"fos_user\",\"jarenfant\",\"mail\",\"message\",\"messages\",\"message_metadata\",\"notif\",\"participer\",\"produit\",\"produit_donation\",\"publication\",\"publicationresolu\",\"rating\",\"reponse\",\"thread\",\"thread_metadata\",\"vues\"],\"table_data[]\":[\"avis\",\"baby\",\"categorie\",\"commande\",\"comment\",\"demande\",\"evenement\",\"fos_user\",\"jarenfant\",\"mail\",\"message\",\"messages\",\"message_metadata\",\"notif\",\"participer\",\"produit\",\"produit_donation\",\"publication\",\"publicationresolu\",\"rating\",\"reponse\",\"thread\",\"thread_metadata\",\"vues\"],\"aliases_new\":\"\",\"output_format\":\"sendit\",\"filename_template\":\"@DATABASE@\",\"remember_template\":\"on\",\"charset\":\"utf-8\",\"compression\":\"none\",\"maxsize\":\"\",\"codegen_structure_or_data\":\"data\",\"codegen_format\":\"0\",\"csv_separator\":\",\",\"csv_enclosed\":\"\\\"\",\"csv_escaped\":\"\\\"\",\"csv_terminated\":\"AUTO\",\"csv_null\":\"NULL\",\"csv_structure_or_data\":\"data\",\"excel_null\":\"NULL\",\"excel_columns\":\"something\",\"excel_edition\":\"win\",\"excel_structure_or_data\":\"data\",\"htmlword_structure_or_data\":\"structure_and_data\",\"htmlword_null\":\"NULL\",\"json_structure_or_data\":\"data\",\"latex_caption\":\"something\",\"latex_structure_or_data\":\"structure_and_data\",\"latex_structure_caption\":\"Structure de la table @TABLE@\",\"latex_structure_continued_caption\":\"Structure de la table @TABLE@ (suite)\",\"latex_structure_label\":\"tab:@TABLE@-structure\",\"latex_relation\":\"something\",\"latex_comments\":\"something\",\"latex_mime\":\"something\",\"latex_columns\":\"something\",\"latex_data_caption\":\"Contenu de la table @TABLE@\",\"latex_data_continued_caption\":\"Contenu de la table @TABLE@ (suite)\",\"latex_data_label\":\"tab:@TABLE@-data\",\"latex_null\":\"\\\\textit{NULL}\",\"mediawiki_structure_or_data\":\"structure_and_data\",\"mediawiki_caption\":\"something\",\"mediawiki_headers\":\"something\",\"ods_null\":\"NULL\",\"ods_structure_or_data\":\"data\",\"odt_structure_or_data\":\"structure_and_data\",\"odt_relation\":\"something\",\"odt_comments\":\"something\",\"odt_mime\":\"something\",\"odt_columns\":\"something\",\"odt_null\":\"NULL\",\"pdf_report_title\":\"\",\"pdf_structure_or_data\":\"structure_and_data\",\"phparray_structure_or_data\":\"data\",\"sql_include_comments\":\"something\",\"sql_header_comment\":\"\",\"sql_use_transaction\":\"something\",\"sql_compatibility\":\"NONE\",\"sql_structure_or_data\":\"structure_and_data\",\"sql_create_table\":\"something\",\"sql_auto_increment\":\"something\",\"sql_create_view\":\"something\",\"sql_procedure_function\":\"something\",\"sql_create_trigger\":\"something\",\"sql_backquotes\":\"something\",\"sql_type\":\"INSERT\",\"sql_insert_syntax\":\"both\",\"sql_max_query_size\":\"50000\",\"sql_hex_for_binary\":\"something\",\"sql_utc_time\":\"something\",\"texytext_structure_or_data\":\"structure_and_data\",\"texytext_null\":\"NULL\",\"xml_structure_or_data\":\"data\",\"xml_export_events\":\"something\",\"xml_export_functions\":\"something\",\"xml_export_procedures\":\"something\",\"xml_export_tables\":\"something\",\"xml_export_triggers\":\"something\",\"xml_export_views\":\"something\",\"xml_export_contents\":\"something\",\"yaml_structure_or_data\":\"data\",\"\":null,\"lock_tables\":null,\"as_separate_files\":null,\"csv_removeCRLF\":null,\"csv_columns\":null,\"excel_removeCRLF\":null,\"htmlword_columns\":null,\"json_pretty_print\":null,\"ods_columns\":null,\"sql_dates\":null,\"sql_relation\":null,\"sql_mime\":null,\"sql_disable_fk\":null,\"sql_views_as_tables\":null,\"sql_metadata\":null,\"sql_create_database\":null,\"sql_drop_table\":null,\"sql_if_not_exists\":null,\"sql_truncate\":null,\"sql_delayed\":null,\"sql_ignore\":null,\"texytext_columns\":null}');

-- --------------------------------------------------------

--
-- Structure de la table `pma__favorite`
--

CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Favorite tables';

-- --------------------------------------------------------

--
-- Structure de la table `pma__history`
--

CREATE TABLE `pma__history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sqlquery` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';

-- --------------------------------------------------------

--
-- Structure de la table `pma__navigationhiding`
--

CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';

-- --------------------------------------------------------

--
-- Structure de la table `pma__pdf_pages`
--

CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) UNSIGNED NOT NULL,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';

-- --------------------------------------------------------

--
-- Structure de la table `pma__recent`
--

CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

--
-- D�chargement des donn�es de la table `pma__recent`
--

INSERT INTO `pma__recent` (`username`, `tables`) VALUES
('root', '[{\"db\":\"pidev\",\"table\":\"evenement\"},{\"db\":\"pidev\",\"table\":\"participer\"},{\"db\":\"pidev\",\"table\":\"produit_donation\"},{\"db\":\"pidev\",\"table\":\"jarenfant\"},{\"db\":\"pidev\",\"table\":\"notif\"},{\"db\":\"pidev\",\"table\":\"fos_user\"},{\"db\":\"pidev\",\"table\":\"publication\"},{\"db\":\"3a22\",\"table\":\"personne\"},{\"db\":\"pidev\",\"table\":\"demande\"},{\"db\":\"pidev\",\"table\":\"mail\"}]');

-- --------------------------------------------------------

--
-- Structure de la table `pma__relation`
--

CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

-- --------------------------------------------------------

--
-- Structure de la table `pma__savedsearches`
--

CREATE TABLE `pma__savedsearches` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';

-- --------------------------------------------------------

--
-- Structure de la table `pma__table_coords`
--

CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT '0',
  `x` float UNSIGNED NOT NULL DEFAULT '0',
  `y` float UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Structure de la table `pma__table_info`
--

CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Structure de la table `pma__table_uiprefs`
--

CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

--
-- D�chargement des donn�es de la table `pma__table_uiprefs`
--

INSERT INTO `pma__table_uiprefs` (`username`, `db_name`, `table_name`, `prefs`, `last_update`) VALUES
('root', 'atelier_5_said', 'document', '{\"sorted_col\":\"`document`.`Reference`  ASC\"}', '2017-10-18 22:48:21'),
('root', 'login', 'user', '{\"sorted_col\":\"`user`.`psw`  DESC\"}', '2017-10-14 16:52:47'),
('root', 'parc', 'produit', '[]', '2018-02-11 04:53:05'),
('root', 'pidev', 'evenement', '[]', '2018-04-27 00:09:30'),
('root', 'pidev', 'participer', '{\"sorted_col\":\"`participer`.`description` ASC\"}', '2018-05-03 03:16:21'),
('root', 'pidev', 'produit_donation', '{\"sorted_col\":\"`produit_donation`.`datenow`  DESC\"}', '2018-05-02 16:17:46');

-- --------------------------------------------------------

--
-- Structure de la table `pma__tracking`
--

CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin,
  `data_sql` longtext COLLATE utf8_bin,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Structure de la table `pma__userconfig`
--

CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `config_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- D�chargement des donn�es de la table `pma__userconfig`
--

INSERT INTO `pma__userconfig` (`username`, `timevalue`, `config_data`) VALUES
('root', '2017-10-03 10:32:34', '{\"lang\":\"fr\",\"collation_connection\":\"utf8mb4_unicode_ci\"}');

-- --------------------------------------------------------

--
-- Structure de la table `pma__usergroups`
--

CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';

-- --------------------------------------------------------

--
-- Structure de la table `pma__users`
--

CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pma__central_columns`
--
ALTER TABLE `pma__central_columns`
  ADD PRIMARY KEY (`db_name`,`col_name`);

--
-- Index pour la table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`);

--
-- Index pour la table `pma__designer_settings`
--
ALTER TABLE `pma__designer_settings`
  ADD PRIMARY KEY (`username`);

--
-- Index pour la table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`);

--
-- Index pour la table `pma__favorite`
--
ALTER TABLE `pma__favorite`
  ADD PRIMARY KEY (`username`);

--
-- Index pour la table `pma__history`
--
ALTER TABLE `pma__history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`db`,`table`,`timevalue`);

--
-- Index pour la table `pma__navigationhiding`
--
ALTER TABLE `pma__navigationhiding`
  ADD PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`);

--
-- Index pour la table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  ADD PRIMARY KEY (`page_nr`),
  ADD KEY `db_name` (`db_name`);

--
-- Index pour la table `pma__recent`
--
ALTER TABLE `pma__recent`
  ADD PRIMARY KEY (`username`);

--
-- Index pour la table `pma__relation`
--
ALTER TABLE `pma__relation`
  ADD PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  ADD KEY `foreign_field` (`foreign_db`,`foreign_table`);

--
-- Index pour la table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`);

--
-- Index pour la table `pma__table_coords`
--
ALTER TABLE `pma__table_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`);

--
-- Index pour la table `pma__table_info`
--
ALTER TABLE `pma__table_info`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Index pour la table `pma__table_uiprefs`
--
ALTER TABLE `pma__table_uiprefs`
  ADD PRIMARY KEY (`username`,`db_name`,`table_name`);

--
-- Index pour la table `pma__tracking`
--
ALTER TABLE `pma__tracking`
  ADD PRIMARY KEY (`db_name`,`table_name`,`version`);

--
-- Index pour la table `pma__userconfig`
--
ALTER TABLE `pma__userconfig`
  ADD PRIMARY KEY (`username`);

--
-- Index pour la table `pma__usergroups`
--
ALTER TABLE `pma__usergroups`
  ADD PRIMARY KEY (`usergroup`,`tab`,`allowed`);

--
-- Index pour la table `pma__users`
--
ALTER TABLE `pma__users`
  ADD PRIMARY KEY (`username`,`usergroup`);

--
-- AUTO_INCREMENT pour les tables d�charg�es
--

--
-- AUTO_INCREMENT pour la table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `pma__history`
--
ALTER TABLE `pma__history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  MODIFY `page_nr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Base de donn�es :  `pidev`
--
CREATE DATABASE IF NOT EXISTS `pidev` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `pidev`;

-- --------------------------------------------------------

--
-- Structure de la table `avis`
--

CREATE TABLE `avis` (
  `id` int(11) NOT NULL,
  `reponse` int(11) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `avis` double NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `baby`
--

CREATE TABLE `baby` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adrese` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `disponibilite` date NOT NULL,
  `idb` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prix` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `baby`
--

INSERT INTO `baby` (`id`, `nom`, `prenom`, `adrese`, `disponibilite`, `idb`, `email`, `prix`, `description`, `image`) VALUES
(1, 'firas', 'firas', 'chez toi', '2013-01-01', 4, 'firas@gmail.com', 13, 'je suis toujours libre 24/24', 'images/20354075_1825173151127788_1252894543_o.jpg'),
(2, 'firas', 'firas', '65465465', '2013-01-01', 4, 'firas@gmail.com', 654654, '54+65+5', 'images/13329385_1603289976649441_990306885505882405_o.jpg'),
(3, 'firas', 'firas', '654654', '2013-01-01', 4, 'firas@gmail.com', 6546, 'qsdqs', 'images/firas.jpg'),
(4, 'firas', 'firas', '654654654', '2013-01-01', 4, 'firas@gmail.com', 54654, '56465456', 'images/IMG_20170503_090724.jpg'),
(5, 'firas', 'firas', '654654', '2013-01-01', 4, 'firas@gmail.com', 654654, '654654654', 'images/firas.jpeg'),
(6, 'ghada', 'ghada', '456', '2013-01-01', 7, '456@gmail.com', 456, '456', 'images/20354075_1825173151127788_1252894543_o.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `id_cat` int(11) DEFAULT NULL,
  `nom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nbr_publications` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `id` int(11) NOT NULL,
  `idp` int(11) NOT NULL,
  `idc` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prix` int(11) NOT NULL,
  `idf` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  `image` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `genre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `commande`
--

INSERT INTO `commande` (`id`, `idp`, `idc`, `nom`, `prix`, `idf`, `stock`, `quantite`, `image`, `description`, `age`, `genre`) VALUES
(1, 15, 4, '654654', 654654, 4, 6546546, 2, 'images/13329385_1603289976649441_990306885505882405_o.jpg', '65465465456', 564654654, 'Gar�on'),
(2, 16, 4, '321', 321, 4, 321321, 3, 'images/aa.jpg', 'mlkmlk', 321321, 'Gar�on'),
(3, 15, 6, '654654', 654654, 4, 6546546, 3, 'images/13329385_1603289976649441_990306885505882405_o.jpg', '65465465456', 564654654, 'Gar�on'),
(4, 14, 6, 'said', 2015, 4, 12234, 2, 'images/aa.jpg', 'qsdqsdqs', 56564231, 'Gar�on'),
(5, 16, 6, '321', 321, 4, 321321, 1, 'images/aa.jpg', 'mlkmlk', 321321, 'Gar�on'),
(6, 16, 4, '321', 321, 4, 321321, 1, 'images/aa.jpg', 'mlkmlk', 321321, 'Gar�on'),
(7, 15, 4, '654654', 654654, 4, 6546546, 2, 'images/13329385_1603289976649441_990306885505882405_o.jpg', '65465465456', 564654654, 'Gar�on'),
(8, 14, 4, 'said', 2015, 4, 12234, 3, 'images/aa.jpg', 'qsdqsdqs', 56564231, 'Gar�on');

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `produit_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `demande`
--

CREATE TABLE `demande` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `disponibilite` date NOT NULL,
  `idb` int(11) NOT NULL,
  `adrese` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prix` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `evenement`
--

CREATE TABLE `evenement` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prix` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datedeb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datefin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomorg` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idu` int(11) DEFAULT NULL,
  `particiate` int(11) DEFAULT '0',
  `lien` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `evenement`
--

INSERT INTO `evenement` (`id`, `nom`, `description`, `type`, `prix`, `datedeb`, `datefin`, `nomorg`, `adresse`, `image`, `idu`, `particiate`, `lien`) VALUES
(186, 'test2t', 'test2t', 'Gratuit ', '0', '2018-05-03', '2018-05-03', 'ahmed', 'test2t', '20180503105353.jpg', 1, 8, 'test2'),
(187, 'bbbbb', 'bbbbb', 'Gratuit ', '0', '2018-05-03', '2018-05-03', 'ahmed', 'bbbbbb', '/image/file.jpg', 1, 1, NULL),
(188, 'aaaaa', 'aaaa', 'Gratuit ', '0', '2018-05-03', '2018-05-03', 'maj', 'aaaaa', '/images/20180503103749.jpg', 3, 1, NULL),
(189, 'rrrr', 'rrrrr', 'Payant ', '3', '2018-05-03', '2018-05-03', 'firas', 'rrrr', '20180503105658.jpg', 4, 0, NULL),
(190, 'ppp', 'pppp', 'Gratuit ', '0', '2018-05-03', '2018-05-03', 'ahmed', 'ppppp', '20180503110012.jpg', 1, 0, NULL),
(191, 'rrr', 'rrrrr', 'Gratuit ', '0', '2018-05-03', '2018-05-03', 'ahmed', 'rrrr', '/images/20180503110406.jpg', 1, 0, NULL),
(192, 'image', 'images', 'Gratuit ', '0', '2018-05-03', '2018-05-03', 'ahmed', 'image', '/images/20180503110804.jpg', 1, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adrese` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `nom`, `prenom`, `image`, `adrese`, `description`, `facebook_id`, `google_id`) VALUES
(1, 'ahmed', 'ahmed', 'Ahmed.benabdallah@esprit.tn', 'ahmed.benabdallah@esprit.tn', 1, NULL, '$2y$13$LZ1mm9DuQmED94.CuZh.eOFm4cF8KmI/hOG2/X2eSeYzVwOF08S62', '2018-04-26 02:48:21', NULL, NULL, 'a:0:{}', '', '', '', '', '', '', ''),
(2, 'aa', 'aa', 'test@mail.com', 'test@mail.com', 1, NULL, '$2y$13$3MuysEdi2l2JkDRjdCJCze8kPN/QQZxfATpflEim1Q2JmERfkeUxW', '2018-02-24 14:03:57', NULL, NULL, 'a:0:{}', '', '', '', '', '', '', ''),
(3, 'maj', 'maj', 'majouli.med.aymen@gmail.com', 'majouli.med.aymen@gmail.com', 1, NULL, '$2y$13$a6GwqKPW67qG25WtAOnn8ekQlUWxkN.K0rOXY61EtBD4yDvIuBr9e', '2018-02-28 05:15:09', NULL, NULL, 'a:0:{}', 'maj', 'maj', '', '', '', '', ''),
(4, 'firas', 'firas', 'firas@gmail.com', 'firas@gmail.com', 1, NULL, '$2y$13$HW//7Q3hpMxvdKrNupN8d.fNmseWmxqrPiXqEBAhgrm9lfgZB4vYG', '2018-04-07 22:45:14', NULL, NULL, 'a:1:{i:0;s:12:\"ROLE_VENDEUR\";}', 'firas', 'firas', '', '', '', '', ''),
(5, 'ons', 'ons', 'ons@esprit.com', 'ons@esprit.com', 1, NULL, '$2y$13$Ty59YGBTWgPmLq7lyi86iup51dl1rAciOob87X5moneu.wObQqoQK', '2018-02-14 19:53:29', NULL, NULL, 'a:1:{i:0;s:12:\"ROLE_VENDEUR\";}', 'ons', 'grine', '', '', '', '', ''),
(6, '123', '123', '32132@gmail.com', '32132@gmail.com', 1, NULL, '$2y$13$8Avaut.IMS6eqtVv1iqW8eGPmB0w9RFZ7xGWhKBu/Pv/opEDN/NwG', '2018-02-15 02:29:34', NULL, NULL, 'a:1:{i:0;s:12:\"ROLE_VENDEUR\";}', '123', '123', '', '', '', '', ''),
(7, '12345', '12345', '123@gmail.com', '123@gmail.com', 1, NULL, '$2y$13$x6v1.igiDSrmMOQMXVKQmOUfgGfBtxFKA/5KKTmK53ZikJJYQ7PsS', '2018-02-16 10:16:53', NULL, NULL, 'a:1:{i:0;s:7:\"ROLE_BS\";}', '123', '123', '', '', '', '', ''),
(8, 'baby', 'baby', 'baby@gmail.com', 'baby@gmail.com', 1, NULL, '$2y$13$WSyXg8D59ase5BzsST0kd.zij5x74g07rtq.WES.Eh2n3fSDNRwN2', '2018-02-16 11:41:09', NULL, NULL, 'a:1:{i:0;s:16:\"ROLE_BABY_SITTER\";}', 'baby', 'baby', '', '', '', '', ''),
(10, 'moula', 'moula', 'moula@gmail.com', 'moula@gmail.com', 1, NULL, '$2y$13$Jxyj25AuWPqSBZx9RIEHLuPGmcPulHkY3AFRGr4XaNWZU1M68POdK', '2018-02-16 11:35:36', NULL, NULL, 'a:1:{i:0;s:17:\"ROLE_PROPRIETAIRE\";}', 'moula', 'moula', '', '', '', '', ''),
(11, 'said', 'said', 'aaa@gmail.comr', 'aaa@gmail.comr', 1, NULL, '$2y$13$Y9kVbBBO8S72zhjCrMUON.9um/z.JvAhmWrozDZVTIzk8.s0UieW.', '2018-04-07 17:23:54', NULL, NULL, 'a:1:{i:0;s:16:\"ROLE_BABY_SITTER\";}', 'rrrr', 'rr', NULL, NULL, NULL, NULL, NULL),
(12, 'admin', 'admin', 'admin@esprit.tn', 'admin@esprit.tn', 1, NULL, '$2y$13$1uaEitRehQDUixAZ7RKDXuHVQyZau2EtYEcRuQNu.loy583ODtrMy', '2018-04-10 10:58:56', NULL, NULL, 'a:1:{i:0;s:10:\"ROLE_ADMIN\";}', 'admin', 'admin', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `jarenfant`
--

CREATE TABLE `jarenfant` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `proprietaire` int(11) NOT NULL,
  `nbrnote` int(11) NOT NULL,
  `note` int(11) NOT NULL,
  `numtel` int(11) NOT NULL,
  `adressemail` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mail`
--

CREATE TABLE `mail` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `thread_id` int(11) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `id_recepteur` int(11) NOT NULL,
  `idEmetteur` int(11) NOT NULL,
  `objet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contenu_message` varchar(3000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `message_metadata`
--

CREATE TABLE `message_metadata` (
  `id` int(11) NOT NULL,
  `message_id` int(11) DEFAULT NULL,
  `participant_id` int(11) DEFAULT NULL,
  `is_read` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `notif`
--

CREATE TABLE `notif` (
  `id` int(11) NOT NULL,
  `nominviteur` varchar(200) NOT NULL,
  `idu` int(11) NOT NULL,
  `ide` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `imagee` varchar(200) NOT NULL,
  `adressee` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- D�chargement des donn�es de la table `notif`
--

INSERT INTO `notif` (`id`, `nominviteur`, `idu`, `ide`, `nome`, `imagee`, `adressee`) VALUES
(100, 'firas', 1, 90, 'tvalidation', '/images/7.jpg', 'test'),
(101, 'firas', 3, 90, 'tvalidation', '/images/7.jpg', 'test'),
(102, 'ahmed', 7, 91, 'sdvsdvsdvsd', '/images/d2.jpg', ' s d'),
(103, 'ahmed', 3, 91, 'sdvsdvsdvsd', '/images/d2.jpg', ' s d');

-- --------------------------------------------------------

--
-- Structure de la table `participer`
--

CREATE TABLE `participer` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prix` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idu` int(11) DEFAULT NULL,
  `particiate` int(11) DEFAULT NULL,
  `datedeb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datefin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomorg` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ide` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `participer`
--

INSERT INTO `participer` (`id`, `nom`, `description`, `type`, `prix`, `idu`, `particiate`, `datedeb`, `datefin`, `nomorg`, `adresse`, `image`, `ide`) VALUES
(601, 'test2t', 'test2t', 'Payant ', '33', 1, NULL, NULL, NULL, 'ahmed', 'test2t', '/image/temp350217704747086527..jpg', 186),
(602, 'test2t', 'test2t', 'Payant ', '33', 1, NULL, NULL, NULL, 'ahmed', 'test2t', '/image/temp350217704747086527..jpg', 186),
(603, 'test2t', 'test2t', 'Payant ', '33', 1, NULL, NULL, NULL, 'ahmed', 'test2t', '/image/temp350217704747086527..jpg', 186),
(604, 'test2t', 'test2t', 'Payant ', '33', 3, NULL, NULL, NULL, 'ahmed', 'test2t', '/image/temp350217704747086527..jpg', 186),
(607, 'test2t', 'test2t', 'Payant ', '33', 4, NULL, NULL, NULL, 'ahmed', 'test2t', '/image/temp350217704747086527..jpg', 186),
(608, 'bbbbb', 'bbbbb', 'Gratuit ', '0', 4, NULL, NULL, NULL, 'ahmed', 'bbbbbb', '/image/file.jpg', 187),
(609, 'aaaaa', 'aaaa', 'Gratuit ', '0', 1, NULL, NULL, NULL, 'maj', 'aaaaa', '/images/20180503103749.jpg', 188);

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE `produit` (
  `id` int(11) NOT NULL,
  `prix` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `age` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idf` int(11) NOT NULL,
  `genre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quantite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `produit`
--

INSERT INTO `produit` (`id`, `prix`, `stock`, `age`, `nom`, `image`, `description`, `idf`, `genre`, `quantite`) VALUES
(14, 2015, 12234, 56564231, 'said', 'images/aa.jpg', 'qsdqsdqs', 4, 'Gar�on', 0),
(15, 654654, 6546546, 564654654, '654654', 'images/13329385_1603289976649441_990306885505882405_o.jpg', '65465465456', 4, 'Gar�on', 0);

-- --------------------------------------------------------

--
-- Structure de la table `produit_donation`
--

CREATE TABLE `produit_donation` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idu` int(11) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datenow` date DEFAULT NULL,
  `categorie` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tel` int(11) DEFAULT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomuser` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `genre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `appro` int(11) DEFAULT NULL,
  `etat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nbj` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `produit_donation`
--

INSERT INTO `produit_donation` (`id`, `nom`, `idu`, `description`, `datenow`, `categorie`, `image`, `tel`, `adresse`, `nomuser`, `genre`, `appro`, `etat`, `nbj`) VALUES
(6, 'aaa', 1, 'aaaa', NULL, NULL, '/images/temp2353880665792774644..jpg', 8888, 'aaaa', 'ahmed', 'Fille', 0, 'en attente', 0),
(7, 'rrr', 4, 'rrr', NULL, NULL, '/images/file.jpg', 25252525, 'rrrr', 'firas', 'Fille', 0, 'en attente', 0),
(8, 'ahmed', 2, 'ahmed', NULL, NULL, '/images/file.jpg', 25252525, 'ahmed', 'ahmed', 'Fille', 0, 'en attente', 0),
(9, '<<<<<', 1, '<<<<', NULL, NULL, '/images/file.jpg', 25252525, '<<<', 'ahmed', 'Fille', 0, 'en attente', 0),
(10, 'ppppp', 3, 'ppppp', NULL, NULL, '/images/file.jpg', 25252525, 'pppp', 'maj', 'Fille', 0, 'en attente', 0);

-- --------------------------------------------------------

--
-- Structure de la table `publication`
--

CREATE TABLE `publication` (
  `id` int(11) NOT NULL,
  `id_categorie` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `titre_qestion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nbr_vue` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `nbr_reponse` int(11) NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contenu` longtext COLLATE utf8_unicode_ci NOT NULL,
  `piece_jointe` varchar(5000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `publicationresolu`
--

CREATE TABLE `publicationresolu` (
  `id` int(11) NOT NULL,
  `id_publication` int(11) DEFAULT NULL,
  `id_reponse` int(11) DEFAULT NULL,
  `id_pub` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `rating`
--

CREATE TABLE `rating` (
  `id` int(11) NOT NULL,
  `rating` int(11) DEFAULT NULL,
  `nom_produit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idproduit` int(11) NOT NULL,
  `idf` int(11) NOT NULL,
  `idc` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `reponse`
--

CREATE TABLE `reponse` (
  `id` int(11) NOT NULL,
  `id_publication` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `contenu` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `thread`
--

CREATE TABLE `thread` (
  `id` int(11) NOT NULL,
  `created_by_id` int(11) DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `is_spam` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `thread_metadata`
--

CREATE TABLE `thread_metadata` (
  `id` int(11) NOT NULL,
  `thread_id` int(11) DEFAULT NULL,
  `participant_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `last_participant_message_date` datetime DEFAULT NULL,
  `last_message_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `vues`
--

CREATE TABLE `vues` (
  `id` int(11) NOT NULL,
  `iduser` int(11) DEFAULT NULL,
  `idcategorie` int(11) DEFAULT NULL,
  `vue` int(11) NOT NULL,
  `idPublication` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `avis`
--
ALTER TABLE `avis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8F91ABF05FB6DEC7` (`reponse`),
  ADD KEY `IDX_8F91ABF08D93D649` (`user`);

--
-- Index pour la table `baby`
--
ALTER TABLE `baby`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_CB8C5497FAABF2` (`id_cat`);

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9474526CF347EFB` (`produit_id`),
  ADD KEY `IDX_9474526CA76ED395` (`user_id`);

--
-- Index pour la table `demande`
--
ALTER TABLE `demande`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `evenement`
--
ALTER TABLE `evenement`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`);

--
-- Index pour la table `jarenfant`
--
ALTER TABLE `jarenfant`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `mail`
--
ALTER TABLE `mail`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B6BD307FE2904019` (`thread_id`),
  ADD KEY `IDX_B6BD307FF624B39D` (`sender_id`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `message_metadata`
--
ALTER TABLE `message_metadata`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_4632F005537A1329` (`message_id`),
  ADD KEY `IDX_4632F0059D1C3019` (`participant_id`);

--
-- Index pour la table `notif`
--
ALTER TABLE `notif`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `participer`
--
ALTER TABLE `participer`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `produit_donation`
--
ALTER TABLE `produit_donation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_AF3C6779C9486A13` (`id_categorie`),
  ADD KEY `IDX_AF3C67796B3CA4B` (`id_user`);

--
-- Index pour la table `publicationresolu`
--
ALTER TABLE `publicationresolu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_1A3A7D33B72EAA8E` (`id_publication`),
  ADD UNIQUE KEY `UNIQ_1A3A7D33812B77B7` (`id_reponse`);

--
-- Index pour la table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `reponse`
--
ALTER TABLE `reponse`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5FB6DEC7B72EAA8E` (`id_publication`),
  ADD KEY `IDX_5FB6DEC76B3CA4B` (`id_user`);

--
-- Index pour la table `thread`
--
ALTER TABLE `thread`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_31204C83B03A8386` (`created_by_id`);

--
-- Index pour la table `thread_metadata`
--
ALTER TABLE `thread_metadata`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_40A577C8E2904019` (`thread_id`),
  ADD KEY `IDX_40A577C89D1C3019` (`participant_id`);

--
-- Index pour la table `vues`
--
ALTER TABLE `vues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_ECAC35835E5C27E9` (`iduser`),
  ADD KEY `IDX_ECAC3583EF619801` (`idPublication`),
  ADD KEY `IDX_ECAC358337667FC1` (`idcategorie`);

--
-- AUTO_INCREMENT pour les tables d�charg�es
--

--
-- AUTO_INCREMENT pour la table `avis`
--
ALTER TABLE `avis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `baby`
--
ALTER TABLE `baby`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `demande`
--
ALTER TABLE `demande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `evenement`
--
ALTER TABLE `evenement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193;

--
-- AUTO_INCREMENT pour la table `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `jarenfant`
--
ALTER TABLE `jarenfant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `mail`
--
ALTER TABLE `mail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `message_metadata`
--
ALTER TABLE `message_metadata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `notif`
--
ALTER TABLE `notif`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT pour la table `participer`
--
ALTER TABLE `participer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=611;

--
-- AUTO_INCREMENT pour la table `produit`
--
ALTER TABLE `produit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `produit_donation`
--
ALTER TABLE `produit_donation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `publication`
--
ALTER TABLE `publication`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `publicationresolu`
--
ALTER TABLE `publicationresolu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `reponse`
--
ALTER TABLE `reponse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `thread`
--
ALTER TABLE `thread`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `thread_metadata`
--
ALTER TABLE `thread_metadata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `vues`
--
ALTER TABLE `vues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables d�charg�es
--

--
-- Contraintes pour la table `avis`
--
ALTER TABLE `avis`
  ADD CONSTRAINT `FK_8F91ABF05FB6DEC7` FOREIGN KEY (`reponse`) REFERENCES `reponse` (`id`),
  ADD CONSTRAINT `FK_8F91ABF08D93D649` FOREIGN KEY (`user`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD CONSTRAINT `FK_CB8C5497FAABF2` FOREIGN KEY (`id_cat`) REFERENCES `categorie` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_9474526CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_9474526CF347EFB` FOREIGN KEY (`produit_id`) REFERENCES `produit` (`id`);

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `FK_B6BD307FE2904019` FOREIGN KEY (`thread_id`) REFERENCES `thread` (`id`),
  ADD CONSTRAINT `FK_B6BD307FF624B39D` FOREIGN KEY (`sender_id`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `message_metadata`
--
ALTER TABLE `message_metadata`
  ADD CONSTRAINT `FK_4632F005537A1329` FOREIGN KEY (`message_id`) REFERENCES `message` (`id`),
  ADD CONSTRAINT `FK_4632F0059D1C3019` FOREIGN KEY (`participant_id`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `publication`
--
ALTER TABLE `publication`
  ADD CONSTRAINT `FK_AF3C67796B3CA4B` FOREIGN KEY (`id_user`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_AF3C6779C9486A13` FOREIGN KEY (`id_categorie`) REFERENCES `categorie` (`id`);

--
-- Contraintes pour la table `publicationresolu`
--
ALTER TABLE `publicationresolu`
  ADD CONSTRAINT `FK_1A3A7D33812B77B7` FOREIGN KEY (`id_reponse`) REFERENCES `reponse` (`id`),
  ADD CONSTRAINT `FK_1A3A7D33B72EAA8E` FOREIGN KEY (`id_publication`) REFERENCES `publication` (`id`);

--
-- Contraintes pour la table `reponse`
--
ALTER TABLE `reponse`
  ADD CONSTRAINT `FK_5FB6DEC76B3CA4B` FOREIGN KEY (`id_user`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_5FB6DEC7B72EAA8E` FOREIGN KEY (`id_publication`) REFERENCES `publication` (`id`);

--
-- Contraintes pour la table `thread`
--
ALTER TABLE `thread`
  ADD CONSTRAINT `FK_31204C83B03A8386` FOREIGN KEY (`created_by_id`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `thread_metadata`
--
ALTER TABLE `thread_metadata`
  ADD CONSTRAINT `FK_40A577C89D1C3019` FOREIGN KEY (`participant_id`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_40A577C8E2904019` FOREIGN KEY (`thread_id`) REFERENCES `thread` (`id`);

--
-- Contraintes pour la table `vues`
--
ALTER TABLE `vues`
  ADD CONSTRAINT `FK_ECAC358337667FC1` FOREIGN KEY (`idcategorie`) REFERENCES `categorie` (`id`),
  ADD CONSTRAINT `FK_ECAC35835E5C27E9` FOREIGN KEY (`iduser`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_ECAC3583EF619801` FOREIGN KEY (`idPublication`) REFERENCES `publication` (`id`);
--
-- Base de donn�es :  `projetetudiant`
--
CREATE DATABASE IF NOT EXISTS `projetetudiant` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `projetetudiant`;

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

CREATE TABLE `etudiant` (
  `matricule` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `projet_id` int(11) DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `niveau` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `numclasse` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `etudiant`
--

INSERT INTO `etudiant` (`matricule`, `projet_id`, `nom`, `prenom`, `niveau`, `numclasse`) VALUES
('53863', 1, '863', '863', '3A', '36');

-- --------------------------------------------------------

--
-- Structure de la table `projet`
--

CREATE TABLE `projet` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `niveau` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `projet`
--

INSERT INTO `projet` (`id`, `titre`, `description`, `niveau`) VALUES
(1, 'dfged', 'gdf', '3A'),
(2, 'zefze', 'vvz', '4TWIN');

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD PRIMARY KEY (`matricule`),
  ADD KEY `IDX_717E22E3C18272` (`projet_id`);

--
-- Index pour la table `projet`
--
ALTER TABLE `projet`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables d�charg�es
--

--
-- AUTO_INCREMENT pour la table `projet`
--
ALTER TABLE `projet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables d�charg�es
--

--
-- Contraintes pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD CONSTRAINT `FK_717E22E3C18272` FOREIGN KEY (`projet_id`) REFERENCES `projet` (`id`);
--
-- Base de donn�es :  `recherche`
--
CREATE DATABASE IF NOT EXISTS `recherche` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `recherche`;

-- --------------------------------------------------------

--
-- Structure de la table `candidat`
--

CREATE TABLE `candidat` (
  `cin` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `score` int(11) NOT NULL,
  `motCle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `candidat`
--

INSERT INTO `candidat` (`cin`, `nom`, `prenom`, `score`, `motCle`) VALUES
(7777, '25', 'ahmed', 11, NULL),
(58778, 'said', 'ahmed', 11, 'c758498969'),
(77777, '25', 'ahmed', 11, NULL),
(222222, 'said', 'said', 2, NULL),
(58778555, 'said', 'aaaaaaaaaa', 3, NULL),
(58778585, '25', 'ahmed', 11, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sujet`
--

CREATE TABLE `sujet` (
  `reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nbpoint` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `sujet`
--

INSERT INTO `sujet` (`reference`, `titre`, `description`, `nbpoint`) VALUES
('c758498969', 'la reactivit�', 'les hydrizaine', 15),
('R2555586', 'reseau', 'reseuax internet ', 25),
('R74582', 'optimisation', 'les reseau Iotont la', 27);

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `candidat`
--
ALTER TABLE `candidat`
  ADD PRIMARY KEY (`cin`),
  ADD KEY `IDX_6AB5B4717CDBE895` (`motCle`);

--
-- Index pour la table `sujet`
--
ALTER TABLE `sujet`
  ADD PRIMARY KEY (`reference`);

--
-- Contraintes pour les tables d�charg�es
--

--
-- Contraintes pour la table `candidat`
--
ALTER TABLE `candidat`
  ADD CONSTRAINT `FK_6AB5B4717CDBE895` FOREIGN KEY (`motCle`) REFERENCES `sujet` (`reference`);
--
-- Base de donn�es :  `symfony`
--
CREATE DATABASE IF NOT EXISTS `symfony` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `symfony`;

-- --------------------------------------------------------

--
-- Structure de la table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `Nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DateDeb` date NOT NULL,
  `date_fin` datetime NOT NULL,
  `Lieu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nom_organisateur` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `HeureDeb` time NOT NULL,
  `HeureFin` time NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `participate` int(11) NOT NULL DEFAULT '0',
  `nbrparticipants` int(11) DEFAULT NULL,
  `idu` int(11) NOT NULL DEFAULT '0',
  `nomuser` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `modele`
--

CREATE TABLE `modele` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `modele`
--

INSERT INTO `modele` (`id`, `nom`, `prenom`) VALUES
(3, '', ''),
(4, '', '');

-- --------------------------------------------------------

--
-- Structure de la table `participer`
--

CREATE TABLE `participer` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idu` int(11) NOT NULL,
  `participate` int(11) NOT NULL,
  `ide` int(11) NOT NULL,
  `datedeb` date NOT NULL,
  `datefin` date NOT NULL,
  `heuredeb` time NOT NULL,
  `heurefin` time NOT NULL,
  `nomorganisateur` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lieu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `nomU` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenomU` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ageU` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user_e_event`
--

CREATE TABLE `user_e_event` (
  `user_e_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `voiture`
--

CREATE TABLE `voiture` (
  `id` int(11) NOT NULL,
  `serie` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Modele` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`);

--
-- Index pour la table `modele`
--
ALTER TABLE `modele`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `participer`
--
ALTER TABLE `participer`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_8D93D649C05FB297` (`confirmation_token`);

--
-- Index pour la table `user_e_event`
--
ALTER TABLE `user_e_event`
  ADD PRIMARY KEY (`user_e_id`,`event_id`),
  ADD KEY `IDX_1CBBE5AECE3D88C6` (`user_e_id`),
  ADD KEY `IDX_1CBBE5AE71F7E88B` (`event_id`);

--
-- Index pour la table `voiture`
--
ALTER TABLE `voiture`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables d�charg�es
--

--
-- AUTO_INCREMENT pour la table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `modele`
--
ALTER TABLE `modele`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `participer`
--
ALTER TABLE `participer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `voiture`
--
ALTER TABLE `voiture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables d�charg�es
--

--
-- Contraintes pour la table `user_e_event`
--
ALTER TABLE `user_e_event`
  ADD CONSTRAINT `FK_1CBBE5AE71F7E88B` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_1CBBE5AECE3D88C6` FOREIGN KEY (`user_e_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;
--
-- Base de donn�es :  `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;
--
-- Base de donn�es :  `test1`
--
CREATE DATABASE IF NOT EXISTS `test1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test1`;

-- --------------------------------------------------------

--
-- Structure de la table `vehicule`
--

CREATE TABLE `vehicule` (
  `matricule` varchar(20) NOT NULL,
  `date` int(11) NOT NULL,
  `prix` int(11) NOT NULL,
  `vitesse` int(11) NOT NULL,
  `nbr` int(11) DEFAULT NULL,
  `capacit�` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Base de donn�es :  `tp_pseudo`
--
CREATE DATABASE IF NOT EXISTS `tp_pseudo` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `tp_pseudo`;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `pseudo` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- D�chargement des donn�es de la table `utilisateur`
--

INSERT INTO `utilisateur` (`pseudo`) VALUES
('ahmed'),
('firas'),
('said ');
--
-- Base de donn�es :  `tunivision`
--
CREATE DATABASE IF NOT EXISTS `tunivision` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `tunivision`;

-- --------------------------------------------------------

--
-- Structure de la table `evenement`
--

CREATE TABLE `evenement` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `liex` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombreplace` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `evenement`
--

INSERT INTO `evenement` (`id`, `nom`, `type`, `date`, `liex`, `nombreplace`) VALUES
(1, '25', 'Musique', '2012-01-01', 'eryer', -1),
(2, 'theatre facefood', 'theatre', '2012-01-01', 'fertouna', 6);

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `evenements` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `reservation`
--

INSERT INTO `reservation` (`id`, `nom`, `prenom`, `email`, `tel`, `evenements`) VALUES
(1, '25', '558', 'htr@aaa.com', '252552', 1),
(2, '25', 'ahmed', 'htr@aaa.com', '252552', 1),
(3, 'gfjkl', 'ghvjbknl,m', 'htr@aaa.com', 'k', 1),
(4, 'gfjkl', 'ghvjbknl,m', 'htr@aaa.com', 'k', 1),
(5, 'gfjkl', 'ghvjbknl,m', 'htr@aaa.com', 'k', 1),
(6, 'eryer', 'erh', 'htr@aaa.com', 'rh', 1),
(7, 'adzazd', 'dazdazd', 'htr@aaaaaaaaaaaa.com', 'azdazda', 1),
(8, 'adzazd', 'dazdazd', 'htr@aaaaaaaaaaaa.com', 'azdazda', 1),
(9, '25', 'ahmed', 'htr@aaaaaaaaaaabbbbbbbbba.com', '523753737', 1),
(10, '25', 'ahmed', 'htr@aaaaaaaaaaabbbbbbbbba.com', 'rethryujtyjtyjte', 1),
(11, '25', 'ahmed', 'htr@aaaaaaaaaaaa.com', 'rethryujtyjtyjte', 1),
(12, '25', 'ahmed', 'htr@aaaaaaaaaaabbbbbbbbba.com', 'rethryujtyjtyjte', 1),
(13, 'eeeeeeeeeeeeeeeeeeee', 'eeeeeeeeeeeeeee', 'htr@eeeeeeeeeeeeeee.com', 'eeeeeeeeeeeeeeeeeeeeeeeee', 1),
(14, 'eeeeeeeeeeeeeeeeeeee', 'eeeeeeeeeeeeeee', 'htr@eeeeeeeeeeeeeee.com', 'eeeeeeeeeeeeeeeeeeeeeeeee', 1),
(15, 'eeeeeeeeeeeeeeeeeeee', 'eeeeeeeeeeeeeee', 'htr@eeeeeeeeeeeeeee.com', 'eeeeeeeeeeeeeeeeeeeeeeeee', 1),
(16, 'eeeeeeeeeeeeeeeeeeee', 'eeeeeeeeeeeeeee', 'htr@eeeeeeeeeeeeeee.com', 'eeeeeeeeeeeeeeeeeeeeeeeee', 1),
(17, '25', 'ahmed', 'htr@aaa.com', '252552', 1),
(18, '25', 'ahmed', 'htr@aaa.com', '252552', 1),
(19, '25', 'ahmed', 'htr@aaa.com', '252552', 1);

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `evenement`
--
ALTER TABLE `evenement`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_42C84955E10AD400` (`evenements`);

--
-- AUTO_INCREMENT pour les tables d�charg�es
--

--
-- AUTO_INCREMENT pour la table `evenement`
--
ALTER TABLE `evenement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Contraintes pour les tables d�charg�es
--

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `FK_42C84955E10AD400` FOREIGN KEY (`evenements`) REFERENCES `evenement` (`id`);
--
-- Base de donn�es :  `voyage`
--
CREATE DATABASE IF NOT EXISTS `voyage` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `voyage`;

-- --------------------------------------------------------

--
-- Structure de la table `offre`
--

CREATE TABLE `offre` (
  `code` int(11) NOT NULL,
  `libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pourcentage` int(11) NOT NULL,
  `destination` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `offre`
--

INSERT INTO `offre` (`code`, `libelle`, `pourcentage`, `destination`) VALUES
(1, 'sdf', 10, 'dqsfdqzfzq');

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE `reservation` (
  `numero` int(11) NOT NULL,
  `offre` int(11) DEFAULT NULL,
  `depart` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `destination` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datedepart` date NOT NULL,
  `voyageur` int(11) NOT NULL,
  `tarif` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `classe` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- D�chargement des donn�es de la table `reservation`
--

INSERT INTO `reservation` (`numero`, `offre`, `depart`, `destination`, `datedepart`, `voyageur`, `tarif`, `classe`) VALUES
(1, 1, 'Tunis', 'dqsfdqzfzq', '2012-01-01', 5, '1', 'economique'),
(2, 1, 'Tunis', 'dqsfdqzfzq', '2012-01-01', 5, '0.75', 'economique');

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `offre`
--
ALTER TABLE `offre`
  ADD PRIMARY KEY (`code`);

--
-- Index pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`numero`),
  ADD KEY `IDX_42C84955AF86866F` (`offre`);

--
-- AUTO_INCREMENT pour les tables d�charg�es
--

--
-- AUTO_INCREMENT pour la table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `numero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables d�charg�es
--

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `FK_42C84955AF86866F` FOREIGN KEY (`offre`) REFERENCES `offre` (`code`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
