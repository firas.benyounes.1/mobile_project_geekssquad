/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.allforkids.Service;
import com.allforkids.Entite.Reponse;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;



/**
 *
 * @author ikbel
 */
public class CommentaireService 
{
    private ConnectionRequest con;
    private String url;
    
    
    public ArrayList<Reponse> getReponses(int idpubl)
    {
        ArrayList<Reponse> listereponses=new ArrayList();
        
        con=new ConnectionRequest();
        con.setUrl("http://localhost/datatable_21/web/app_dev.php/home/commentss/pub/"+idpubl);
        
     
       con.addResponseListener(e->{
       
           JSONParser jsp=new JSONParser();
           JSONParser jsp2=new JSONParser();
            try {
                
                Map<String,Object> reponses=jsp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray() ));
                
                //System.out.println(reponses);
                List<Map<String, Object>> list = (List<Map<String, Object>>) reponses.get("root");
                
                for(Map<String,Object> obj: list)
                {
                    Reponse r=new Reponse();
                    
                    float  idreponse=Float.parseFloat(obj.get("id").toString());
                    String contenu=obj.get("contenu").toString();
                    
                     Map<String,Object> objuser=(Map<String,Object>)obj.get("idUser");
                Map<String,Object> tuser=jsp2.parseJSON(new CharArrayReader(new String(JSONParser.mapToJson(objuser)).toCharArray() ));
  
                float iduser=Float.parseFloat(tuser.get("id").toString());
                String nomuser=tuser.get("nom").toString()+" "+tuser.get("prenom").toString();
                System.out.println(nomuser);
              
               
                
          
   
  
  
       r.setId((int)idreponse);
       r.setId_user((int)iduser);
       r.setNomuser(nomuser);
       r.setContenu(contenu);
         listereponses.add(r);
                }
            } 
            catch (IOException ex) {
                
                System.out.println(ex);
            }
           
       });
       
        
        NetworkManager.getInstance().addToQueueAndWait(con);
       return listereponses; 
    }
         public void ajoutReponse(Reponse r) {
        ConnectionRequest con = new ConnectionRequest();   
        
        String Url = "http://localhost/datatable_21/web/app_dev.php/home/adjs/repdmd" +"?publication="+ r.getId_publication()+"&user="+r.getId_user()+ "&avis=" + r.getAvis()+"&contenu=" + r.getContenu();
        con.setUrl(Url);
        con.addArgument(Url, Url);
        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
}

