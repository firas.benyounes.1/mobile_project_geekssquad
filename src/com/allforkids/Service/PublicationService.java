/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allforkids.Service;

import com.allforkids.Entite.Categorie;
import com.allforkids.Entite.Publication;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;


import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;



/**
 *
 * @author ikbel
 */
public class PublicationService 
{
    private ConnectionRequest con;
    private String url;
    
    
    public ArrayList<Publication> getPublications()
    {
        ArrayList<Publication> listepublications=new ArrayList();
        
        con=new ConnectionRequest();
        con.setUrl("http://localhost/datatable_21/web/app_dev.php/home/pubs/alljs");
        
     
       con.addResponseListener(e->{
       
           JSONParser jsp=new JSONParser();
           JSONParser jsp2=new JSONParser();
            try {
                
                Map<String,Object> publications=jsp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray() ));
                
                //System.out.println(publications);
                List<Map<String, Object>> list = (List<Map<String, Object>>) publications.get("root");
                
                for(Map<String,Object> obj: list)
                {
                    Publication p=new Publication();
                    
                    float  idpublication=Float.parseFloat(obj.get("id").toString());
                    float  resolu;
                    String resol;
                    if((obj.get("resolu"))!= null){
                    resolu=Float.parseFloat(obj.get("resolu").toString());
                    resol="résolue";
                    }
                    else
                    {resolu=0;
                    resol="non résolue";
                    }
                    float  nbrreponse=Float.parseFloat(obj.get("nbrReponse").toString());
                    String titre=obj.get("titre").toString();
                    String contenu=obj.get("contenu").toString();
                    
                     Map<String,Object> objuser=(Map<String,Object>)obj.get("iduser");
                Map<String,Object> tuser=jsp2.parseJSON(new CharArrayReader(new String(JSONParser.mapToJson(objuser)).toCharArray() ));
  
                float iduser=Float.parseFloat(tuser.get("id").toString());
                String nomuser=tuser.get("username").toString()+" "+tuser.get("prenom").toString();
                    System.out.println(nomuser);
               
                 Map<String,Object> objcategorie=(Map<String,Object>)obj.get("idCategorie");
                 
          Map<String,Object> tcategorie=jsp2.parseJSON(new CharArrayReader(new String(JSONParser.mapToJson(objcategorie)).toCharArray() ));

                float idCategorie=Float.parseFloat(tcategorie.get("id").toString());
                String nomcategorie=tcategorie.get("nom").toString();
                
               
                
          
   
  
  
       p.setId((int)idpublication);
       p.setTitre(titre);
       p.setId_categorie((int)idCategorie);
       p.setIduser((int)iduser);
       p.setNbrReponse((int)nbrreponse);
       p.setNomcategorie(nomcategorie);
       p.setNomuser(nomuser);
       p.setContenu(contenu);
       p.setResolu((int)resolu);
       p.setResol(resol);
         listepublications.add(p);
                }
            } 
            catch (IOException ex) {
                
                System.out.println(ex);
            }
           
       });
       
        
        NetworkManager.getInstance().addToQueueAndWait(con);
       return listepublications; 
    }
    public List<Categorie> getCategories()
    {
        List<Categorie> listecategories=new ArrayList();
        
        con=new ConnectionRequest();
        con.setUrl("http://localhost/datatable_21/web/app_dev.php/home/categs/alljs");
        
     
       con.addResponseListener(e->{
       
           JSONParser jsp=new JSONParser();
           JSONParser jsp2=new JSONParser();
            try {
                
                Map<String,Object> categories=jsp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray() ));
              
                List<Map<String, Object>> list = (List<Map<String, Object>>) categories.get("root");
                
                for(Map<String,Object> obj: list)
                {
                    Categorie c=new Categorie();
                    
                    float  idcategorie=Float.parseFloat(obj.get("id").toString());
                    String nomcat=obj.get("nom").toString();
                    
                 
       c.setId((int)idcategorie);
       c.setNom(nomcat);
      
         listecategories.add(c);
                }
            } 
            catch (IOException ex) {
                
                System.out.println(ex);
            }
           
       });
       
       
        NetworkManager.getInstance().addToQueueAndWait(con);
       return listecategories; 
    }
      public void ajoutPublication(Publication p) {
        ConnectionRequest con = new ConnectionRequest();   
        
        String Url = "http://localhost/datatable_21/web/app_dev.php/home/adjs/pubdmd" +"?categorie="+p.getIdcategorie() +"&user="+p.getIduser()+ "&titre=" + p.getTitre()+ "&vues=" + p.getNbr_vue()+ "&created=" + p.getCreated()+ "&location="+ p.getLocation()+ "&contenu=" + p.getContenu()+ "&reponses=" + p.getNbrReponse();
          
        con.setUrl(Url);
        con.addArgument(Url, Url);
        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
    
}

