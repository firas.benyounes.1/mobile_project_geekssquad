/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allforkids.Service;


import com.allforkids.Entite.Produit;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author said
 */
public class ProduitService {
    
    int id=Produit.getId_courant();
    
    public ArrayList<Produit> getListProduit() {
        ArrayList<Produit> listproduit = new ArrayList<>();
           ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/pidev/web/app_dev.php/boutique/mobileAllProduct");
        
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                
                try {
                    Map<String, Object> tasks = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                
                    List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");
                    for (Map<String, Object> result : list) {
                        Produit p = new Produit();
                        float id = Float.parseFloat(result.get("id").toString());
                        p.setId((int) id);
             float age=Float.parseFloat(result.get("age").toString());
                    p.setAge((int)age);
             float quantite=Float.parseFloat(result.get("quantite").toString());
                    p.setQuantite((int)quantite);
             float stock=Float.parseFloat(result.get("stock").toString());
                    p.setStock((int)stock);
                   
             float prix=Float.parseFloat(result.get("prix").toString());
                    p.setPrix((int)prix);
              
            p.setNom(result.get("nom").toString());
            p.setCategorie(result.get("categorie").toString());
            p.setDescription(result.get("description").toString());
            p.setGenre(result.get("genre").toString());
            p.setImage(result.get("image").toString());
                       
            listproduit.add(p);
                                

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        System.out.println("list "+listproduit);
        return listproduit;
         
    }
    
    int quick_id=Produit.getId_courant();
    public ArrayList<Produit> quickviewProduit(int quick_id) {
        ArrayList<Produit> listproduit2 = new ArrayList<>();
           ConnectionRequest con2 = new ConnectionRequest();
        con2.setUrl("http://localhost/pidev/web/app_dev.php/boutique/quickviewMobile/"+quick_id+"");
        System.out.println("said "+Produit.getId_courant());
        System.out.println(con2.getUrl());
        con2.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                
                try {
                    
                    Map<String, Object> tasks = jsonp.parseJSON(new CharArrayReader(new String(con2.getResponseData()).toCharArray()));
                
                    List<Map<String, Object>> list2 = (List<Map<String, Object>>) tasks.get("root");
                    for (Map<String, Object> result : list2) {
                        System.out.println("aaaaaaaaaaaaaannnnnnnnnnnnnnnn");
                        Produit p2 = new Produit();
                        float id = Float.parseFloat(result.get("id").toString());
                        p2.setId((int) id);
             float age=Float.parseFloat(result.get("age").toString());
                    p2.setAge((int)age);
             float quantite=Float.parseFloat(result.get("quantite").toString());
                    p2.setQuantite((int)quantite);
             float stock=Float.parseFloat(result.get("stock").toString());
                    p2.setStock((int)stock);
                   
             float prix=Float.parseFloat(result.get("prix").toString());
                    p2.setPrix((int)prix);
              
            p2.setNom(result.get("nom").toString());
            p2.setCategorie(result.get("categorie").toString());
            p2.setDescription(result.get("description").toString());
            p2.setGenre(result.get("genre").toString());
            p2.setImage(result.get("image").toString());
                                   System.out.println("sayeb zebi "+p2);  
            listproduit2.add(p2);
                                

                    }
                } catch (IOException ex) {
                    
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con2);
        System.out.println("list said "+listproduit2);
        return listproduit2;
         
    }
    
}
