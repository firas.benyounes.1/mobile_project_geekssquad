/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codename1.uikit.materialscreens;

import com.allforkids.Entite.Reponse;
import com.allforkids.Service.CommentaireService;
import com.codename1.components.ImageViewer;
import com.codename1.io.Log;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.table.TableLayout;
import com.codename1.ui.util.Resources;



import java.util.List;

/**
 *
 * @author ikbel
 */
public class CommentaireInterface 
{
 
    private Form f1;
    private Form f3;
    private Label lbc;
    private Label lbu;
    private Resources theme;
    private TextField redac;
    private Button ajp;
    
    public CommentaireInterface()
    {
        theme = UIManager.initFirstTheme("/theme");
            Toolbar.setGlobalToolbar(true);
        Log.bindCrashProtection(true);
        
        f1=new Form("Liste des commentaires",new TableLayout(1,1));
       
        f3=new Form();
        
        f1.getToolbar().addCommandToRightBar("Retour", null, e->{ 
        
                        System.out.println("okkkkkkkk");
                      
            InterfaceAcceuil im=new InterfaceAcceuil();
              im.getF().show();
        });
    }
    
    public Container AddReponsetocontainer(Reponse r)
    {
     Container cprincipale=new Container(BoxLayout.y());
         
     String rr=String.valueOf(r.getContenu());
     lbc=new Label("Commentaire :"+rr);
     lbu=new Label("publié par: "+r.getNomuser());
     cprincipale.add(lbc);
     cprincipale.add(lbu);
     return cprincipale;
    }
    
    public void AfficherReponse(int ip)
    {
        CommentaireService rs=new CommentaireService();
        List<Reponse> lsr=rs.getReponses(ip);
        
        redac=new TextField();
        
        redac.setHint("Rédiger votre commentaire ici...");
        int i=0;
       
        for(Reponse r: lsr)
        {
        i++;
            if(i<=9)
            {
                f1.add(AddReponsetocontainer(r));
                
            }
          }
           f1.add(redac);
           ajp = new Button("Ajouter");
        ajp.setWidth(20);
        ajp.addActionListener(e->{
           CommentaireService csrvc= new CommentaireService();
           String conten=redac.getText();
           Reponse rep = new Reponse(conten,ip , LoginForm.UserConnected.getId(), 0);
           csrvc.ajoutReponse(rep);
           CommentaireInterface ci=new CommentaireInterface();
           ci.getF1(ip).show();
                 });
          f1.add(ajp);
        }
        
    

    public Form getF1(int ip) {
       AfficherReponse(ip);
        return f1;
    }

    public void setF1(Form f1) {
        this.f1 = f1;
    }

   

    public Form getF3() {
        return f1;
    }

    public void setF3(Form f3) {
        this.f3 = f3;
    }
    
    
    
    
    
    
    
}
