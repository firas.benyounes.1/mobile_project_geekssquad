/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codename1.uikit.materialscreens;

import com.allforkids.Entite.evenement;
import com.allforkids.Service.ServiceEvenement;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.ImageViewer;
import com.codename1.components.SignatureComponent;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.FileSystemStorage;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import static com.codename1.ui.Component.LEFT;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.RadioButton;
import com.codename1.ui.Slider;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import com.codename1.ui.validation.LengthConstraint;
import com.codename1.ui.validation.Validator;
import java.io.IOException;
import java.util.Date;



public class upda extends Form{
    
    Image img = null;
    Image toupload = null;
    Image profilePic = null;
    Label profile = null;
    ImageViewer image = new ImageViewer();
    Slider starRank = new Slider();
    static int SR;
     String type="";
  int prixx;
    evenement e=MyListEv.specDetails;
     private Resources themee = UIManager.initFirstTheme("/theme");
    public upda() {
        this(com.codename1.ui.util.Resources.getGlobalResources());
    }

    
 
    
    public upda(Resources theme) {

        super(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER_ABSOLUTE));
        setUIID("LoginForm");
        
        try {
           // LoginForm.setTheme(theme.openLayered("/theme"));
        } catch (Exception e) {
            System.out.println(e);
        }
        Container welcome = FlowLayout.encloseCenter(
                new Label("Ajouter", "WelcomeRed")
        );
        Button register = new Button("Modifier");
        FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_IMAGE);

        Image profilePic = theme.getImage("user-picture.jpg");
        Image mask = theme.getImage("round-mask.png");
        profilePic = profilePic.fill(mask.getWidth(), mask.getHeight());

        profile = new Label();
        fab.addActionListener((ActionListener) (ActionEvent evt) -> {
            Display.getInstance().openGallery((ActionListener) (ActionEvent ev) -> {
                if (ev != null && ev.getSource() != null) {
                    String filePath = (String) ev.getSource();
                    int fileNameIndex = filePath.lastIndexOf("/") + 1;
                    String fileName = filePath.substring(fileNameIndex);
                    
                    img = null;
                    toupload = null;
                    
                    try {
                        
                        img = Image.createImage(FileSystemStorage.getInstance().openInputStream(filePath));
                        toupload = img;
                        img.fill(mask.getWidth(), mask.getHeight());
                        profile.setIcon(img);
                        profile.setUIID("ProfilePicTitle");
                        profile.setMask(mask.createMask());
                        refreshTheme();
                    } catch (IOException e) {
                    }
                    
                }
            }, Display.GALLERY_IMAGE);
        });

        getTitleArea().setUIID("Container");
 Picker date = new Picker();
date.setType(Display.PICKER_TYPE_DATE);        

       

        Image logo = theme.getImage("logo.png");

        //    Image mask = theme.getImage("round-mask.png");
        //    logo = logo.fill(mask.getWidth(), mask.getHeight());
        Label profilePicLabel = new Label(logo, "ProfilePic");

        //   profilePicLabel.setMask(mask.createMask());
        TextField tfDep = new TextField("", e.getNom(), 20, 0);
        TextField tfArr = new TextField("", e.getAdresse(), 20, 0);
        TextField tfDescription = new TextField("", e.getDescription(), 20, 0);
            TextField Description = new TextField("", "Description", 20, 0);
        TextField tfNbr = new TextField("", String.valueOf(e.getPrix()), 20, 0);
//        TextField rating = new TextField("", "Rating", 20, 0);

       
       
        
        Validator v = new Validator();
        v.addConstraint(tfDep, new LengthConstraint(2)).
                addConstraint(tfArr, new LengthConstraint(2)).
                addConstraint(tfDescription, new LengthConstraint(2)).
                addConstraint(tfNbr, new LengthConstraint(1));
//                addConstraint(etat, new LengthConstraint(2));                
        // v.addSubmitButtons(register);
        tfDep.getAllStyles().setMargin(LEFT, 0);
        tfArr.getAllStyles().setMargin(LEFT, 0);
        tfDescription.getAllStyles().setMargin(LEFT, 0);
        tfNbr.getAllStyles().setMargin(LEFT, 0);
//        etat.getAllStyles().setMargin(LEFT, 0);
//        rating.getAllStyles().setMargin(LEFT, 0);

        Label nomicon = new Label("", "TextField");
        Label Descriptionicon = new Label("", "TextField");
        Label Imageicon = new Label("", "TextField");
        Label Prixicon = new Label("", "TextField");
          Label datee = new Label("", "TextField");
        Label etaticon = new Label("", "TextField");
        Label ratingicon = new Label("", "TextField");

        Picker datedeb = new Picker();
datedeb.setType(Display.PICKER_TYPE_DATE);

       

      Picker datefin = new Picker();
datefin.setType(Display.PICKER_TYPE_DATE);
        
        
        
        
        
        nomicon.getAllStyles().setMargin(RIGHT, 0);
        Descriptionicon.getAllStyles().setMargin(RIGHT, 0);
        Imageicon.getAllStyles().setMargin(RIGHT, 0);
        Prixicon.getAllStyles().setMargin(RIGHT, 0);
        etaticon.getAllStyles().setMargin(RIGHT, 0);
        ratingicon.getAllStyles().setMargin(RIGHT, 0);

        FontImage.setMaterialIcon(nomicon, FontImage.MATERIAL_PERSON_OUTLINE, 3);
        FontImage.setMaterialIcon(Descriptionicon, FontImage.MATERIAL_PERSON_OUTLINE, 3);
        FontImage.setMaterialIcon(Imageicon, FontImage.MATERIAL_ACCESS_ALARMS, 3);
        FontImage.setMaterialIcon(Prixicon, FontImage.MATERIAL_PERSON_OUTLINE, 3);
        FontImage.setMaterialIcon(datee, FontImage.MATERIAL_ACCESS_ALARMS, 3);
        FontImage.setMaterialIcon(ratingicon, FontImage.MATERIAL_FACE, 3);

        
        
        
        
        
        
        
        
/*Button*/
//        StringBuffer str = new StringBuffer();
//        ConnectionRequest con = new ConnectionRequest();
//        register.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent evt) {
//                               
//                if (v.isValid()) {
//         System.out.println(Integer.parseInt(tfNbr.getText()));
//
//                    con.setPost(true);
//                    
////                        
//        con.setUrl("http://localhost/datatable_21/web/app_dev.php/home/AddEv?nom="+tfDep.getText()+"&prenom="+tfArr.getText()+ "&description=" + tfDescription.getText() + 
//                 "&prix=" +Integer.parseInt(tfNbr.getText())+ "&disponibilite=" + date.getText());
//
//      
//                    con.addRequestHeader("Content-Type", "application/x-www-form-urlencoded");
//                    con.addResponseListener(new ActionListener<NetworkEvent>() {
//                        @Override
//                        public void actionPerformed(NetworkEvent evt) {
//
//       
//                               Dialog.show("Felicitation", "Ajout effectué avec succes", "Voir vos Affiches", null);
//
//                                Toolbar.setGlobalToolbar(false);
//                               // new MyListBaby(theme).show();
//                                Toolbar.setGlobalToolbar(true);
//                                refreshTheme();
//
//                          
//                                
//                        }
//                    });
//                    NetworkManager.getInstance().addToQueue(con);
//
//                } else {
//                    Dialog.show("Humm", "Vérifiez vos informations...", "J'ai compris", null);
//                             //  System.out.println("Kais Rating "+starRank.getProgress());
//
//                }
//            }
//        });

        Button goBackToLogin = new Button(" << ");
        goBackToLogin.setUIID("StasForm");
        goBackToLogin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
            Toolbar.setGlobalToolbar(false);
               new MyListEv(themee).show();

                Toolbar.setGlobalToolbar(true);
            }
        });
        // We remove the extra space for low resolution devices so things fit better
        Label spaceLabel;
        if (!Display.getInstance().isTablet() && Display.getInstance().getDeviceDensity() < Display.DENSITY_VERY_HIGH) {
            spaceLabel = new Label();
        } else {
            spaceLabel = new Label(" ");
        }

        Container by = BoxLayout.encloseY(
                welcome,
                spaceLabel,
                profile,
                fab,
                BorderLayout.center(tfDep).
                add(BorderLayout.WEST, nomicon),
                BorderLayout.center(tfArr).
                add(BorderLayout.WEST, Descriptionicon),
                BorderLayout.center(datefin).
               
                      
                add(BorderLayout.WEST, Imageicon),
                BorderLayout.center(datedeb).
                add(BorderLayout.WEST, datee),
            
                BorderLayout.center(tfDescription).
                add(BorderLayout.WEST, etaticon)
          
              
                
                
        );
        
          RadioButton rb1 = new RadioButton("Gratuit");
       rb1.setSelected(true);
RadioButton rb2 = new RadioButton("Payant");
        

rb1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                rb2.setSelected(false);
           
            }
        });
rb2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                rb1.setSelected(false);
                String Payant;
             Payant="Payant";
               
                System.out.println(Payant);
               
            }
        });
         TextField prix = new TextField("", "Prix", 4, TextArea.NUMERIC);
        by.add(rb1);
            by.add(rb2);
                by.add(prix);
                
                
                                   
              
                
           
                     register.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                               
                System.out.println(e.toString() +"  _____________  "+ e.getAdresse());
            int prixxx=0;
                    
                   if (rb1.isSelected()){
                     type="Gratuit";
                     System.out.println("gratuit");
                     prixxx=0;
                 }
                 else{
                     type="Payant";
                     prixxx=Integer.parseInt(prix.getText());
                     System.out.println(prixxx);
                 }    
                
                
                               
                 java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd");
                 String db= format.format(datedeb.getDate());
                 String df= format.format(datefin.getDate());
                 
                  java.text.SimpleDateFormat format1 = new java.text.SimpleDateFormat("yyyyMMdd");
                 String d1= format1.format(datedeb.getDate());
                 String d2= format1.format(datefin.getDate());
         String d3= format1.format(new Date().getTime());
            System.out.println("date lyoum "+d3);
            
        int a=Integer.parseInt(d1);
        int b=Integer.parseInt(d2);
         int datelyoum=Integer.parseInt(d3);
        int cc=b-a;
        int ddd=a-datelyoum;
           
                
                
                
                
                     if (tfDep.getText().length()==0|| tfDescription.getText().length()==0||tfArr.getText().length()==0||cc<0||ddd<0)
                 {
                     Dialog.show("Humm", "Vérifiez vos informations...", "J'ai compris", null);
                
                     System.out.println(tfDep.getText());
                     System.out.println(tfDescription.getText());
                     System.out.println(tfArr.getText()); 
                     System.out.println(type);
                     System.out.println(prixx);
                     System.out.println(db+"    "+df);
                 }
                
                
                
                     else 
                         
                
                     {
                         ServiceEvenement se=new ServiceEvenement();
                     //  se.AddEv(tfDep.getText(), "ahmed", tfDescription.getText(), tfArr.getText(), type, 1, prixxx,db,df);
                          //    se.UpdateEv(e.getId(),tfDep.getText(), "ahmed", tfDescription.getText(), tfArr.getText(), type, 1,prixxx,db,df);

                       Dialog D = new Dialog();
                 D.show("Success! ", "Modification effectueé","ok",null);
                     new MyListPart(themee).showBack();
                     }
//                        
//        con.setUrl("http://localhost/datatable_21/web/app_dev.php/home/AddEv?nom="+tfDep.getText()+"&prenom="+tfArr.getText()+ "&description=" + tfDescription.getText() + 
//                 "&prix=" +Integer.parseInt(tfNbr.getText())+ "&disponibilite=" + date.getText());

      
                 
                    
                    
                    
       

               
            }
        });
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
         by.add(register);
        by.add(goBackToLogin);
        add(BorderLayout.CENTER, by);

        
        
        
        
        
        
        
        
        
        
        // for low res and landscape devices
        by.setScrollableY(true);
        by.setScrollVisible(true);
    }
    



}
