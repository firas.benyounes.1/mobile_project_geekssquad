/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codename1.uikit.materialscreens;

/**
 *
 * @author shannah
 */
public interface DirectionsRouteListener {
    
    public void routeCalculated(DirectionsResult result);
    
}
