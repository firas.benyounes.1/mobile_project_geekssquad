/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codename1.uikit.materialscreens;

import com.allforkids.Entite.baby;
import com.allforkids.Entite.evenement;
import com.allforkids.Service.ServiceBaby;
import com.allforkids.Service.ServiceEvenement;
import com.codename1.components.Ads;
import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.io.URL;
import com.codename1.io.URL.HttpURLConnection;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import static com.codename1.ui.TextArea.URL;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author ghada
 */
public class ListBabyForm extends SideMenuBaseForm1 {

    private Image img;
    private ImageViewer imgv;
    private EncodedImage enc;
    public static baby specDetails = new baby();
    private Resources theme = UIManager.initFirstTheme("/theme");

    public ListBabyForm(Resources res) {

        super(BoxLayout.y());

        Toolbar tb = getToolbar();
        tb.setTitleCentered(false);
        Image profilePic = res.getImage("user-picture.jpg");

        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());
        Label tit = new Label("", "Title");
    // tit.getAllStyles().setFgColor(0xE12336);
        Container titleCmp = BoxLayout.encloseY(
                FlowLayout.encloseIn(menuButton),
                BorderLayout.centerAbsolute(
                        BoxLayout.encloseY(
//                                new Label("amal", "SubTitle"),
                                tit
                        )
                ),
                GridLayout.encloseIn(2)
        );

        tb.setTitleComponent(titleCmp);

        Label Liste = new Label("");
     
        Label Liste0 = new Label(" ");
        Liste.getAllStyles().setFgColor(0xE12336);

        Container listCon = BoxLayout.encloseY(
                BorderLayout.centerAbsolute(
                        BoxLayout.encloseY(
                                Liste
                        )
                ),
                GridLayout.encloseIn(2)
        );
       // add(listCon);
ComboBox gov = new ComboBox("Ariana","Béja","Ben Arous","Bizerte","Gabès","Gafsa","Jendouba","Kairouan","Kasserine","Kébili","Le Kef","Mahdia","La Manouba","Médenine","Monastir","Nabeul","Sfax","Sidi Bouzid","Siliana","Sousse","Zaghouan","Tataouine","Tozeur","Tunis");
gov.setPreferredW(50);
gov.getAllStyles().setBgColor(0xFFFFFF);

//add(gov);
// gov.getAllStyles().setMargin(LEFT, 0);
        FontImage arrowDown = FontImage.createMaterial(FontImage.MATERIAL_KEYBOARD_ARROW_DOWN, "Label", 3);

        ServiceBaby Sp = new ServiceBaby();
        System.out.println("test");
        getUnselectedStyle().setBackgroundType(Style.BACKGROUND_GRADIENT_RADIAL);
        getUnselectedStyle().setBackgroundGradientEndColor(0xFFFFFF);
        getUnselectedStyle().setBackgroundGradientStartColor(0xFFFFFF);

        // for (EntitySpecialiste spec : Sp.getListPediatre()) {
        //   System.out.println("test2");
        // addButtonBottom(profilePic, "ok", 0xd997f1, true);
        //}
        ArrayList<baby> lis = new ArrayList<baby>();
        lis = Sp.getListBaby();
        System.out.println("test2");

        for (int i = 0; i < lis.size(); i++) {
            addButtonBottom(profilePic, lis.get(i), 0xd997f1, true, i);
        }
        
              
setupSideMenu(res);
    
    
    
    }

    private void addButtonBottom(Image arrowDown, baby spec, int color, boolean first, int i) {

        Container finishLandingPage = new Container(BoxLayout.x());
        Container c = new Container(BoxLayout.y());
        Container containervide = new Container(BoxLayout.x());
        Label spaceLabel0 = new Label(" ");
        Label spaceLabel2 = new Label(" ");
        containervide.add(spaceLabel0);
        containervide.add(spaceLabel2);

        try {
            enc = EncodedImage.create("/giphy.gif");
        } catch (IOException ex) {
            System.out.println("error encoder");
        }

        img = URLImage.createToStorage(enc, "imagee" + i, "http://localhost/datatable_21/web" + spec.getImage(), URLImage.RESIZE_SCALE);

        img.fill(10, 50);
      
imgv=new ImageViewer(img.fill(150, 200));
        finishLandingPage.add(imgv);
        Label l = new Label(spec.getNom());
        Label ad= new Label(spec.getAdrese());
        ad.getAllStyles().setFgColor(0x1c02f6);
        l.getAllStyles().setFgColor(0xF69602);
        
       // SpanLabel l2 = new SpanLabel(spec.getType());

        //l2.getAllStyles().setFgColor(0x000000);
        //Label part = new Label( String.valueOf(spec.getParticiate())+"  Personne(s)" );
        //part.getAllStyles().setFgColor(0xff0000);
        SpanLabel l3 = new SpanLabel(spec.getDescription());
        Button loginButton = new Button(">> Details");

        
        loginButton.getAllStyles().setFgColor(0x008A4F);
        loginButton.addActionListener(e -> {
            /**
             * ************** page detail ************
             */
           specDetails = spec;
            System.out.println(specDetails.toString());
            new DetailsBabyForm(theme).show();

            /**
             * **************************************
             */
        });
        c.add(l);
        c.add(ad);
        //c.add(l2);
        //c.add(part);
        // c.add(l3);
        c.add(loginButton);
        c.setWidth(500);
        c.getUnselectedStyle().setBackgroundType(Style.BACKGROUND_GRADIENT_RADIAL);
        c.getUnselectedStyle().setBackgroundGradientEndColor(0xeae4e4);
        c.getUnselectedStyle().setBackgroundGradientStartColor(0xeae4e4);
        containervide.getUnselectedStyle().setBackgroundType(Style.BACKGROUND_GRADIENT_RADIAL);
        containervide.getUnselectedStyle().setBackgroundGradientEndColor(0xFFFFFF);
        containervide.getUnselectedStyle().setBackgroundGradientStartColor(0xFFFFFF);
        finishLandingPage.getUnselectedStyle().setBackgroundType(Style.BACKGROUND_GRADIENT_RADIAL);
        finishLandingPage.getUnselectedStyle().setBackgroundGradientEndColor(0xFFFFFF);
        finishLandingPage.getUnselectedStyle().setBackgroundGradientStartColor(0xFFFFFF);
        // c.add(containervide);
        finishLandingPage.add(c);
        // finishLandingPage.add(containervide);
        c.setPreferredW(400);
        add(FlowLayout.encloseIn(finishLandingPage));
        add(containervide);
    }

    private Image createCircleLine(int color, int height, boolean first) {
        Image img = Image.createImage(height, height, 0);
        Graphics g = img.getGraphics();
        g.setAntiAliased(true);
        g.setColor(0xcccccc);
        int y = 0;
        if (first) {
            y = height / 6 + 1;
        }
        g.drawLine(height / 2, y, height / 2, height);
        g.drawLine(height / 2 - 1, y, height / 2 - 1, height);
        g.setColor(color);
        g.fillArc(height / 2 - height / 4, height / 6, height / 2, height / 2, 0, 360);
        return img;
    }

    @Override
    protected void showOtherForm(Resources res) {
        new StatsForm(res).show();
    }
    
//    public void contacter(String phone)
//    {
//        try {
//            // Construct data
//            String apiKey = "apikey=" + "yALqkwU+/LM-pSqWsFXlCFaP6qOes06jGKfD5AyDFQ";
//            String message = "&message=" + "Le parent "+ new AuthService().getConnected().getFullname() + " souhaite vous contacter";
//            String sender = "&sender=" + /*txtsender.getText()*/ "KIDS";
//            String numbers = "&numbers=" + "+216"+ phone ;
//
//            // Send data
//            HttpURLConnection conn = (HttpURLConnection) new URL("https://api.txtlocal.com/send/?").openConnection();
//            String data = apiKey + numbers + message + sender;
//            conn.setDoOutput(true);
//            conn.setRequestMethod("POST");
//            conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
//            conn.getOutputStream().write(data.getBytes("UTF-8"));
//            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//            final StringBuffer stringBuffer = new StringBuffer();
//            String line;
//            while ((line = rd.readLine()) != null) {
//                //stringBuffer.append(line);
//                JOptionPane.showMessageDialog(null, "message"+line);
//            }
//            rd.close();
//
//            //return stringBuffer.toString();
//        } catch (Exception e) {
//            //System.out.println("Error SMS "+e);
//            JOptionPane.showMessageDialog(null, e);
//            //return "Error "+e;
//        }
//
//    }
//
    
}