/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codename1.uikit.materialscreens;


import com.allforkids.Entite.baby;
import com.allforkids.Entite.evenement;
import com.allforkids.Service.ServiceBaby;
import com.allforkids.Service.ServiceEvenement;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.ImageViewer;
import com.codename1.components.MultiButton;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author ghada
 */
public class MyListBaby extends SideMenuBaseForm1 {

    private Image img;
    private ImageViewer imgv;
    private EncodedImage enc;
    public static baby specDetails = new baby();
    private Resources theme = UIManager.initFirstTheme("/theme");

    public MyListBaby(Resources res) {

        super(BoxLayout.y());

        Toolbar tb = getToolbar();
       // tb.setTitleCentered(false);
        Image profilePic = res.getImage("user-picture.jpg");

        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());
        Label tit = new Label("", "Title");
    // tit.getAllStyles().setFgColor(0xE12336);
        Container titleCmp = BoxLayout.encloseY(
                FlowLayout.encloseIn(menuButton),
                BorderLayout.centerAbsolute(
                        BoxLayout.encloseY(
//                                new Label("amal", "SubTitle"),
                                tit
                        )
                ),
                GridLayout.encloseIn(2)
        );

        tb.setTitleComponent(titleCmp);

        Label Liste = new Label("");
     
        Label Liste0 = new Label(" ");
        Liste.getAllStyles().setFgColor(0xE12336);

        Container listCon = BoxLayout.encloseY(
                BorderLayout.centerAbsolute(
                        BoxLayout.encloseY(
                                Liste
                        )
                ),
                GridLayout.encloseIn(2)
        );
       // add(listCon);
ComboBox gov = new ComboBox("Ariana","Béja","Ben Arous","Bizerte","Gabès","Gafsa","Jendouba","Kairouan","Kasserine","Kébili","Le Kef","Mahdia","La Manouba","Médenine","Monastir","Nabeul","Sfax","Sidi Bouzid","Siliana","Sousse","Zaghouan","Tataouine","Tozeur","Tunis");
gov.setPreferredW(50);
gov.getAllStyles().setBgColor(0xFFFFFF);

//add(gov);
// gov.getAllStyles().setMargin(LEFT, 0);
        FontImage arrowDown = FontImage.createMaterial(FontImage.MATERIAL_KEYBOARD_ARROW_DOWN, "Label", 3);

        ServiceBaby Sp = new ServiceBaby ();
        System.out.println("test");
        getUnselectedStyle().setBackgroundType(Style.BACKGROUND_GRADIENT_RADIAL);
        getUnselectedStyle().setBackgroundGradientEndColor(0xFFFFFF);
        getUnselectedStyle().setBackgroundGradientStartColor(0xFFFFFF);

        // for (EntitySpecialiste spec : Sp.getListPediatre()) {
        //   System.out.println("test2");
        // addButtonBottom(profilePic, "ok", 0xd997f1, true);
        //}
        ArrayList<baby> lis = new ArrayList<baby>();
        lis = Sp.getMyListBabyt();
        System.out.println("test22222222222");
        System.out.println(lis.toString());
        for (int i = 0; i < lis.size(); i++) {
            addButtonBottom(profilePic, lis.get(i), 0xd997f1, true, i);
        }
        setupSideMenu(res);
    }

    private void addButtonBottom(Image arrowDown, baby spec, int color, boolean first, int i) {

        Container finishLandingPage = new Container(BoxLayout.x());
        Container c = new Container(BoxLayout.y());
        Container containervide = new Container(BoxLayout.x());
        Label spaceLabel0 = new Label(" ");
        Label spaceLabel2 = new Label(" ");
        containervide.add(spaceLabel0);
        containervide.add(spaceLabel2);

        try {
            enc = EncodedImage.create("/giphy.gif");
        } catch (IOException ex) {
            System.out.println("error encoder");
        }

        img = URLImage.createToStorage(enc, "imagee" + i, "http://localhost/datatable_21/web/" + spec.getImage(), URLImage.RESIZE_SCALE);

        img.fill(10, 50);
      
imgv=new ImageViewer(img.fill(150, 200));
        finishLandingPage.add(imgv);
        Label l = new Label(spec.getNom());
        Label ad= new Label(spec.getPrenom());
        ad.getAllStyles().setFgColor(0x1c02f6);
        l.getAllStyles().setFgColor(0xF69602);
        
        
        
        Label l3 = new Label(spec.getDescription());
        Button loginButton = new Button(">> Details");

        loginButton.getAllStyles().setFgColor(0x008A4F);
        loginButton.addActionListener(e -> {
            /**
             * ************** page detail ************
             */
           specDetails = spec;
            System.out.println(specDetails.toString());
            new DetailMyBaby(theme).show();

            /**
             * **************************************
             */
        });
        c.add(l);
        c.add(ad);
      
        // c.add(l3);
        c.add(loginButton);
        c.setWidth(500);
        c.getUnselectedStyle().setBackgroundType(Style.BACKGROUND_GRADIENT_RADIAL);
        c.getUnselectedStyle().setBackgroundGradientEndColor(0xeae4e4);
        c.getUnselectedStyle().setBackgroundGradientStartColor(0xeae4e4);

        containervide.getUnselectedStyle().setBackgroundType(Style.BACKGROUND_GRADIENT_RADIAL);
        containervide.getUnselectedStyle().setBackgroundGradientEndColor(0xFFFFFF);
        containervide.getUnselectedStyle().setBackgroundGradientStartColor(0xFFFFFF);
        finishLandingPage.getUnselectedStyle().setBackgroundType(Style.BACKGROUND_GRADIENT_RADIAL);
        finishLandingPage.getUnselectedStyle().setBackgroundGradientEndColor(0xFFFFFF);
        finishLandingPage.getUnselectedStyle().setBackgroundGradientStartColor(0xFFFFFF);
        // c.add(containervide);
        finishLandingPage.add(c);
        // finishLandingPage.add(containervide);
        c.setPreferredW(400);
        add(FlowLayout.encloseIn(finishLandingPage));
        add(containervide);
    }

    private Image createCircleLine(int color, int height, boolean first) {
        Image img = Image.createImage(height, height, 0);
        Graphics g = img.getGraphics();
        g.setAntiAliased(true);
        g.setColor(0xcccccc);
        int y = 0;
        if (first) {
            y = height / 6 + 1;
        }
        g.drawLine(height / 2, y, height / 2, height);
        g.drawLine(height / 2 - 1, y, height / 2 - 1, height);
        g.setColor(color);
        g.fillArc(height / 2 - height / 4, height / 6, height / 2, height / 2, 0, 360);
        return img;
    }

    @Override
    protected void showOtherForm(Resources res) {
        new StatsForm(res).show();
}


    
}
