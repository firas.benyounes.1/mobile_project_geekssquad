/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.kids.Gui;

import com.codename1.ui.Button;
import com.codename1.ui.Form;
import com.codename1.ui.TextField;
import com.codename1.ui.layouts.BoxLayout;

/**
 *
 * @author ikbel
 */
public class InterfaceConnection 
{
    private Form f;
    
    private TextField login;
    
    private TextField pwd;
    
    

    
    
    public InterfaceConnection()
    {
        f=new Form(BoxLayout.y());
        login=new TextField();
        pwd=new TextField();
        
        login.setHint("Username");
        pwd.setHint("Mot de passe");
        pwd.setConstraint(TextField.PASSWORD);
        
      
    }
    
    public void AddelementtoForm()
    {
        f.add(login);
        f.add(pwd);
    }

    public Form getF() {
        
        AddelementtoForm();
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

    public TextField getLogin() {
        return login;
    }

    public void setLogin(TextField login) {
        this.login = login;
    }

    public TextField getPwd() {
        return pwd;
    }

    public void setPwd(TextField pwd) {
        this.pwd = pwd;
    }
    
    
    
}
