/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codename1.uikit.materialscreens;

import com.codename1.components.ShareButton;
import com.codename1.io.Log;
import com.codename1.location.Geofence;
import com.codename1.location.Location;
import com.codename1.location.LocationManager;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;


import javafx.geometry.Pos;
/**
 *
 * @author ikbel
 */
public class InterfaceAcceuil 
{
    private Form f;
    private Label lbinfos;
     private Resources theme;
  

    
    public InterfaceAcceuil()
    {
          theme = UIManager.initFirstTheme("/theme");
            Toolbar.setGlobalToolbar(true);
        Log.bindCrashProtection(true);
        Location position = LocationManager.getLocationManager().getCurrentLocationSync();
        Geofence gf = new Geofence("test", position, 100, 100000);
LocationManager.getLocationManager().addGeoFencing(Ik_InterfaceNotif.class, gf);
        f=new Form();
        
        f.getToolbar().addCommandToSideMenu("Forum", null, e->{
           PublicationInterface pi=new PublicationInterface();
           pi.getF1().show();
        });
        ShareButton sb=new ShareButton();
sb.setText("Invitez vos amis"); 
sb.setTextToShare("staller l’application nommée XXX. C’est un truc de ouf pour…");
       

f.add(sb);
        f.getToolbar().addCommandToSideMenu("e-commerce", null, null);
        f.getToolbar().addCommandToSideMenu("Baby-sitting", null, null);
        f.getToolbar().addCommandToSideMenu("Donations et evenements", null, null);
        f.getToolbar().addCommandToSideMenu("Jardins d'enfants", null, null);
        f.getToolbar().addCommandToOverflowMenu("Se Deconnecter", null, e->{
        
           
        
          
        });
        f.getToolbar().addCommandToOverflowMenu("Home", null, e->{
        
         
        new First(theme).showBack();
        });
        
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
    
    
    
}
