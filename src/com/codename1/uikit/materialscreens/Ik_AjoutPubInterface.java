
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codename1.uikit.materialscreens;



import com.allforkids.Entite.Categorie;
import com.allforkids.Entite.Publication;
import com.allforkids.Service.PublicationService;
import com.codename1.ui.TextArea;

import com.codename1.io.Log;
import com.codename1.messaging.Message;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.list.DefaultListCellRenderer;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;



import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 *
 * @author ikbel
 */
public class Ik_AjoutPubInterface 
{
 
    private Form f1;
   
    private Resources theme;
    private int categoryId;
    private String categoryName;

   
    Form f;
    TextField titre;
    TextArea cont;
    ComboBox categ;

    Button btnajout, btnaff;
    
    public Ik_AjoutPubInterface ()
    {
       theme = UIManager.initFirstTheme("/theme");
        Toolbar.setGlobalToolbar(true);
        Log.bindCrashProtection(true);

        f1 = new Form("ajouter publication");


        f1.getToolbar().addCommandToRightBar("Retour", null, e -> {

            System.out.println("okkkkkkkk");

            PublicationInterface im = new PublicationInterface();
            im.getF1().show();
        });

        titre = new TextField();
        cont = new TextField();
        cont.setHint("Rédiger votre publication...");
        PublicationService se= new PublicationService();
      ComboBox getCategoryComboBox = new ComboBox();
getCategoryComboBox.setUIID("TextField");
getCategoryComboBox.addItem("Choose Category");
for (  Categorie entry : se.getCategories()) {
     categoryName = (String) entry.getNom();
     categoryId = (int) entry.getId();
    getCategoryComboBox.addItem(entry);
}
    getCategoryComboBox.setRenderer(new DefaultListCellRenderer<Object>() {   
   public Component getCellRendererComponent(Component list, Object model, Object value, int index, boolean isSelected) {
      if(value instanceof Categorie) {
         value = ((Categorie)value).getNom();
      }
      return super.getCellRendererComponent(list, model, value, index, isSelected);
   }
});
    
        btnajout = new Button("ajouter");
        btnaff = new Button("Affichage");
        f1.add(titre);
        f1.add(cont);
        f1.add(getCategoryComboBox);
        f1.add(btnajout);
        
       
            btnajout.addActionListener((e) -> {
            int intec = ((Categorie)getCategoryComboBox.getSelectedItem()).getId();
            PublicationService ser = new PublicationService();
        
            Publication p = new Publication(intec,LoginForm.UserConnected.getId(),titre.getText(), 0, (-1334798781), "en attente", cont.getText(), 0);
            ser.ajoutPublication(p);
 Message ma = new Message("Votre demande de publier :\n"+titre.getText()+"\n"+"est en attente, merci.");

Display.getInstance().sendMessage(new String[] {"ikbel.talebhssan@esprit.tn"}, "Subject of message", ma);
        });
    }
    
     
    
    

    public Form getF1() {
        return f1;
    }

    public void setF1(Form f1) {
        this.f1 = f;
    }

    public TextField getTitre() {
        return titre;
    }

    public void setTitre(TextField titre) {
        this.titre = titre;
    }

    public TextArea getCont() {
        return cont;
    }

    public void setCont(TextArea cont) {
        this.cont = cont;
    }

    public Categorie getAdre() {
        return (Categorie)categ.getSelectedItem();
    }

    public void setAdre(ComboBox categ) {
        this.categ = categ;
    }

    public Button getBtnajout() {
        return btnajout;
    }

    public void setBtnajout(Button btnajout) {
        this.btnajout = btnajout;
    }


}