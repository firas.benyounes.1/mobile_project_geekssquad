/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codename1.uikit.materialscreens;

import com.allforkids.Service.ServiceDemande;
import com.codename1.capture.Capture;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.ImageViewer;
import com.codename1.components.InfiniteProgress;
import com.codename1.components.SignatureComponent;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.FileSystemStorage;
import com.codename1.io.MultipartRequest;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import static com.codename1.ui.Component.LEFT;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Slider;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import com.codename1.ui.validation.LengthConstraint;
import com.codename1.ui.validation.NumericConstraint;
import com.codename1.ui.validation.Validator;
import java.io.IOException;
import java.util.Date;


/**
 *  
 * @author Kais
 */
public class AddDemande extends Form{
    
    Image img = null;
    Image toupload = null;
    Image profilePic = null;
    Label profile = null;
    ImageViewer image = new ImageViewer();
    Slider starRank = new Slider();
    static int SR;
    ServiceDemande aaaa= new ServiceDemande();
    
   // public final String link="http://localhost/Pi/web/app_dev.php";
    
    public AddDemande() {
        this(com.codename1.ui.util.Resources.getGlobalResources());
    }

    
    
    
//        private void initStarRankStyle(Style s, Image star) {
//    s.setBackgroundType(Style.BACKGROUND_IMAGE_TILE_BOTH);
//    s.setBorder(Border.createEmpty());
//    s.setBgImage(star);
//    s.setBgTransparency(0);
//}
//
//private Slider createStarRankSlider() {
//    Slider starRank = new Slider();
//    starRank.setEditable(true);
//    starRank.setMinValue(0);
//    starRank.setMaxValue(5);
//    Font fnt = Font.createTrueTypeFont("native:MainThin", "native:MainThin").
//            derive(Display.getInstance().convertToPixels(5, true), Font.STYLE_PLAIN);
//    Style s = new Style(0xffff33, 0, fnt, (byte)0);
//    Image fullStar = FontImage.createMaterial(FontImage.MATERIAL_STAR, s).toImage();
//    s.setOpacity(100);
//    s.setFgColor(0);
//    Image emptyStar = FontImage.createMaterial(FontImage.MATERIAL_STAR, s).toImage();
//    initStarRankStyle(starRank.getSliderEmptySelectedStyle(), emptyStar);
//    initStarRankStyle(starRank.getSliderEmptyUnselectedStyle(), emptyStar);
//    initStarRankStyle(starRank.getSliderFullSelectedStyle(), fullStar);
//    initStarRankStyle(starRank.getSliderFullUnselectedStyle(), fullStar);
//    starRank.setPreferredSize(new Dimension(fullStar.getWidth() * 5, fullStar.getHeight()));
//    starRank.addActionListener(new ActionListener() {
//        @Override
//        public void actionPerformed(ActionEvent evt) {
//           System.out.println(starRank.getProgress());
// 
//           SR=starRank.getProgress();
//        }
//    });
//    return starRank;
//}
//    

      String Filenom;
    
    public AddDemande(Resources theme) {

        super(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER_ABSOLUTE));
        setUIID("LoginForm");
        
        try {
           // LoginForm.setTheme(theme.openLayered("/theme"));
        } catch (Exception e) {
            System.out.println(e);
        }
        Container welcome = FlowLayout.encloseCenter(
                new Label("Ajouter Offre", "WelcomeRed")
        );
        Button register = new Button("add offre");
        FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_IMAGE);

        Image profilePic = theme.getImage("user-picture.jpg");
        Image mask = theme.getImage("round-mask.png");
        profilePic = profilePic.fill(mask.getWidth(), mask.getHeight());

        profile = new Label();
         fab.addActionListener( (ActionEvent ev) -> {
          
                if (ev != null && ev.getSource() != null) {
                   
                       
                        
                    try {
                        String fileNameInServer = "";
                        MultipartRequest cr = new MultipartRequest();
                        String filePath = Capture.capturePhoto(-1, -1);
                        System.out.println(filePath);
                        String filenom="/images/file.jpg";
                        System.out.println(filenom);
                        cr.setUrl("http://localhost/datatable_21/web/uploadimage.php");
                        cr.setPost(true);
                        String mime = "image/jpeg";
                        cr.addData("file", filePath, mime);
                         String out = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
                         cr.setFilename("file", out + ".jpg");
                        
                        fileNameInServer += out + ".jpg";
                        Filenom=fileNameInServer;
                        System.err.println("path2 =" + fileNameInServer.toString());
                        InfiniteProgress prog = new InfiniteProgress();
                        Dialog dlg = prog.showInifiniteBlocking();
                        cr.setDisposeOnCompletion(dlg);
                        NetworkManager.getInstance().addToQueueAndWait(cr);
                        //  try {
                        
                        img = Image.createImage(FileSystemStorage.getInstance().openInputStream(filePath));
                        toupload = img;
                        img.fill(mask.getWidth(), mask.getHeight());
                        profile.setIcon(img);
                        profile.setUIID("ProfilePicTitle");
                        profile.setMask(mask.createMask());
                        refreshTheme();
                        //} catch (IOException e) {
                        //  }
                    } catch (IOException ex) {
                    }
                        
                    
                    
                }
            }
        );

        getTitleArea().setUIID("Container");
 Picker date = new Picker();
date.setType(Display.PICKER_TYPE_DATE);        
SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
                String db= dateFormatter.format(date.getDate());
        //date.setFormatter(dateFormatter);

        Image logo = theme.getImage("logo.png");

        //    Image mask = theme.getImage("round-mask.png");
        //    logo = logo.fill(mask.getWidth(), mask.getHeight());
        Label profilePicLabel = new Label(logo, "ProfilePic");

        //   profilePicLabel.setMask(mask.createMask());
        TextField tfDep = new TextField("", "Nom", 20, 0);
        TextField tfArr = new TextField("", "Prenom", 20, 0);
        TextField tfDescription = new TextField("", "Description", 20, 0);
        TextField tfNbr = new TextField("", "Prix", 20, 0);
//        TextField rating = new TextField("", "Rating", 20, 0);

       
       
        
        Validator v = new Validator();
        v.addConstraint(tfDep, new LengthConstraint(2)).
                addConstraint(tfArr, new LengthConstraint(2)).
                addConstraint(tfDescription, new LengthConstraint(2)).
                addConstraint(tfNbr, new NumericConstraint(true));
//                addConstraint(etat, new LengthConstraint(2));                
      //  v.addSubmitButtons(register);
        tfDep.getAllStyles().setMargin(LEFT, 0);
        tfArr.getAllStyles().setMargin(LEFT, 0);
        tfDescription.getAllStyles().setMargin(LEFT, 0);
        tfNbr.getAllStyles().setMargin(LEFT, 0);
//        etat.getAllStyles().setMargin(LEFT, 0);
//        rating.getAllStyles().setMargin(LEFT, 0);

        Label nomicon = new Label("", "TextField");
        Label Descriptionicon = new Label("", "TextField");
        Label Imageicon = new Label("", "TextField");
        Label Prixicon = new Label("", "TextField");
        Label etaticon = new Label("", "TextField");
        Label ratingicon = new Label("", "TextField");

        nomicon.getAllStyles().setMargin(RIGHT, 0);
        Descriptionicon.getAllStyles().setMargin(RIGHT, 0);
        Imageicon.getAllStyles().setMargin(RIGHT, 0);
        Prixicon.getAllStyles().setMargin(RIGHT, 0);
        etaticon.getAllStyles().setMargin(RIGHT, 0);
        ratingicon.getAllStyles().setMargin(RIGHT, 0);

        FontImage.setMaterialIcon(nomicon, FontImage.MATERIAL_PERSON_OUTLINE, 3);
        FontImage.setMaterialIcon(Descriptionicon, FontImage.MATERIAL_PERSON_OUTLINE, 3);
        FontImage.setMaterialIcon(Imageicon, FontImage.MATERIAL_FACE, 3);
        FontImage.setMaterialIcon(Prixicon, FontImage.MATERIAL_PERSON_OUTLINE, 3);
        FontImage.setMaterialIcon(etaticon, FontImage.MATERIAL_PERSON_OUTLINE, 3);
        FontImage.setMaterialIcon(ratingicon, FontImage.MATERIAL_FACE, 3);

        
        
        
        
        
        
        
        
/*Button*/
        StringBuffer str = new StringBuffer();
        ConnectionRequest con = new ConnectionRequest();
        register.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                               
                if (v.isValid()) {
         System.out.println(Integer.parseInt(tfNbr.getText()));
aaaa.ajoutdemande(tfDep.getText(),  tfArr.getText(),  "aabbbaaa", db, 1, "images/"+Filenom, tfNbr.getText(), tfDescription.getText(), db);
         
// con.setPost(true);
                    
//                    con.setHttpMethod("POST");
// con.setUrl("http://localhost/DEV/pi/insert.php?pointDepart=" + tfDep.getText() + "&pointArrivee=" + tfArr.getText() + "&description=" + tfDescription.getText() + 
//                    "$date= " + dateTimePicker.getDate() + "$nbPlaceMax= " +Integer.parseInt(tfNbr.getText())+ "");
 
// con.setUrl("http://localhost/datatable_21/web/app_dev.php/home/ajoutDemande" + tfDep.getText() + "&prenom=" + tfArr.getText() + "&description=" + tfDescription.getText() + 
  //                 "&prix=" +Integer.parseInt(tfNbr.getText())+ "&disponibilite=" + date.getText());
 
// con.setUrl("http://localhost/DEV/pi/update.php?pointDepart=" + tfDep.getText() + "&pointArrivee=" + tfArr.getText() + "&description=" + tfDescription.getText() + 
//                    "$date= " + dateTimePicker.getDate() + "$id=3");
////                                  
       // con.setUrl("http://localhost/datatable_21/web/app_dev.php/home/ajoutDemande?nom="+tfDep +"&prenom="+tfArr+"&adrese="+"aabbbaaa"+"&disponibilite="+date+"&idb="+1+"+&email="+"ccccc"+"&prix="+tfNbr+"&description="+tfDescription+"&datefin="+date+"&numtel="+508768936+"&latitude="+1.036+"&longitude="+36.214);
                    System.out.println("aaaaaaaa");
     
               //   con.addRequestHeader("Content-Type", "application/x-www-form-urlencoded");
//                    con.addResponseListener(new ActionListener<NetworkEvent>() {
//                        @Override
//                        public void actionPerformed(NetworkEvent evt) {

       
                               Dialog.show("Felicitation", "Ajout effectué avec succes", "Voir vos Affiches", null);

                                

                          
//                                
//                        }
//                    });
                 //   NetworkManager.getInstance().addToQueue(con);

                } else {
                    Dialog.show("Humm", "Vérifiez vos informations...", "J'ai compris", null);
                             //  System.out.println("Kais Rating "+starRank.getProgress());

                }
            }
        });

        Button goBackToLogin = new Button(" Revenir à la page précédente");
        goBackToLogin.setUIID("StasForm");
        goBackToLogin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                Toolbar.setGlobalToolbar(false);
               new MyListDemande(theme).show();

                Toolbar.setGlobalToolbar(true);
            }
        });
        // We remove the extra space for low resolution devices so things fit better
        Label spaceLabel;
        if (!Display.getInstance().isTablet() && Display.getInstance().getDeviceDensity() < Display.DENSITY_VERY_HIGH) {
            spaceLabel = new Label();
        } else {
            spaceLabel = new Label(" ");
        }

        Container by = BoxLayout.encloseY(
                welcome,
                spaceLabel,
                profile,
                fab,
                BorderLayout.center(tfDep).
                add(BorderLayout.WEST, nomicon),
                BorderLayout.center(tfArr).
                add(BorderLayout.WEST, Descriptionicon),
                BorderLayout.center(tfDescription).
                add(BorderLayout.WEST, Imageicon),
                BorderLayout.center(date).
                add(BorderLayout.WEST, Prixicon),
                BorderLayout.center(tfNbr).
                add(BorderLayout.WEST, etaticon),
//                BorderLayout.center(rating).
//                add(BorderLayout.WEST,(createStarRankSlider())),               
                register,
                goBackToLogin
                
        );
        add(BorderLayout.CENTER, by);

        // for low res and landscape devices
        by.setScrollableY(true);
        by.setScrollVisible(false);
    }
    



}
