/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codename1.uikit.materialscreens;

import com.allforkids.Entite.Produit;
import com.allforkids.Service.ProduitService;

import com.codename1.components.ImageViewer;
import com.codename1.components.MultiButton;
import com.codename1.components.SpanLabel;
import com.codename1.components.ToastBar;
import com.codename1.db.Cursor;
import com.codename1.db.Database;
import com.codename1.db.Row;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.ComponentGroup;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Slider;
import com.codename1.ui.SwipeableContainer;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author said
 */
public class afficheProduit extends s_SideMenuBaseForm {

    private Image img;
    private boolean Verif;
    Boolean created = false;
    private Database db;
    private ImageViewer imgv;
    private EncodedImage enc;
    private Resources theme = UIManager.initFirstTheme("/theme");
    private Image coeur;
    private Image coeur2;
            ArrayList<Produit> lis = new ArrayList<Produit>();

    int idUser= LoginForm.UserConnected.getId();
  
    ArrayList<Produit> lis2;
    
    
    public ArrayList<Produit> selectFavoris() {
           ArrayList<Produit> ALLproducts = new ArrayList<>();
        try {
            Cursor c = db.executeQuery("select * from favoris where iduser="+idUser+"");
            while (c.next()) {
                Produit pr = new Produit();
                Row r = c.getRow();
                pr.setId(r.getInteger(0));
                pr.setNom(r.getString(1));
                pr.setQuantite(r.getInteger(2));
                pr.setStock(r.getInteger(3));
                pr.setPrix(r.getInteger(4));
                pr.setImage(r.getString(5));
                pr.setIduser(idUser);
                ALLproducts.add(pr);
            }

        } catch (IOException ex) {
            //Logger.getLogger(s_Panier.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("erreur");
        }
        System.out.println("allP" + ALLproducts);
        return ALLproducts;
    }

    public void acheter(Produit p) {
        try {
            setcart(p);
            //User idcnx = Main.getUserCnx();
            db.execute("INSERT INTO `panier` (`id`, `nom`, `quantite`, `stock`, `prix`, `image`, `iduser`) VALUES (" + p.getId() + ",'" + p.getNom() + "'," + p.getQuantite() + "," + p.getStock() + "," + p.getPrix() + ",'" + p.getImage() + "'," + idUser + ");");
            //Dialog.show("Produit Ajouté avec succes", "ajout", "ok", null);
             ToastBar.showErrorMessage("Produit Ajouté avec succes",10);

            System.out.println("ajout ok");

        } catch (IOException ex) {
            System.out.println("erreur d'insertion");
        }

    }

    public void update(Produit p, int id) {
        try {
            setcart(p);
            db.execute("UPDATE `panier` SET `quantite`=" + p.getQuantite() + " WHERE `id`=" + id + ";");
            System.out.println("update effectuer");

        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    public void favoris(Produit p) {
        try {
            //             User idcnx = Main.getUserCnx();
            db.execute("INSERT INTO `favoris` (`id`, `nom`, `quantite`, `stock`, `prix`,`image`,`iduser`) VALUES (" + idUser*1000+p.getId() + ",'" + p.getNom() + "'," + p.getQuantite() + "," + p.getStock() + "," + p.getPrix() + ",'" + p.getImage() + "'," + idUser + ");");
            //db.execute("INSERT INTO `favoris` (`id`,`iduser`, `nom`, `quantite`, `stock`, `prix`,`image`) VALUES ("+p.getId()+",null,'"+p.getNom()+"',"+p.getQuantite()+","+p.getStock()+","+p.getPrix()+",'"+p.getImage()+"');");
            //Dialog.show("favori Ajouté avec succes", "ajout", "ok", null);
            ToastBar.showErrorMessage("favori Ajouté avec succes",10);

            System.out.println("favoris ok");
        } catch (IOException ex) {
            System.out.println("erreur d'insertion");
            System.out.println(ex);
        }
    }

    public boolean verifFavoris(Produit p) {
        ArrayList<Produit> ALLproducts = new ArrayList<>();
        Produit P2 = new Produit();
        boolean test = false;
        Cursor c;
        try {
            c = db.executeQuery("select * from favoris where id=" + idUser*1000+p.getId() + " AND iduser="+ idUser +";");
            while (c.next()) {
                Row r = c.getRow();
                P2.setId(r.getInteger(0));
                ALLproducts.add(P2);
            }
        } catch (IOException ex) {
        }

        return ALLproducts.size() == 1;
    }

    public ArrayList<Produit> selectProduit() {
        ArrayList<Produit> ALLproducts = new ArrayList<>();
        try {
            Cursor c = db.executeQuery("select * from panier where iduser="+idUser+"");
            while (c.next()) {
                Produit pr = new Produit();
                Row r = c.getRow();
                pr.setId(r.getInteger(0));
                pr.setNom(r.getString(1));
                pr.setQuantite(r.getInteger(2));
                pr.setStock(r.getInteger(3));
                pr.setPrix(r.getInteger(4));
                pr.setImage(r.getString(5));
                ALLproducts.add(pr);
            }

        } catch (IOException ex) {
            //Logger.getLogger(s_Panier.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("erreur");
        }
        System.out.println("allP" + ALLproducts);
        return ALLproducts;
    }

    public afficheProduit(Resources res) {

        super(BoxLayout.y());
        System.out.println("id user **"+idUser);
        created = Database.exists("firaspidev28");
        try {
            db = Database.openOrCreate("firaspidev28");
            System.out.println("panier");

            if (created == false) {

                db.execute("CREATE TABLE panier (id INTEGER PRIMARY KEY  ,nom TEXT , quantite INTEGER , stock INTEGER ,  prix INTEGER , image TEXT, iduser INTEGER );");
                db.execute("CREATE TABLE favoris (id INTEGER PRIMARY KEY ,nom TEXT ,quantite INTEGER , stock INTEGER  ,  prix INTEGER , image TEXT , iduser INTEGER);");

            }
        } catch (IOException ex) {

        }
        //houni
        refreshpanier();
        Toolbar tb = getToolbar();
        tb.setTitleCentered(false);
        Image profilePic = res.getImage("user-picture.jpg");

        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());
        Label tit = new Label("Liste des produits", "Title");
        // tit.getAllStyles().setFgColor(0xE12336);
        Container titleCmp = BoxLayout.encloseY(
                FlowLayout.encloseIn(menuButton),
                BorderLayout.centerAbsolute(
                        BoxLayout.encloseY(
                                //                                new Label("amal", "SubTitle"),
                                tit
                        )
                ),
                GridLayout.encloseIn(2)
        );

        tb.setTitleComponent(titleCmp);

        Label Liste = new Label("");

        Label Liste0 = new Label(" ");
        Liste.getAllStyles().setFgColor(0xE12336);

        Container listCon = BoxLayout.encloseY(
                BorderLayout.centerAbsolute(
                        BoxLayout.encloseY(
                                Liste
                        )
                ),
                GridLayout.encloseIn(2)
        );
        FontImage arrowDown = FontImage.createMaterial(FontImage.MATERIAL_KEYBOARD_ARROW_DOWN, "Label", 3);

        ProduitService pr_service = new ProduitService();
        getUnselectedStyle().setBackgroundType(Style.BACKGROUND_GRADIENT_RADIAL);
        getUnselectedStyle().setBackgroundGradientEndColor(0xFFFFFF);
        getUnselectedStyle().setBackgroundGradientStartColor(0xFFFFFF);

        lis = pr_service.getListProduit();

        TextField nomtxt = new TextField();
        nomtxt.setHint("chercher par nom ");
        nomtxt.getAllStyles().setBgColor(0xFFFFFF);
        nomtxt.getAllStyles().setBorder(Border.createLineBorder(1, 0xB5D56A));
        nomtxt.getAllStyles().setFgColor(0x000000);

        nomtxt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                removeAll();
                refreshTheme();
                add(nomtxt);
               // add(vide);
                lis2 = new ArrayList<Produit>();
                if (nomtxt.getText().trim().length() == 0) {
                    for (int i = 0; i < lis.size(); i++) {
                        addButtonBottom(profilePic, lis.get(i), 0xd997f1, true, i);

                    }

                } else {
                    for (int i = 0; i < lis.size(); i++) {
                        if (lis.get(i).getNom().toLowerCase().contains(nomtxt.getText().toLowerCase()) ) {
                            lis2.add(lis.get(i));
                        }
                    }

                    for (int i = 0; i < lis2.size(); i++) {
                        addButtonBottom(profilePic, lis2.get(i), 0xd997f1, true, i);

                    }
                }
            }
        });

        add(nomtxt);
        for (int i = 0; i < lis.size(); i++) {
            addButtonBottom(profilePic, lis.get(i), 0xd997f1, true, i);
        }

        
        setupSideMenu(res);
    }

    private void addButtonBottom(Image arrowDown, Produit p, int color, boolean first, int i) {

        Container finishLandingPage = new Container(BoxLayout.x());
        Container c = new Container(BoxLayout.x());
        Container c1 = new Container(BoxLayout.y());
        Container cimage = new Container(BoxLayout.x());
        Container containervide = new Container(BoxLayout.x());
//        Label spaceLabel0 = new Label(" ");
//        Label spaceLabel2 = new Label(" ")  ;
//        containervide.add(spaceLabel0);
//        containervide.add(spaceLabel2);

        try {
            enc = EncodedImage.create("/giphy.gif");
        } catch (IOException ex) {
            System.out.println("error encoder");
        }

        img = URLImage.createToStorage(enc, "vhggh" + i, "http://localhost/pidev/web" + p.getImage(), URLImage.RESIZE_SCALE);
        img.fill(500, 100);
        cimage.add(img.scaledWidth(110));
        finishLandingPage.add(cimage);
        Label l = new Label(p.getNom());
        l.getAllStyles().setFgColor(0x000000);
        Label l2 = new Label(String.valueOf(p.getPrix()) + " TND");

        l.getAllStyles().setFgColor(0xF69602);
        SpanLabel l3 = new SpanLabel(p.getDescription());
//////////////****************QUICK VIEW ********************//////////////////////////////////////
        Button loginButton = new Button("Details");
        loginButton.getAllStyles().setFgColor(0x008A4F);
        loginButton.addActionListener(e -> {
            Produit.setId_courant(p.getId());
            new s_QuickView(theme).show();

        });

        Button acheterButton = new Button("acheter");
        acheterButton.addActionListener(e -> {
            if (Produit.getPanier().contains(p)) {
                for (int m = 0; m < Produit.getPanier().size(); m++) {
                    if (Produit.getPanier().get(m).getId() == p.getId()) {
                        if (Produit.getPanier().get(m).getQuantite() < p.getStock()) {
                            // Verif=Produit.setPanier(p);
                            update(Produit.getPanier().get(m), Produit.getPanier().get(m).getId());

                            //getShowPane();
                        } else {
                            System.out.println("depassez stock");
                        }
                    }
                }
            } else {
                if (p.getStock() > 0) {  //Verif=Produit.setPanier(p);
                    acheter(p);
                    //  getShowPane();             
                } else {
                    System.out.println("depasser");
                }
            }
        });
        Button fav = new Button("fav");

        coeur = URLImage.createToStorage(enc, "fav1.png" + i, "http://localhost/pidev/web/images/fav1.png", URLImage.RESIZE_SCALE_TO_FILL);
        coeur.fill(50, 50);
        coeur2 = URLImage.createToStorage(enc, "fav2.png", "http://localhost/pidev/web/images/fav2.png", URLImage.RESIZE_SCALE_TO_FILL);
        coeur2.fill(50, 50);
        Container image1 = new Container(BoxLayout.x());
        Container image5 = new Container(BoxLayout.x());
        image1.add(coeur.scaledWidth(20));
        image5.add(coeur2.scaledWidth(20));
        if (verifFavoris(p)) {
            c.addAll(image5, acheterButton);
        } else {
            c.addAll(image1, acheterButton);
        }
        fav.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (verifFavoris(p)) {
                    try {
                        db.execute("DELETE FROM favoris WHERE id="+idUser*1000+p.getId()+"");
                        c.replace(image5, image1, null);
                        refreshTheme();
                    } catch (IOException ex) {
                    }
                } else {
                    favoris(p);
                    c.replace(image1, image5, null);
                    refreshTheme();
                }
            }
        });

        image1.setLeadComponent(fav);
        image5.setLeadComponent(fav);

        

        c1.add(l);
        c1.add(l2);

        // c.add(l3);
//        c.add(loginButton);
//        c.add(acheterButton);
        //c.setWidth(300);
//        c.getUnselectedStyle().setBackgroundType(Style.BACKGROUND_GRADIENT_RADIAL);
//        c.getUnselectedStyle().setBackgroundGradientEndColor(0xeae4e4);
//        c.getUnselectedStyle().setBackgroundGradientStartColor(0xeae4e4);
        containervide.getUnselectedStyle().setBackgroundType(Style.BACKGROUND_GRADIENT_RADIAL);
        containervide.getUnselectedStyle().setBackgroundGradientEndColor(0xFFFFFF);
        containervide.getUnselectedStyle().setBackgroundGradientStartColor(0xFFFFFF);
        finishLandingPage.getUnselectedStyle().setBackgroundType(Style.BACKGROUND_GRADIENT_RADIAL);
        finishLandingPage.getUnselectedStyle().setBackgroundGradientEndColor(0xFFFFFF);
        finishLandingPage.getUnselectedStyle().setBackgroundGradientStartColor(0xFFFFFF);
        // c.add(containervide);
        c1.add(c);
        finishLandingPage.add(c1);
        //add(ComponentGroup.enclose(c));
             
        cimage.setLeadComponent(loginButton);
        // finishLandingPage.add(containervide);
        c.setPreferredW(400);
        add(ComponentGroup.enclose(finishLandingPage));
        //add(containervide);
        //getUnselectedStyle().setBackgroundGradientStartColor(0xeae4e4);
    }

    private Image createCircleLine(int color, int height, boolean first) {
        Image img = Image.createImage(height, height, 0);
        Graphics g = img.getGraphics();
        g.setAntiAliased(true);
        g.setColor(0xcccccc);
        int y = 0;
        if (first) {
            y = height / 6 + 1;
        }
        g.drawLine(height / 2, y, height / 2, height);
        g.drawLine(height / 2 - 1, y, height / 2 - 1, height);
        g.setColor(color);
        g.fillArc(height / 2 - height / 4, height / 6, height / 2, height / 2, 0, 360);
        return img;
    }

    @Override
    protected void showOtherForm(Resources res) {
        new afficheProduit(res).show();
    }

    @Override
    protected void showOtherForm2(Resources res) {
        new s_favoris(res).show();
    }

    @Override
    protected void showOtherForm3(Resources res) {
        new s_Panier(res).show();
    }

    private void refreshpanier() {
        ArrayList<Produit> liste = selectProduit();
        Produit.Panier = liste;
    }

    private void setcart(Produit p) {

        Produit.setPanier(p);
        System.out.println(p);

    }

}
