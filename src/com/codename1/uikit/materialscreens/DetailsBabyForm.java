/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codename1.uikit.materialscreens;

import com.allforkids.Entite.baby;
import com.allforkids.Entite.evenement;
import com.allforkids.Service.ServiceBaby;
import com.codename1.components.Ads;
import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.io.IOException;

/**
 *
 * @author ghada
 */
public class DetailsBabyForm extends SideMenuBaseForm1 {
    ServiceBaby s= new ServiceBaby();
       private Image img;
    private ImageViewer imgv;
    private EncodedImage enc ;
      private Resources theme = UIManager.initFirstTheme("/theme");
    public DetailsBabyForm(Resources res) {
        super(BoxLayout.y());
             
//        getUnselectedStyle().setBackgroundType(Style.BACKGROUND_GRADIENT_RADIAL);
//        getUnselectedStyle().setBackgroundGradientEndColor(0xffffff);
//    getUnselectedStyle().setBackgroundGradientStartColor(0x000000);
        Toolbar tb = getToolbar();
        tb.setTitleCentered(false);
        Image profilePic = res.getImage("user-picture.jpg");
 

        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_ARROW_BACK);
        menuButton.addActionListener(e-> new ListBabyForm(res).show());
        Label tit = new Label(""+ListBabyForm.specDetails.getNom(), "Title");
     
        
        

         Container titleCmp = BoxLayout.encloseY(
                        FlowLayout.encloseIn(menuButton),
                        BorderLayout.centerAbsolute(
                                BoxLayout.encloseY(
                                     new Label("", "SubTitle"),
                                   tit
                                )
                            ),
                        GridLayout.encloseIn(2)
                );
        
        
       // FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ADD);
        //fab.getAllStyles().setMarginUnit(Style.UNIT_TYPE_PIXELS);
        //fab.getAllStyles().setMargin(BOTTOM, completedTasks.getPreferredH() - fab.getPreferredH() / 2);
        tb.setTitleComponent(titleCmp);
                    
   
    
        
        FontImage arrowDown = FontImage.createMaterial(FontImage.MATERIAL_KEYBOARD_ARROW_DOWN, "Label", 3);
  
        baby Sp = new baby();
        System.out.println("test");
      
        // for (EntitySpecialiste spec : Sp.getListPediatre()) {
          //   System.out.println("test2");
    // addButtonBottom(profilePic, "ok", 0xd997f1, true);
   //}

        System.out.println("detail");
        
          addButtonBottom(profilePic, ListBabyForm.specDetails, 0xF69602, true);
      
    }
    
    private void addButtonBottom(Image arrowDown, baby spec, int color, boolean first) {
        
            Container  finishLandingPage = new Container(BoxLayout.y());
       
//        MultiButton finishLandingPage = new MultiButton(text);    
//        finishLandingPage.setEmblem(arrowDown.scaledHeight(50));
//        finishLandingPage.setUIID("Container");
//        finishLandingPage.setUIIDLine1("TodayEntry");
//        finishLandingPage.setIcon(createCircleLine(color, finishLandingPage.getPreferredH(),  first));
//        finishLandingPage.setIconUIID("Container");
//arrowDown.scaledWidth(100);
//finishLandingPage.add(arrowDown.scaledHeight(100));
try {
            enc = EncodedImage.create("/giphy.gif");
        } catch (IOException ex) {
            System.out.println("error encoder");
        }

       img = URLImage.createToStorage(enc, "imagee", "http://localhost/datatable_21/web/"+spec.getImage(), URLImage.RESIZE_SCALE);
   

img.fill( 500, 100);
imgv=new  ImageViewer(img.fill(300, 100));

finishLandingPage.add(imgv);
Label l = new Label( spec.getNom()+" ");


Label prixx = new Label("Prix :"+String.valueOf(spec.getPrix()));

l.getAllStyles().setFgColor(0xF69602);
//Label l2 = new Label("Organisateur: "+spec.getNomorg());
prixx.getAllStyles().setFgColor(0x57d973);


//l2.getAllStyles().setFgColor(0xF69602);
SpanLabel l3 = new SpanLabel("Description:  "+spec.getDescription());
//l3.getAllStyles().setBackgroundGradientEndColor(0xF69602);
 Container  liked = new Container(BoxLayout.x());
 Container  disliked = new Container(BoxLayout.x());
        Button likeb = new Button("like:  ");
Label like = new Label(String.valueOf(s.nbrelike(ListBabyForm.specDetails.getId())));
likeb.getAllStyles().setFgColor(0x57d973);
liked.add(likeb);
liked.add(like);
 Button dislikeb = new Button("Dislike:  ");

Label dislike = new Label(String.valueOf(s.nbredislike(ListBabyForm.specDetails.getId())));
dislikeb.getAllStyles().setFgColor(0x57d973);
disliked.add(dislikeb);
disliked.add(dislike);

dislikeb.addActionListener(e -> {
                int A=s.verifavis1(ListBabyForm.specDetails.getId());
   
       if(A==0)
       {
       
        
             System.out.println("Vous avez deja réagit l'offre ");
       }
       else if (A==1)
       {
           System.out.println("Votre avis sur l'offre a été modifié");
       
       dislike.setText(String.valueOf(Integer.parseInt(dislike.getText())+1));
       like.setText(String.valueOf(Integer.parseInt(like.getText())-1));
       
       }
       else {
             dislike.setText(String.valueOf(Integer.parseInt(dislike.getText())+1));
           System.out.println("Vous avez réagit sur l'offre");
             
       }
        
 
        });

likeb.addActionListener(e -> {
               
             int A=s.verifavis(ListBabyForm.specDetails.getId());
   
       if(A==0)
       {
       
           System.out.println("Vous avez deja aimé l'offre");
       }
       else if (A==1)
       {
        
            System.out.println("Votre avis sur l'offre a été modifié");
       dislike.setText(String.valueOf(Integer.parseInt(dislike.getText())-1));
       like.setText(String.valueOf(Integer.parseInt(like.getText())+1));
              
       }
       else {
         System.out.println("Vous avez aimé l'offre ");
       like.setText(String.valueOf(Integer.parseInt(like.getText())+1));     
       }
 
        });




Button btn_map = new Button("map");
          
          btn_map.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
              System.out.println("aaaa");
             // new MapAnnonce().Map();
               
               Form hi = new Form("Google Direction Service");
        
        final GoogleMap map = new GoogleMap();
       hi.setLayout(new BorderLayout());
        
        final TextField start = new TextField("Tunis");
      start.setHint("Start location");
       
       
        final TextField end = new TextField();
//        end.setHint(ev.getLieu());
        end.setHint("Tunis");
        
      Container form = new Container();
        form.setLayout(new BorderLayout());
//        form.addComponent(BorderLayout.NORTH, start);
//      form.addComponent(BorderLayout.SOUTH, end);
       
       ActionListener routeListener = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                
                
//                  ConnectionRequest con2 = new ConnectionRequest();
//        
//                con2.setUrl("http://localhost/datatable_21/web/app_dev.php/home/Mobileupdateavis?id="+avisbaby.get(0).getId()+"&nbrdislike="+0+"&nbrlike="+1+"&moyenne="+1+"&idu="+5+"&idb="+idb+"");
//
//       NetworkManager.getInstance().addToQueueAndWait(con2);
       
       
    
               if ( !"".equals(start.getText()) && !"".equals(end.getText())){
                    DirectionsRequest req = new DirectionsRequest();
                    req.setTravelMode(DirectionsRequest.TRAVEL_MODE_DRIVING);
                    //req.setOriginName(start.getText());
                    //req.setDestinationName("Tunis");
                    map.route(req, new DirectionsRouteListener(){
                        public void routeCalculated(DirectionsResult result) {
                            System.out.println("Successfully mapped route");
                        }
                        
                    });
                }
            }
            
        };
       
       start.addActionListener(routeListener);
        end.addActionListener(routeListener);
       
        hi.addComponent(BorderLayout.NORTH, form);
        
       hi.addComponent(BorderLayout.CENTER, map);
       hi.show();
            }
   });

//finishLandingPage.add(l2);
finishLandingPage.add(prixx);

Ads  a = new Ads ("ca-app-pub-3940256099942544/1033173712");
        
Container ADDS = new Container();             

finishLandingPage.add(l3);
finishLandingPage.add(ADDS);
finishLandingPage.add(liked);
finishLandingPage.add(disliked);

finishLandingPage.add(btn_map);





//
// Button loginButton = new Button(">> participer ");
//     finishLandingPage.add(loginButton);
//
//    loginButton.getAllStyles().setFgColor(0xE12336);
//        loginButton.addActionListener(e -> {
//            /*************** form redez-vous***************/
//            
////        s.delete(spec.getId());
////            System.out.println(spec.getId());
//        new ListBabyForm(theme).show();
//
//            /*******************************************/
//         
//        });

            // c.add(containervide);
//finishLandingPage.add(c);
//finishLandingPage.add(containervide);

        add(FlowLayout.encloseIn(finishLandingPage));
        
    }
    
   

    @Override
    protected void showOtherForm(Resources res) {
        new StatsForm(res).show();
    }
    
public void refresh()
{
     refreshTheme();
}

    
}

