/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codename1.uikit.materialscreens;

import com.allforkids.Entite.Produit;
import com.allforkids.Service.ProduitService;

import com.codename1.components.SpanLabel;
import com.codename1.components.ToastBar;
import com.codename1.db.Cursor;
import com.codename1.db.Database;
import com.codename1.db.Row;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author said
 */
public class s_favoris extends s_SideMenuBaseForm {

    private Database db;
    private EncodedImage enc;
    private Image img;
    private Image coeur;
    private Image coeur2;
            int idUser= LoginForm.UserConnected.getId();


    

    public ArrayList<Produit> selectFavoris() {
        ArrayList<Produit> ALLproducts = new ArrayList<>();
        try {
            Cursor c = db.executeQuery("select * from favoris where iduser="+idUser+"");
            while (c.next()) {
                Produit pr = new Produit();
                Row r = c.getRow();
                pr.setId(r.getInteger(0));
                pr.setNom(r.getString(1));
                pr.setQuantite(r.getInteger(2));
                pr.setStock(r.getInteger(3));
                pr.setPrix(r.getInteger(4));
                pr.setImage(r.getString(5));
                pr.setIduser(idUser);
                ALLproducts.add(pr);
            }

        } catch (IOException ex) {
            //Logger.getLogger(s_Panier.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("erreur");
        }
        System.out.println("allP" + ALLproducts);
        return ALLproducts;
    }

    
    public void favoris(Produit p) {
        try {
            //             User idcnx = Main.getUserCnx();
            db.execute("INSERT INTO `favoris` (`id`, `nom`, `quantite`, `stock`, `prix`,`image`,`iduser`) VALUES (" + p.getId()+ ",'" + p.getNom() + "'," + p.getQuantite() + "," + p.getStock() + "," + p.getPrix() + ",'" + p.getImage() + "'," + idUser + ");");
            //db.execute("INSERT INTO `favoris` (`id`,`iduser`, `nom`, `quantite`, `stock`, `prix`,`image`) VALUES ("+p.getId()+",null,'"+p.getNom()+"',"+p.getQuantite()+","+p.getStock()+","+p.getPrix()+",'"+p.getImage()+"');");
            //Dialog.show("favori Ajouté avec succes", "ajout", "ok", null);
            ToastBar.showErrorMessage("favori Ajouté avec succes",6);

            System.out.println("favoris ok");
        } catch (IOException ex) {
            System.out.println("erreur d'insertion");
            System.out.println(ex);
        }
    }

    public boolean verifFavoris(Produit p) {
        ArrayList<Produit> ALLproducts = new ArrayList<>();
        Produit P2 = new Produit();
        boolean test = false;
        Cursor c;
        try {
            c = db.executeQuery("select * from favoris where id="+p.getId()+ " AND iduser="+ idUser +";");
            while (c.next()) {
                Row r = c.getRow();
                P2.setId(r.getInteger(0));
                ALLproducts.add(P2);
            }
        } catch (IOException ex) {
        }

        return ALLproducts.size() == 1;
    }

    
    
    private void setcart(Produit p) {

        Produit.setPanier(p);
        System.out.println(p);

    }
    

    public void acheter(Produit p) {
        try {
            setcart(p);
            //User idcnx = Main.getUserCnx();
            db.execute("INSERT INTO `panier` (`id`, `nom`, `quantite`, `stock`, `prix`, `image`, `iduser`) VALUES (" + p.getId() + ",'" + p.getNom() + "'," + p.getQuantite() + "," + p.getStock() + "," + p.getPrix() + ",'" + p.getImage() + "'," + idUser + ");");
            Dialog.show("Produit Ajouté avec succes", "ajout", "ok", null);
            System.out.println("ajout ok");

        } catch (IOException ex) {
            System.out.println("erreur d'insertion");
        }

    }

    public void update(Produit p, int id) {
        try {
            setcart(p);
            db.execute("UPDATE `panier` SET `quantite`=" + p.getQuantite() + " WHERE `id`=" + id + ";");
            System.out.println("update effectuer");

        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    public s_favoris(Resources res) {
        super(BoxLayout.y());

        try {
            db = Database.openOrCreate("firaspidev28");
        } catch (IOException ex) {
        }
        Toolbar tb = getToolbar();
        tb.setTitleCentered(false);
        Image profilePic = res.getImage("user-picture.jpg");
        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());
        Label tit = new Label("Favoris", "Title");
        // tit.getAllStyles().setFgColor(0xE12336);
        Container titleCmp = BoxLayout.encloseY(
                FlowLayout.encloseIn(menuButton),
                BorderLayout.centerAbsolute(
                        BoxLayout.encloseY(
                                //                                new Label("amal", "SubTitle"),
                                tit
                        )
                ),
                GridLayout.encloseIn(2)
        );

        tb.setTitleComponent(titleCmp);

        Label Liste = new Label("");

        Label Liste0 = new Label(" ");
        Liste.getAllStyles().setFgColor(0xE12336);

        Container listCon = BoxLayout.encloseY(
                BorderLayout.centerAbsolute(
                        BoxLayout.encloseY(
                                Liste
                        )
                ),
                GridLayout.encloseIn(2)
        );
        FontImage arrowDown = FontImage.createMaterial(FontImage.MATERIAL_KEYBOARD_ARROW_DOWN, "Label", 3);

        ProduitService pr_service = new ProduitService();
        getUnselectedStyle().setBackgroundType(Style.BACKGROUND_GRADIENT_RADIAL);
        getUnselectedStyle().setBackgroundGradientEndColor(0xFFFFFF);
        getUnselectedStyle().setBackgroundGradientStartColor(0xFFFFFF);

        ArrayList<Produit> lis = new ArrayList<Produit>();
        lis = selectFavoris();
        System.out.println("favoris list " + lis);

        for (int i = 0; i < lis.size(); i++) {
            afficheFav(profilePic, lis.get(i), 0xd997f1, true, i);
        }

        setupSideMenu(res);
    }

    private void afficheFav(Image arrowDown, Produit p, int color, boolean first, int i) {
        Container c = new Container(BoxLayout.y());
        Container c1 = new Container(BoxLayout.y());
        Container c2 = new Container(BoxLayout.x());
        Container all = new Container(BoxLayout.x());
        Container videC = new Container(BoxLayout.y());
        try {
            enc = EncodedImage.create("/giphy.gif");
        } catch (IOException ex) {

        }
        img = URLImage.createToStorage(enc, p.getImage(), "http://localhost/pidev/web" + p.getImage(), URLImage.RESIZE_SCALE_TO_FILL);
        img.fill(50, 50);
        coeur = URLImage.createToStorage(enc, "coeur.png" + i, "http://localhost/pidev/web/images/coeur.png", URLImage.RESIZE_SCALE_TO_FILL);
        coeur.fill(50, 50);
        coeur2 = URLImage.createToStorage(enc, "coeur2.png", "http://localhost/pidev/web/images/coeur2.png", URLImage.RESIZE_SCALE_TO_FILL);
        coeur2.fill(50, 50);
        //image1.add(coeur.scaledWidth(30));
        Label quantite = new Label();
        quantite.setText(String.valueOf(p.getQuantite()));

        SpanLabel l = new SpanLabel(p.getNom());
        l.getAllStyles().setFgColor(0xF69602);

        quantite.setText(String.valueOf(p.getQuantite()));
        Label l2 = new Label();
        l2.setText(String.valueOf(p.getPrix()) + " TND");
        l2.getAllStyles().setFgColor(0xF69602);
        Button fav = new Button("fav");
        Button acheter = new Button("acheter");
        acheter.addActionListener(e -> {

            if (Produit.getPanier().contains(p)) {
                for (int m = 0; m < Produit.getPanier().size(); m++) {
                    if (Produit.getPanier().get(m).getId() == p.getId()) {
                        if (Produit.getPanier().get(m).getQuantite() < p.getStock()) {
                            // Verif=Produit.setPanier(p);
                            update(Produit.getPanier().get(m), Produit.getPanier().get(m).getId());

                            //getShowPane();
                        } else {
                            System.out.println("depassez stock");
                        }
                    }
                }
            } else {
                if (p.getStock() > 0) {  //Verif=Produit.setPanier(p);
                    acheter(p);
                    //  getShowPane();             
                } else {
                    System.out.println("depasser");
                }
            }
        });
        Container image1 = new Container(BoxLayout.x());
        Container image5 = new Container(BoxLayout.x());
        image1.add(coeur.scaledWidth(25));
        image5.add(coeur2.scaledWidth(25));
        if (verifFavoris(p)) {
            c2.addAll(image5, acheter);
        } else {
            c2.addAll(image1, acheter);
        }

        fav.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (verifFavoris(p)) {
                    try {
                        db.execute("DELETE FROM favoris WHERE id = " +p.getId() + "");
                        c2.replace(image5, image1, null);
                        refreshTheme();
                    } catch (IOException ex) {
                    }
                } else {
                    favoris(p);
                    c2.replace(image1, image5, null);
                    refreshTheme();
                }
            }
        });

        image1.setLeadComponent(fav);
        image5.setLeadComponent(fav);
        c1.add(l);
        c1.add(l2);
        //c2.addAll(fav, acheter);
        c.addAll(c1, c2);
        //all.getStyle().setBorder(Border.createLineBorder(1));
        all.add(img.scaledWidth(70));
        all.add(c);
        add(FlowLayout.encloseIn(all));
//        }

    }

    @Override
    protected void showOtherForm(Resources res) {
        new afficheProduit(res).show();
    }

    @Override
    protected void showOtherForm2(Resources res) {
        new s_favoris(res).show();
    }

    @Override
    protected void showOtherForm3(Resources res) {
        new s_Panier(res).show();
    }

}
