/*
 * Copyright (c) 2016, Codename One
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

package com.codename1.uikit.materialscreens;

import com.allforkids.Entite.evenement;
import com.allforkids.Entite.participer;
import com.allforkids.Entite.produit_donation;
import com.allforkids.Service.ServiceEvenement;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.ImageViewer;
import com.codename1.components.MultiButton;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Represents a user profile in the app, the first form we open after the walkthru
 *
 * @author Shai Almog
 */
public class DetailDoForm extends SideMenuBaseFormDon {
    ServiceEvenement s= new ServiceEvenement();
       private Image img;
    private ImageViewer imgv;
    private EncodedImage enc ;
      private Resources theme = UIManager.initFirstTheme("/theme");
    public DetailDoForm(Resources res) {
        super(BoxLayout.y());
             
        getUnselectedStyle().setBackgroundType(Style.BACKGROUND_GRADIENT_RADIAL);
        getUnselectedStyle().setBackgroundGradientEndColor(0xffffff);
    getUnselectedStyle().setBackgroundGradientStartColor(0x000000);
        Toolbar tb = getToolbar();
        tb.setTitleCentered(false);
        Image profilePic = res.getImage("user-picture.jpg");
 

        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_ARROW_BACK);
        menuButton.addActionListener(e-> new ListEvForm(res).show());
        Label tit = new Label(""+ListDoForm.specDetails.getNom(), "Title");
     
        
        

         Container titleCmp = BoxLayout.encloseY(
                        FlowLayout.encloseIn(menuButton),
                        BorderLayout.centerAbsolute(
                                BoxLayout.encloseY(
                                     new Label("", "SubTitle"),
                                   tit
                                )
                            ),
                        GridLayout.encloseIn(2)
                );
        
        
       // FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ADD);
        //fab.getAllStyles().setMarginUnit(Style.UNIT_TYPE_PIXELS);
        //fab.getAllStyles().setMargin(BOTTOM, completedTasks.getPreferredH() - fab.getPreferredH() / 2);
        tb.setTitleComponent(titleCmp);
                    
   
    
        
        FontImage arrowDown = FontImage.createMaterial(FontImage.MATERIAL_KEYBOARD_ARROW_DOWN, "Label", 3);
  
        evenement Sp = new evenement();
        
        
        System.out.println("test");
      
        // for (EntitySpecialiste spec : Sp.getListPediatre()) {
          //   System.out.println("test2");
    // addButtonBottom(profilePic, "ok", 0xd997f1, true);
   //}
   
        System.out.println("detail");
       
            addButtonBottom(profilePic, ListDoForm.specDetails , 0xd997f1, true);
        
          //addButtonBottom(profilePic, ListEvForm.specDetails , 0xF69602, true);
      
    }
   

 
 

    
    
    
    
    private void addButtonBottom(Image arrowDown, produit_donation spec,int color, boolean first) {
        
            Container  finishLandingPage = new Container(BoxLayout.y());
       
//        MultiButton finishLandingPage = new MultiButton(text);    
//        finishLandingPage.setEmblem(arrowDown.scaledHeight(50));
//        finishLandingPage.setUIID("Container");
//        finishLandingPage.setUIIDLine1("TodayEntry");
//        finishLandingPage.setIcon(createCircleLine(color, finishLandingPage.getPreferredH(),  first));
//        finishLandingPage.setIconUIID("Container");
//arrowDown.scaledWidth(100);
//finishLandingPage.add(arrowDown.scaledHeight(100));
try {
            enc = EncodedImage.create("/giphy.gif");
        } catch (IOException ex) {
            System.out.println("error encoder");
        }

       img = URLImage.createToStorage(enc, spec.getImage(),"http://localhost/datatable_21/web"+spec.getImage(), URLImage.RESIZE_SCALE);
   

img.fill( 500, 100);
imgv=new  ImageViewer(img.fill(300, 100));

finishLandingPage.add(imgv);
Label l = new Label( spec.getNom()+" ");


Label prixx = new Label("Télephone :"+String.valueOf(spec.getTel()));

l.getAllStyles().setFgColor(0xF69602);



     
 Label l2 = new Label("Donateur: "+spec.getNomuser());
 
 
prixx.getAllStyles().setFgColor(0x57d973);



l2.getAllStyles().setFgColor(0xF69602);
SpanLabel l3 = new SpanLabel("Description:  "+spec.getDescription());
l3.getAllStyles().setBackgroundGradientEndColor(0xF69602);

 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

 



      
finishLandingPage.add(l2);
finishLandingPage.add(prixx);

finishLandingPage.add(l3);

  /*************   userr    ***********/
     

 
  
        System.out.println("---------_________________________-----");
        

 
  
    


   
 
  

/***************************/

   //}
   
   //}


        add(FlowLayout.encloseIn(finishLandingPage));
        
    }
    
   

    @Override
    protected void showOtherForm(Resources res) {
        new StatsForm(res).show();
    }
    
    }
