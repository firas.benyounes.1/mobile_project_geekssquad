/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codename1.uikit.materialscreens;

import com.allforkids.Entite.Produit;
import com.codename1.components.SpanLabel;
import com.codename1.db.Cursor;
import com.codename1.db.Database;
import com.codename1.db.Row;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import java.util.ArrayList;
import com.allforkids.Service.ProduitService;

import com.codename1.components.ToastBar;
import com.codename1.ui.BrowserComponent;
import com.codename1.ui.ComponentGroup;
import com.codename1.ui.Form;


/**
 *
 * @author said
 */
public class s_Panier extends s_SideMenuBaseForm {

    ProduitService sv = new ProduitService();
    private EncodedImage enc;
    Boolean created = false;
    private Image img;
    private Image coeur;
    private Image coeur2;
    private Image moins;
    private Image plus;
    private Image poubelle;
    private Database db;
    Button commande;
   int idUser= LoginForm.UserConnected.getId();
    ConnectionRequest con = new ConnectionRequest();
    Label prixtotal = new Label();
    private Resources theme = UIManager.initFirstTheme("/theme");

    public s_Panier(Resources res) {

        super(BoxLayout.y());
        
        created = Database.exists("firaspidev28");
        try {
            db = Database.openOrCreate("firaspidev28");

            if (created == true) {
                //db.execute("CREATE TABLE favoris (id INTEGER PRIMARY KEY , iduser INTEGER ,nom TEXT ,quantite INTEGER , stock INTEGER  ,  prix INTEGER , image TEXT ,etat INTEGER);");
                db.execute("CREATE TABLE favoris (id INTEGER PRIMARY KEY ,nom TEXT ,quantite INTEGER , stock INTEGER  ,  prix INTEGER , image TEXT,iduser INTEGER );");
            }
        } catch (IOException ex) {

        }

        Toolbar tb = getToolbar();
        tb.setTitleCentered(false);
        Image profilePic = res.getImage("user-picture.jpg");
        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());
        Label tit = new Label("Liste des produits", "Title");
        // tit.getAllStyles().setFgColor(0xE12336);
        Container titleCmp = BoxLayout.encloseY(
                FlowLayout.encloseIn(menuButton),
                BorderLayout.centerAbsolute(
                        BoxLayout.encloseY(
                            tit
                        )
                ),
                GridLayout.encloseIn(2)
        );
        tb.setTitleComponent(titleCmp);
        Label Liste = new Label("");
        Label Liste0 = new Label(" ");
        Liste.getAllStyles().setFgColor(0xE12336);
        Container listCon = BoxLayout.encloseY(
                BorderLayout.centerAbsolute(
                        BoxLayout.encloseY(
                                Liste
                        )
                ),
                GridLayout.encloseIn(2)
        );
        FontImage arrowDown = FontImage.createMaterial(FontImage.MATERIAL_KEYBOARD_ARROW_DOWN, "Label", 3);
        getUnselectedStyle().setBackgroundType(Style.BACKGROUND_GRADIENT_RADIAL);
        getUnselectedStyle().setBackgroundGradientEndColor(0xFFFFFF);
        getUnselectedStyle().setBackgroundGradientStartColor(0xFFFFFF);
        setupSideMenu(res);
        refresh();
        commande = new Button(">> Passer la commande");

        ArrayList<Produit> list = selectProduit();
        ArrayList<Produit> list2 = selectFavoris();
        System.out.println("list fav "+list2);
        int prixtot = 0;
        for (int i = 0; i < list.size(); i++) {
            prixtot = prixtot + list.get(i).getQuantite() * list.get(i).getPrix();
            System.out.println("");
        }
        prixtotal.setText(String.valueOf(prixtot));
        add(prixtotal);
        /////////********************PASSER commande*********************/////////////////////////// 
        commande.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                ArrayList<Produit> list = selectProduit();
                ArrayList<Produit> list2 = sv.getListProduit();
                Boolean Test = true;
                for (int i = 0; i < list.size(); i++) {
                    System.out.println("commande " + list.get(i));
                    System.out.println("commande qnt " + list.get(i).getQuantite());
                    for (int j = 0; j < list2.size(); j++) {
                        if(list2.get(j).getId()==list.get(i).getId()){
                        if (list2.get(j).getStock() < list.get(i).getQuantite()) {
                            Test = false;
                                System.out.println("stock"+list2.get(j).getStock());
                            System.out.println("qnt"+list.get(i).getQuantite());
                        }
                    }
                        }

                        if (Test) {
                            int stock = list.get(i).getStock() - list.get(i).getQuantite();
                            String Url = "http://localhost/pidev/web/app_dev.php/boutique/mobileCommande?idp=" + list.get(i).getId() + "&nom=" + list.get(i).getNom() + "&quantite=" + list.get(i).getQuantite() + "&stock=" + stock + "&prix=" + list.get(i).getPrix() + "&image=" + list.get(i).getImage() +"&idc="+ idUser+"";
                            String Url2 = "http://localhost/pidev/web/app_dev.php/boutique/updatestockMobile/" + list.get(i).getId() + "?qnt=" + list.get(i).getQuantite() + "";
                            con.setUrl(Url);
                             con.addResponseListener((e) -> {
                                String str = new String(con.getResponseData());
                                try {
                                    //System.out.println(str);
                                    db.execute("DELETE FROM panier WHERE iduser = " + LoginForm.UserConnected.getId() + "");
                                } catch (IOException ex) {
                                }
                            });
                            NetworkManager.getInstance().addToQueueAndWait(con);
                            con.setUrl(Url2);
                             con.addResponseListener((e) -> {
                                String str = new String(con.getResponseData());
                                //System.out.println(str);

                            });
                            NetworkManager.getInstance().addToQueueAndWait(con);
                            payer(Integer.parseInt(prixtotal.getText()));
                        }

                    
                
                if (Test == false) {
                              
                    Dialog.show("L'un de vos produits est épuisé", "Warning", "ok", null);
                }
                }
            }
        });
        add(commande);

    }

    public ArrayList<Produit> selectFavoris() {
        ArrayList<Produit> ALLproducts = new ArrayList<>();
        try {
            Cursor c = db.executeQuery("select * from favoris where iduser="+idUser+"");
            while (c.next()) {
                Produit pr = new Produit();
                Row r = c.getRow();
                pr.setId(r.getInteger(0));
                pr.setNom(r.getString(1));
                pr.setQuantite(r.getInteger(2));
                pr.setStock(r.getInteger(3));
                pr.setPrix(r.getInteger(4));
                pr.setImage(r.getString(5));
                pr.setIduser(idUser);
                ALLproducts.add(pr);
            }

        } catch (IOException ex) {
            //Logger.getLogger(s_Panier.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("erreur");
        }
        System.out.println("allP" + ALLproducts);
        return ALLproducts;
    }

    
    public void favoris(Produit p) {
        try {
            //             User idcnx = Main.getUserCnx();
            db.execute("INSERT INTO `favoris` (`id`, `nom`, `quantite`, `stock`, `prix`,`image`,`iduser`) VALUES (" + idUser*1000+p.getId() + ",'" + p.getNom() + "'," + p.getQuantite() + "," + p.getStock() + "," + p.getPrix() + ",'" + p.getImage() + "'," + idUser + ");");
            //db.execute("INSERT INTO `favoris` (`id`,`iduser`, `nom`, `quantite`, `stock`, `prix`,`image`) VALUES ("+p.getId()+",null,'"+p.getNom()+"',"+p.getQuantite()+","+p.getStock()+","+p.getPrix()+",'"+p.getImage()+"');");
            //Dialog.show("favori Ajouté avec succes", "ajout", "ok", null);
            ToastBar.showErrorMessage("favori Ajouté avec succes",6);

            System.out.println("favoris ok");
        } catch (IOException ex) {
            System.out.println("erreur d'insertion");
            System.out.println(ex);
        }
    }

    public boolean verifFavoris(Produit p) {
        ArrayList<Produit> ALLproducts = new ArrayList<>();
        Produit P2 = new Produit();
        boolean test = false;
        Cursor c;
        try {
            c = db.executeQuery("select * from favoris where id=" + idUser*1000+p.getId()+ " AND iduser="+ idUser +";");
            while (c.next()) {
                Row r = c.getRow();
                P2.setId(r.getInteger(0));
                ALLproducts.add(P2);
            }
        } catch (IOException ex) {
        }

        return ALLproducts.size() == 1;
    }

    public void refresh() {
        ArrayList<Produit> list = selectProduit();
        for (int i = 0; i < list.size(); i++) {
            affiche(list.get(i), 0xd997f1, true, i);
        }
    }

        public ArrayList<Produit> selectProduit() {
            ArrayList<Produit> ALLproducts = new ArrayList<>();
            try {
                Cursor c = db.executeQuery("select * from panier where iduser="+idUser+"");
                while (c.next()) {
                    Produit pr = new Produit();
                    Row r = c.getRow();
                    pr.setId(r.getInteger(0));
                    pr.setNom(r.getString(1));
                    pr.setQuantite(r.getInteger(2));
                    pr.setStock(r.getInteger(3));
                    pr.setPrix(r.getInteger(4));
                    pr.setImage(r.getString(5));
                    ALLproducts.add(pr);
                }

            } catch (IOException ex) {
                System.out.println("erreur");
            }
            System.out.println("allP" + ALLproducts);
            return ALLproducts;
        }

    public void payer( int total)
    {
       
        //User u = Main.getUserCnx();

       Form hi = new Form("Browser", new BorderLayout());
    BrowserComponent browser = new BrowserComponent();
   
    //browser.setURL("http://localhost/new/tranche.php?MONTANT="+total+"&NAME="+u.getName()+"&EMAIL="+u.getEmail());
 browser.setURL("http://localhost/payement/a.php?MONTANT="+total+"&NAME="+LoginForm.UserConnected.getNom()+" &EMAIL="+LoginForm.UserConnected.getEmail());
 hi.add(BorderLayout.CENTER ,browser);
 hi.show();

    }

    
    
    public void affiche(Produit p, int color, boolean first, int i) {
        Container c = new Container(BoxLayout.x());
        Container videC = new Container(BoxLayout.y());
        try {
            enc = EncodedImage.create("/giphy.gif");
        } catch (IOException ex) {

        }
        img = URLImage.createToStorage(enc, p.getImage(), "http://localhost/pidev/web" + p.getImage(), URLImage.RESIZE_SCALE_TO_FILL);
        img.fill(50, 50);
        coeur = URLImage.createToStorage(enc, "coeur.png" + i, "http://localhost/pidev/web/images/coeur.png", URLImage.RESIZE_SCALE_TO_FILL);
        coeur.fill(50, 50);
        coeur2 = URLImage.createToStorage(enc, "coeur2.png", "http://localhost/pidev/web/images/coeur2.png", URLImage.RESIZE_SCALE_TO_FILL);
        coeur2.fill(50, 50);
        moins = URLImage.createToStorage(enc, "moins.png", "http://localhost/pidev/web/images/moins.png", URLImage.RESIZE_SCALE_TO_FILL);
        moins.fill(50, 50);
        plus = URLImage.createToStorage(enc, "plus.png", "http://localhost/pidev/web/images/plus.png", URLImage.RESIZE_SCALE_TO_FILL);
        plus.fill(50, 50);
        poubelle = URLImage.createToStorage(enc, "poubelle.png", "http://localhost/pidev/web/images/poubelle.png", URLImage.RESIZE_SCALE_TO_FILL);
        poubelle.fill(50, 50);
        Container c2 = new Container(BoxLayout.y());
        Container imageContainer = new Container(BoxLayout.x());
        Container image1 = new Container(BoxLayout.x());
        Container image2 = new Container(BoxLayout.x());
        Container all = new Container(BoxLayout.y());
        Container image3 = new Container(BoxLayout.x());
        Container image4 = new Container(BoxLayout.x());
        Container image5 = new Container(BoxLayout.x());
        image1.add(coeur.scaledWidth(30));
        Label quantite = new Label();
        quantite.setText(String.valueOf(p.getQuantite()));
        Label vide = new Label("  ");
        Label vide2 = new Label("     ");
        image2.add(poubelle.scaledWidth(25));
        image3.add(moins.scaledWidth(25));
        image4.add(plus.scaledWidth(25));
        image5.add(coeur2.scaledWidth(25));
        Button b1 = new Button();
        Button b2 = new Button();
        Button b3 = new Button();
        Button b4 = new Button();
        if (verifFavoris(p)) {
            imageContainer.addAll(image5, vide, image2, vide2, image3, quantite, image4);
        } else {
            imageContainer.addAll(image1, vide, image2, vide2, image3, quantite, image4);
        }

        c.add(img.scaledWidth(70));
        SpanLabel l = new SpanLabel(p.getNom());
        l.getAllStyles().setFgColor(0xF69602);

        quantite.setText(String.valueOf(p.getQuantite()));
        Label l2 = new Label();
        l2.setText(String.valueOf(p.getPrix() * p.getQuantite()) + " TND");

        l2.getAllStyles().setFgColor(0xF69602);

        //*******************FAVORIS******************///
        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (verifFavoris(p)) {
                    try {
                        db.execute("DELETE FROM favoris WHERE id = " + idUser*1000+p.getId() + "");
                        imageContainer.replace(image5, image1, null);
                        refreshTheme();
                    } catch (IOException ex) {
                    }
                } else {
                    favoris(p);
                    imageContainer.replace(image1, image5, null);
                    refreshTheme();
                }
            }
        });

        //*******************SUPPRIMER quantite******************///
        b2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                try {
                    db.execute("DELETE FROM panier WHERE id = " + p.getId() + "");

                    int px = Integer.parseInt(prixtotal.getText());
                    px = px - (p.getPrix() * p.getQuantite());
                    prixtotal.setText(String.valueOf(px));

                    Produit.getPanier().remove(p);

                    Produit.getPanier();
                    all.removeComponent(c);
                    all.removeComponent(imageContainer);
                    removeComponent(all);
                    refreshTheme();
                    Dialog.show("votre produit a été supprimé", "supression", "ok", null);
                } catch (IOException ex) {
                }
                p.setQuantite(p.getQuantite());
            }
        });

        //*******************MOIN quantite******************///     
        b3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                p.setQuantite(p.getQuantite() - 1);
                for (int m = 0; m < Produit.getPanier().size(); m++) {
                    if (Produit.getPanier().get(m).getId() == p.getId()) {
                        Produit.getPanier().get(m).setQuantite(Produit.getPanier().get(m).getQuantite() - 1);
                        quantite.setText(String.valueOf(Produit.getPanier().get(m).getQuantite()));

                        try {
                            db.execute("UPDATE `panier` SET `quantite`=" + Produit.getPanier().get(m).getQuantite() + " WHERE `id`=" + p.getId() + ";");
                        } catch (IOException ex) {
                        }
                        l2.setText(String.valueOf(p.getPrix() * p.getQuantite()) + " TND");
                        int px = Integer.parseInt(prixtotal.getText());
                        px = px - p.getPrix();
                        prixtotal.setText(String.valueOf(px));

                        System.out.println("quantite diminiue");
                        System.out.println(p.getQuantite());
                    }

                }
                if (p.getQuantite() == 0) {
                    try {
                        System.out.println("remove");
                        db.execute("DELETE FROM panier WHERE id = " + p.getId() + "");
                        Produit.getPanier().remove(p);

                        Produit.getPanier();
//                    all.removeComponent(c);
//                    all.removeComponent(imageContainer);

                    
                     
                               all.removeComponent(c);
                                  
                    all.removeComponent(imageContainer);
                     refreshTheme();
                    Dialog.show("votre produit a été supprimé", "supression", "ok", null);
                    } catch (IOException ex) {
                    }
                    p.setQuantite(p.getQuantite());
                }
            }
        });

        //*******************PLUS quantite******************///
        b4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {

                if (Produit.getPanier().contains(p)) {
                    for (int m = 0; m < Produit.getPanier().size(); m++) {
                        if (Produit.getPanier().get(m).getId() == p.getId()) {
                            if (Produit.getPanier().get(m).getQuantite() < p.getStock()) {
                                p.setQuantite(p.getQuantite() + 1);

                                update(Produit.getPanier().get(m), Produit.getPanier().get(m).getId());
                                quantite.setText(String.valueOf(Produit.getPanier().get(m).getQuantite()));
                                l2.setText(String.valueOf(p.getPrix() * p.getQuantite()) + " TND");
                                int px = Integer.parseInt(prixtotal.getText());
                                px = px + p.getPrix();
                                prixtotal.setText(String.valueOf(px));

                                //getShowPane();
                            } else {
                                System.out.println("depassez stock");
                            }
                        }
                    }
                }
                System.out.println(p.getQuantite());
            }
        });

        image4.setLeadComponent(b4);
        image3.setLeadComponent(b3);
        image2.setLeadComponent(b2);
        image1.setLeadComponent(b1);
        image5.setLeadComponent(b1);
        c2.add(l);
        c2.add(l2);
        c.add(c2);
        all.getStyle().setBorder(Border.createLineBorder(1));
        all.add(c);
        all.add(imageContainer);
        //add(ComponentGroup.enclose(all));

        add(FlowLayout.encloseIn(all));
        
//        }

    }

    @Override
    protected void showOtherForm(Resources res) {
        new afficheProduit(res).show();
    }

    @Override
    protected void showOtherForm2(Resources res) {
        new s_favoris(res).show();
    }

    @Override
    protected void showOtherForm3(Resources res) {
    new s_Panier(res).show();
    }


    public void update(Produit p, int id) {
        try {
            Produit.setPanier(p);
            db.execute("UPDATE `panier` SET `quantite`=" + p.getQuantite() + " WHERE `id`=" + id + ";");
            System.out.println("update effectuer");

        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
}
