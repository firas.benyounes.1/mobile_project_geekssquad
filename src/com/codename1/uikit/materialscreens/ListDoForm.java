/*
 * Copyright (c) 2016, Codename One
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package com.codename1.uikit.materialscreens;

import com.allforkids.Entite.evenement;
import com.allforkids.Entite.participer;
import com.allforkids.Entite.produit_donation;
import com.allforkids.Service.ServiceDo;
import com.allforkids.Service.ServiceEvenement;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.ImageViewer;
import com.codename1.components.MultiButton;
import com.codename1.components.SpanLabel;
import com.codename1.db.Cursor;
import com.codename1.db.Database;
import com.codename1.db.Row;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Represents a user profile in the app, the first form we open after the
 * walkthru
 *
 * @author Shai Almog
 */
public class ListDoForm extends SideMenuBaseFormDon {

    private Image img;
    private ImageViewer imgv;
    private EncodedImage enc;
    
     private Image imgg;
    private ImageViewer imgvv;
    private EncodedImage encc;
    public static produit_donation specDetails = new produit_donation();
    private Resources theme = UIManager.initFirstTheme("/theme");
Database db;
    boolean created= false ;
    private Image coeur;
    private Image coeur2;
    
    public ArrayList<produit_donation> selectFavoris() {
        ArrayList<produit_donation> ALLproducts = new ArrayList<>();
        try {
            Cursor c = db.executeQuery("select * from favoris");
            while (c.next()) {
                produit_donation pr = new produit_donation();
                Row r = c.getRow();
                pr.setId(r.getInteger(0));
                pr.setNom(r.getString(1));
                pr.setTel(r.getInteger(2));
                pr.setGenre(r.getString(3));
                
                pr.setImage(r.getString(4));
                ALLproducts.add(pr);
            }

        } catch (IOException ex) {
            //Logger.getLogger(s_Panier.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("erreur");
        }
        System.out.println("allP" + ALLproducts);
        return ALLproducts;
    }
  
    public void favoris(produit_donation p) {
        try {
            //             User idcnx = Main.getUserCnx();
            db.execute("INSERT INTO favoris (id, nom, tel, genre,`image`,idu) VALUES (" + p.getId() + ",'" + p.getNom() + "'," + p.getTel()+ ",'" + p.getGenre()+ "','" + p.getImage() + "','" + LoginForm.UserConnected.getId() + "');");
            //db.execute("INSERT INTO favoris (id,`iduser`, nom, quantite, stock, prix,`image`) VALUES ("+p.getId()+",null,'"+p.getNom()+"',"+p.getQuantite()+","+p.getStock()+","+p.getPrix()+",'"+p.getImage()+"');");
            Dialog.show("favori Ajouté avec succes", "ajout", "ok", null);
            System.out.println("favoris ok");
        } catch (IOException ex) {
            System.out.println("erreur d'insertion");
            System.out.println(ex);
        }
    }
    
    public boolean verifFavoris(produit_donation p) {
        ArrayList<produit_donation> ALLproducts = new ArrayList<>();
        produit_donation P2 = new produit_donation();
        boolean test = false;
        Cursor c;
        try {
            c = db.executeQuery("select * from favoris where  idu=" +LoginForm.UserConnected.getId() + ";");
            while (c.next()) {
                Row r = c.getRow();
                P2.setId(r.getInteger(0));
                ALLproducts.add(P2);
            }
        } catch (IOException ex) {
        }

        return ALLproducts.size() == 1;
    }
    
    public ListDoForm(Resources res) {

        super(BoxLayout.y());
created = Database.exists("cc");
        try {
            db = Database.openOrCreate("cc");

            if (created == false) {

                db.execute("CREATE TABLE favoris (id INTEGER PRIMARY KEY ,nom TEXT ,tel INTEGER , genre TEXT  , image TEXT ,idu INTEGER);");

            }
        } catch (IOException ex) {

        }
        Toolbar tb = getToolbar();
        tb.setTitleCentered(false);
        Image profilePic = res.getImage("user-picture.jpg");

        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());
        Label tit = new Label("Donations", "Title");
    // tit.getAllStyles().setFgColor(0xE12336);
        Container titleCmp = BoxLayout.encloseY(
                FlowLayout.encloseIn(menuButton),
                BorderLayout.centerAbsolute(
                        BoxLayout.encloseY(
                               new Label("", "SubTitle"),
                                tit
                        )
                ),
                GridLayout.encloseIn(2)
        );

        tb.setTitleComponent(titleCmp);

        Label Liste = new Label("");
     
        Label Liste0 = new Label(" ");
        Liste.getAllStyles().setFgColor(0xE12336);

        Container listCon = BoxLayout.encloseY(
                BorderLayout.centerAbsolute(
                        BoxLayout.encloseY(
                                Liste
                        )
                ),
                GridLayout.encloseIn(2)
        );
       // add(listCon);
ComboBox gov = new ComboBox("Ariana","Béja","Ben Arous","Bizerte","Gabès","Gafsa","Jendouba","Kairouan","Kasserine","Kébili","Le Kef","Mahdia","La Manouba","Médenine","Monastir","Nabeul","Sfax","Sidi Bouzid","Siliana","Sousse","Zaghouan","Tataouine","Tozeur","Tunis");
gov.setPreferredW(50);
gov.getAllStyles().setBgColor(0xFFFFFF);

//add(gov);
// gov.getAllStyles().setMargin(LEFT, 0);
        FontImage arrowDown = FontImage.createMaterial(FontImage.MATERIAL_KEYBOARD_ARROW_DOWN, "Label", 3);

        ServiceDo Sp = new ServiceDo();
        System.out.println("test");
        getUnselectedStyle().setBackgroundType(Style.BACKGROUND_GRADIENT_RADIAL);
        getUnselectedStyle().setBackgroundGradientEndColor(0xFFFFFF);
        getUnselectedStyle().setBackgroundGradientStartColor(0xFFFFFF);

        // for (EntitySpecialiste spec : Sp.getListPediatre()) {
        //   System.out.println("test2");
        // addButtonBottom(profilePic, "ok", 0xd997f1, true);
        //}
        ArrayList<produit_donation> lis = new ArrayList<produit_donation>();
         ArrayList<participer> lisPart = new ArrayList<participer>();
        lis = Sp.getListDo();
      
        System.out.println("test2");

        for (int i = 0; i < lis.size(); i++) {
            addButtonBottom(profilePic, lis.get(i), 0xd997f1, true, i);
        }
        setupSideMenu(res);
    }

    private void addButtonBottom(Image arrowDown, produit_donation spec, int color, boolean first, int i) {

        Container finishLandingPage = new Container(BoxLayout.x());
        Container c = new Container(BoxLayout.y());
        Container containervide = new Container(BoxLayout.x());
        Label spaceLabel0 = new Label(" ");
        Label spaceLabel2 = new Label(" ");
        containervide.add(spaceLabel0);
        containervide.add(spaceLabel2);

        try {
            enc = EncodedImage.create("/giphy.gif");
        } catch (IOException ex) {
            System.out.println("error encoder");
        }

        img = URLImage.createToStorage(enc, spec.getImage(), "http://localhost/datatable_21/web" + spec.getImage(), URLImage.RESIZE_SCALE);

        img.fill(10, 50);
      
imgv=new ImageViewer(img.fill(150, 200));
        finishLandingPage.add(imgv);
        Label l = new Label(spec.getNom());
        Label ad= new Label(spec.getAdresse());
        ad.getAllStyles().setFgColor(0x1c02f6);
        l.getAllStyles().setFgColor(0xF69602);
        
        SpanLabel l2 = new SpanLabel(spec.getGenre());

       
        
        l2.getAllStyles().setFgColor(0x000000);
     
     
//  imgg = URLImage.createToStorage(encc, "wishlist.png", "http://localhost/datatable_21/web/images/wishlist.png" , URLImage.RESIZE_SCALE);

  //      imgg.fill(10, 50);
      
//imgvv=new ImageViewer(imgg.fill(25, 25));
        
        SpanLabel l3 = new SpanLabel(spec.getDescription());
        Button loginButton = new Button(">> Details");

        loginButton.getAllStyles().setFgColor(0x008A4F);
        loginButton.addActionListener(e -> {
            /**
             * ************** page detail ************
             */
           specDetails = spec;
            System.out.println(specDetails.toString());
            new DetailDoForm(theme).show();

            /**
             * **************************************
             */
             imgg = URLImage.createToStorage(encc, "wishlist.png", "http://localhost/datatable_21/web/images/fav.png" , URLImage.RESIZE_SCALE);

    imgg.fill(10, 50);
      
imgvv=new ImageViewer(imgg.fill(25, 25));
            
        });
       
            
        Button fav=new Button("Wishlist");
//        wl.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent evt) {
//          
//                
//            }
//        });
//        c.add(wl);
//        finishLandingPage.add(imgvv);

        coeur = URLImage.createToStorage(enc, "fav.png" + i, "http://localhost/datatable_21/web/images/fav.png", URLImage.RESIZE_SCALE_TO_FILL);
        coeur.fill(50, 70);
        coeur2 = URLImage.createToStorage(enc, "defav.png", "http://localhost/datatable_21/web/images/defav.png", URLImage.RESIZE_SCALE_TO_FILL);
        coeur2.fill(50, 70);
        Container image1 = new Container(BoxLayout.x());
        Container image5 = new Container(BoxLayout.x());
        image1.add(coeur.scaledWidth(20));
        image5.add(coeur2.scaledWidth(20));
  
        if (verifFavoris(spec)) {
            c.addAll(l,ad,l2,image1, loginButton);
        } else {
            c.addAll(l,ad,l2,image5, loginButton);
        }
        fav.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (verifFavoris(spec)) {
                    try {
                        db.execute("DELETE FROM favoris WHERE  idu= " + spec.getId() + "");
                        c.replace(image1, image5, null);
                        refreshTheme();
                    } catch (IOException ex) {
                    }
                } else {
                    favoris(spec);
                    c.replace(image5, image1, null);
                    refreshTheme();
                }
            }
        });

        image1.setLeadComponent(fav);
        image5.setLeadComponent(fav);
        
        c.setWidth(500);
        c.getUnselectedStyle().setBackgroundType(Style.BACKGROUND_GRADIENT_RADIAL);
        c.getUnselectedStyle().setBackgroundGradientEndColor(0xeae4e4);
        c.getUnselectedStyle().setBackgroundGradientStartColor(0xeae4e4);

        containervide.getUnselectedStyle().setBackgroundType(Style.BACKGROUND_GRADIENT_RADIAL);
        containervide.getUnselectedStyle().setBackgroundGradientEndColor(0xFFFFFF);
        containervide.getUnselectedStyle().setBackgroundGradientStartColor(0xFFFFFF);
        finishLandingPage.getUnselectedStyle().setBackgroundType(Style.BACKGROUND_GRADIENT_RADIAL);
        finishLandingPage.getUnselectedStyle().setBackgroundGradientEndColor(0xFFFFFF);
        finishLandingPage.getUnselectedStyle().setBackgroundGradientStartColor(0xFFFFFF);
        // c.add(containervide);
        finishLandingPage.add(c);
        // finishLandingPage.add(containervide);
        c.setPreferredW(400);
        add(FlowLayout.encloseIn(finishLandingPage));
        add(containervide);
    }

    private Image createCircleLine(int color, int height, boolean first) {
        Image img = Image.createImage(height, height, 0);
        Graphics g = img.getGraphics();
        g.setAntiAliased(true);
        g.setColor(0xcccccc);
        int y = 0;
        if (first) {
            y = height / 6 + 1;
        }
        g.drawLine(height / 2, y, height / 2, height);
        g.drawLine(height / 2 - 1, y, height / 2 - 1, height);
        g.setColor(color);
        g.fillArc(height / 2 - height / 4, height / 6, height / 2, height / 2, 0, 360);
        return img;
    }

    @Override
    protected void showOtherForm(Resources res) {
        new StatsForm(res).show();
    }
}
