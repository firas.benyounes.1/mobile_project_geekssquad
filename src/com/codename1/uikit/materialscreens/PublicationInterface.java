
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.codename1.uikit.materialscreens;


import com.allforkids.Entite.Publication;
import com.allforkids.Service.PublicationService;
import com.codename1.components.SpanLabel;
import com.codename1.io.Log;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.table.TableLayout;
import com.codename1.ui.util.Resources;

import java.util.List;

/**
 *
 * @author ikbel
 */
public class PublicationInterface 
{
 
    private Form f1;
    private Form f3;
    private Label lbnompublication;
    private Label lbnomt;
    private Label resolu;
    private Button btcommentaires;
    private Resources theme;
    private Button btdetailpublication;
    private Button ajp;
    
    public PublicationInterface()
    {
        theme = UIManager.initFirstTheme("/theme");
            Toolbar.setGlobalToolbar(true);
        Log.bindCrashProtection(true);
        
        f1=new Form("Liste des publications",new TableLayout(2,2));
       
        f3=new Form();
        
        f1.getToolbar().addCommandToRightBar("Partage", null, e->{ 
        
                        System.out.println("okkkkkkkk");
                      
            InterfaceAcceuil im=new InterfaceAcceuil();
              im.getF().show();
        });
    }
    
    public Container AddPublicationtocontainer(Publication p)
    {
 
        
        lbnompublication = new Label("Titre :"+p.getTitre() );
        lbnomt = new Label("Publié par :" +p.getNomuser() );
        btdetailpublication = new Button("Details");
        btdetailpublication.setWidth(20);
        btcommentaires = new Button("Voir commentaires");
        btcommentaires.setWidth(20);
        
        resolu = new Label("Cette publication est "+p.getResol());
        btcommentaires.addActionListener(e->{
                  CommentaireInterface icom=new CommentaireInterface();
                  icom.getF1(p.getId()).show();
                 });
        btdetailpublication.addActionListener(e -> {  
            String msg = "Contenu :" + p.getContenu();
            String categ = "Categorie: " + p.getNomcategorie();
            SpanLabel message = new SpanLabel(msg);
            Dialog.show(p.getTitre(), msg + "\n" + categ , "Fermer", null);
            
        });
        
        Container cnt1 = new Container(BoxLayout.y());
        Container cnt2 = new Container(BoxLayout.x());
        cnt1.add(lbnompublication);
        cnt1.add(lbnomt);
        cnt1.add(resolu);
        cnt1.add(btdetailpublication);
        cnt1.add(btcommentaires);
        cnt2.add(cnt1);
        if((p.getResolu())==1)
        {
            cnt2.getStyle().setBgColor(10682268);
            cnt2.getStyle().setBgTransparency(255);
        }
        return cnt2;
    
    }
    
    public void AfficherProduit()
    {
        PublicationService ps=new PublicationService();
        List<Publication> lsp=ps.getPublications();
        
        int i=0;
       
        for(Publication p: lsp)
        {
        i++;
            if(i<=9)
            {
                f1.add(AddPublicationtocontainer(p));
                 f1.add("\n");
            }
          }
        
        ajp = new Button("Ajouter publication");
        ajp.setWidth(20);
        ajp.addActionListener(e->{
           PublicationService psrvc= new PublicationService();
           Ik_AjoutPubInterface pi=new Ik_AjoutPubInterface();
           pi.getF1().show();
                 });
          f1.add(ajp);
        }
    

    public Form getF1() {
       AfficherProduit();
        return f1;
    }

    public void setF1(Form f1) {
        this.f1 = f1;
    }

   

    public Form getF3() {
        return f3;
    }

    public void setF3(Form f3) {
        this.f3 = f3;
    }
    
    
    
    
    
    
    
}